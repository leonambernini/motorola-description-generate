// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#0d0f23', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // MOSAIC
    { 'mosaic': {
        'title': '<div style="padding-bottom: 2vh; padding-top: 2vh">life looks better with moto e</div>',
        'items': [
            {'width': '50%', 'height': '350px', 'bg': 'pic-2-0044.jpg', 'title': 'design + display', 'text': 'Photos, videos, and games come to life on a stunning smartphone that fits right in your hand', 'btn': 'Learn more'},
            {'width': '50%', 'height': '350px', 'bg': 'pic-2-0045.jpg', 'title': 'life-ready features', 'text': 'Worry less thanks to a water-repellent coating and a secure fingerprint reader', 'btn': 'Learn more'},
            {'width': '50%', 'height': '350px', 'bg': 'pic-2-0046.jpg', 'title': 'fast performance', 'text': 'The processor and 4G speed you need, plus the world’s most popular OS', 'btn': 'Learn more'},
            {'width': '25%', 'height': '350px', 'bg': 'pic-2-0047.jpg', 'title': 'advanced cameras', 'text': 'Capture your life beautifully, even in low light', 'btn': 'Learn more'},
            {'width': '25%', 'height': '350px', 'bg': 'pic-2-0048.jpg', 'title': 'all-day battery', 'text': 'Keep going for hours on a single charge', 'btn': 'Learn more'},
        ],
        'modalbg': '#ff6901',
        'modalcolor': '#fff',
        'modalBtnClose': 'icon_xCLose_50.svg',
        'modalItems': [
            {'model': 'text-image', 'title': 'visibly brilliant', 'text': 'With a vibrant, 5" HD display and a compact design, this is a phone you’ll want to hold onto. Choose from sophisticated finishes in Licorice Black and Fine Gold.<sup>‡</sup>', 'img': 'pic-2-0049.jpg', 'colText': '5', 'colImage': '7'},
            {'model': 'multi-text', 'texts': [
                {'type': 'col', 'col': '8', 'title': 'make a splash', 'text': 'Never let spills, splashes, or a little rain get in your way. The Moto E uses a water-repellent nanocoating to protect your phone inside and out.*', 'img': 'pic-2-0050.jpg', 'colText': '6', 'colImage': '6'},
                {'type': 'card', 'col': '4', 'title': 'one less thing to remember', 'text': 'Go ahead, forget your passcode. All you need is the touch of your finger to instantly unlock your phone.**', 'img': 'pic-2-0051.jpg'},
            ]},
            {'model': 'cards', 'title': 'for everyone who’s always looking for more', 'text': '', 'cards': [
                {'img': 'pic-2-0052.jpg', 'title': 'quad-core processor', 'text': 'With a quad-core processor, play games, swipe through apps, and scroll through websites, without any lag.§', 'bg': '#ff6a00', 'col': '4'},
                {'img': 'pic-2-0053.jpg', 'title': 'Android™ 7.1', 'text': 'Run Android™ 7.1, an updated version of the world’s most popular operating system.', 'bg': '#ff6a00', 'col': '4'},
                {'img': 'pic-2-0054.jpg', 'title': '4G speed', 'text': 'Enjoy stutter-free streaming of your favorite videos, with the 4G speed you need.§', 'bg': '#ff6a00', 'col': '4'},
            ]},
            {'model': 'multi-text', 'texts': [
                {'type': 'col', 'col': '8', 'title': 'everything’s in autofocus', 'text': 'Snap crisp, gorgeous photos with the 8 MP autofocus camera, even in low light conditions.', 'img': 'pic-2-0055.jpg', 'colText': '6', 'colImage': '6'},
                {'type': 'card', 'col': '4', 'title': 'smiles everywhere', 'text': 'It’s your moment. Take selfies worth sharing with the 5 MP front camera.', 'img': 'pic-2-0056.jpg'},
            ]},
            {'model': 'text-image', 'title': 'power play', 'text': 'Never worry about recharging when you’re out and about. Free yourself with an all-day 2800 mAh battery.†', 'img': 'pic-2-0057.jpg', 'colText': '5', 'colImage': '7'},
        ],
    }},

    // SPECIFICATION MORE
    { 'specification-btn-more': {
        'title': 'specifications', 
        'img': 'pic-2-0058.jpg', 
        'bgcolor': '#f5f5f5',
        'titlecolor': '#000',
        'col1': '<h5>5” HD display + compact design</h5><p class="p1">Bring photos, videos, and games to life on a stunning smartphone that fits right in your hand</p><h5>water-repellent</h5><p class="p1">Nanocoating protects your phone inside and out*</p><h5>fingerprint sensor</h5><p class="p1">Instantly unlock your phone using just the touch of your finger (select carriers**)</p>',
        'col2': '<h5>fast performance + Android™ 7.1</h5><p class="p1">With a quad-core processor and 4G speed, watch videos, play games, and more, without having to wait.<sup>§</sup></p><h5>advanced cameras</h5><p class="p1">Take beautifully sharp photos and selfies, even in low light conditions</p><h5>all-day battery</h5><p class="p1">Keep going for hours on a single charge†</p>',
        'more': {
            'img': 'pic-2-0043.jpg', 
            'bgcolor': '#fff',
            'titlecolor': '#000',
            'col1': '<h5>unlocked + carrier versions</h5><p>Moto E⁴ is unlocked for all major U.S. networks, like Verizon, AT&amp;T, T-Mobile, and Sprint<sup>††</sup>. Enjoy the freedom to switch carriers but keep your phone. Or, select a carrier version that works with the carrier`s SIM card and includes the carrier apps and services you expect.<br><a href="https://www.motorola.com/us/carrier-compatibility" style="color: #0090a6">View carrier compatibility chart</a></p><h5>operating system</h5><p class="p1">Android™ 7.1, Nougat</p><h5>system architecture/processor</h5><p class="p1">Quad-core 1.4 GHz , Qualcomm® Snapdragon™ 425, or, Quad-core 1.4 GHz , Qualcomm® Snapdragon™ 427</p><h5>memory (RAM)</h5><p class="p1">2 GB</p><h5>storage (ROM)</h5><p class="p1">16 GB</p><h5>expandable storage</h5><p>Up to 128GB</p><h5>dimensions</h5><p class="p1">144.5 x 72 x 9.3mm</p><h5>weight</h5><p class="p1">150g</p><h5>display</h5><p class="p1">5” HD 720p (1280x720)<br>Up to 16 million colors<br>Lenovo Screen 70% NTSC<br>2.5D cover glass<br>Full lamination</p><h5>battery</h5><p class="p1">2800 mAh†<br>Removable<br>5W/10W rapid charger</p><h5>water protection</h5><p class="p1">Yes. Water-repellent coating.*</p><h5>networks</h5><p class="p1">GSM<br>CDMA<br>WCDMA<br>TDD<br>FDD</p><h5>bands</h5><p class="p1">GSM 850/900/1800/1900 MHz</p><p class="p1">WCDMA B1 (2100)/ B2 (1900)/ B4 (1700/2100)/ B5 (850)/ B8 (900)MHz</p><p class="p1">CDMA BC0 (850) BC1 (1900) BC10 (850+)</p><p class="p1">LTE TDD 38/41</p><p class="p1">FDD Band 1/2/4/5/7/8/12(lower 700 1bc)/13 (upper 700)/17 (lower 700 bc)/25/26/66</p><p>.</p>',
            'col2': '<h5><h5>rear camera</h5><p class="p1">8 MP<br>ƒ / 2.2 aperture<br>1.12 um microns<br>71° lens<br>Autofocus<br>Single LED flash<br>Burst mode<br>Panorama<br>HDR<br>Beautification mode</p><h5>front camera</h5><p class="p1">5 MP<br>ƒ / 2.2 aperture<br>1.4 um microns<br>74° lens<br>Fixed focus<br>Single LED flash<br>Burst mode<br>HDR<br>Beautification mode</p><h5>video capture</h5><p class="p1">1080p (30fps)</p><h5>SIM card</h5><p class="p1">Single Nano-SIM</p><h5>connectivity</h5><p class="p1">MicroUSB<br>3.5mm headset port</p><h5>Bluetooth® technology</h5><p class="p1">Bluetooth® version 4.1 LE</p><h5>Wi-Fi</h5><p class="p1">802.11a/b/g/n<br>Wi-Fi hotspot</p><h5>speakers/microphones</h5><p class="p1">Two-in-one speaker at 84dB<br>Dual microphones</p><h5>NFC</h5><p class="p1">No</p><h5>location services</h5><p class="p1">GPS</p><h5>sensors</h5><p class="p1">Vibration<br>Proximity<br>Light<br>Accelerometer<br>Magnetometer (e-compass)<br>Fingerprint reader (in some models)**</p><h5>base color<sup>‡</sup></h5><p class="p1">Licorice Black<br>Fine Gold</p>',
        }
    }},
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '48 MP rear camera sensor** with 4x greater light sensitivity', 'bgcolor': '#0d0f23', 'text': 'Our largest-ever sensor with Quad Pixel technology combines four pixels into one large 1.6µm pixel for vastly improved low light performance. In other words, you get incredibly sharp 12 MP photos—brighter shots with way less noise.', 'img': 'pic-1-0086.jpg'} },
        { 'info-right': {'title': 'capture the night', 'bgcolor': '#0d0f23', 'text': 'Switch to Night Vision mode in low light conditions to bring more details out of the dark. Capture bright, vivid photos with stunning clarity and more accurate colors.', 'img': 'pic-1-0087.jpg'} },
        { 'info-left': {'title': 'shoot photos like a pro with AI', 'bgcolor': '#0d0f23', 'text': 'With new features like Smart Composition, Auto Smile Capture, and AI Portrait Lighting, artificial intelligence (AI) inside the moto z4 looks for ways to give you the most professional-looking photos possible.', 'img': 'pic-1-0088.jpg'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}