// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#fff', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'power play', 
        'text': '<div style="padding-bottom: 13vh">Go a full day on a single charge, thanks to a 4000 mAh battery. And when it’s time to power up, the 10 W rapid charger gives you a quick burst of power when you need it most.*</div>', 
        'img': 'pic-1-0114.jpg',
        'paddingBox': '13vh 25px 8vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'expansive views', 
        'text': '<div style="padding-bottom: 13vh">Unlike traditional displays, the 5.7” Max Vision screen has a unique 18:9 aspect ratio, so you can see more at a glance. Do less scrolling on websites. Get a crazy-wide landscape view of your favorite games. All without sacrificing comfort, thanks to a slim design and a narrow bezel.</div>', 
        'img': 'pic-1-0115.jpg',
        'paddingBox': '11vh 25px 10vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'goodbye, blurry photos', 
        'text': '<div style="padding-bottom: 13vh">Taking great shots is easy. The 8 MP camera gives you autofocus. It focuses in the blink of an eye and captures bright, clear photos—even in low light conditions.</div>', 
        'img': 'pic-1-0116.jpg',
        'paddingBox': '13vh 25px 8vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'feel the joy', 
        'text': '<div style="padding-bottom: 13vh">With an arched back and a rounded glass front, the compact design is so comfortable to use with just one hand it’ll make you smile. It’s easy on the eyes, too. Aerial lines are hidden away for a smart, clean look.</div>', 
        'img': 'pic-1-0117.jpg',
        'paddingBox': '13vh 25px 10vh',
    }},
        // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'demand more', 
        'text': '<div style="padding-bottom: 10vh">We believe you shouldn’t have to pay premium prices for features that make your phone-life relationship better.</div>',
        'paddingBox': '15vh 25px 0px',
    }},
    // BLOCO DE CARDS
    { 'cards': {'title': '', 'paddingBox': '2vh 25px 5vh', 'cards': [
        {'img': 'icon-068.jpg', 'title': 'Smart, secure fingerprint reader', 'text': 'Touch the fingerprint reader to wake up and unlock your phone instantly, and lock it again when you are done. It is discreetly located within the phone’s iconic logo, so you hardly know it’s there until you use it.'},
        {'img': 'icon-069.jpg', 'title': 'Exclusive interactions', 'text': 'Only <strong>motorola </strong>phones react to simple gestures to instantly reveal a function or communication you need at that moment. Customise which <strong>moto experiences</strong> you’d like to use by simply visiting the <strong>moto </strong>app on your new phone.'},
        {'img': 'icon-070.jpg', 'title': 'You can have it all', 'text': 'Add up to 128 GB more photos, songs, and movies thanks to an extra microSD card slot. Now you don’t have to choose between adding a second SIM card for work or adding more storage.<sup>‡</sup>'},
    ]} },
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'paddingBox': '50px 25px 5vh', 'specifications': [
        {'title': 'All-day battery', 'text': 'Go a full day on a single charge with a 4000 mAh battery.*', 'icon': 'icon-047.svg'},
        {'title': '5.7" Max Vision display', 'text': 'Expansive views with a narrow bezel and a unique 18:9 aspect ratio, all in a slim body.', 'icon': 'icon-071.svg'},
        {'title': '8 MP camera with autofocus', 'text': 'Focus in the blink of an eye and capture bright, clear photos—even in low light conditions.', 'icon': 'icon-056.svg'},
        {'title': 'Compact design', 'text': 'With a curved back and rounded glass front, it’s a joy to hold.', 'icon': 'icon-072.svg'},
        {'title': 'Fingerprint reader', 'text': 'Never mind remembering a passcode. One touch wakes up your phone and unlocks it instantly.', 'icon': 'icon-057.svg'},
        {'title': 'Expandable storage', 'text': 'Make room for more photos, songs, and movies thanks to an extra microSD card slot.<sup>‡</sup>', 'icon': 'icon-073.svg'},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}