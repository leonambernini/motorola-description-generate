// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#fff', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#fff', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#0d0f23', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // BLOCO VIDEO
    { 'movie': {
        'title': '<div style="font-size: 30px"><strong>introducing Polaroid Insta-Share Printer</strong></div><br><br>Watch the product video', 
        'yt_id': 'QZDjJ5mGPMI', 
        'btn_icon': 'icon_circleplay_100@2x.png', 
        'bg': 'pic-2-0096.jpg', 
        'height': '430px',
        'bgcolor': '#fff',
        'titlecolor': '#fff'
    }},
    // CARD 3
    { 'cards3': {
        'title': 'the Polaroid you love, right from your phone', 
        'bgcolor': '#eff2f3',
        'titlecolor': '#000',
        'paddingBox': '0vh 25px 3vh',
        'cards': [
            {'img': 'pic-2-0097.jpg', 'title': 'real Polaroid feel', 'text': 'Press and hold the printer’s physical capture button to launch your <strong>moto z</strong> phone’s camera. Click it again to snap a picture.', 'button': '', 'bgcolor': '#ff6901', 'titlecolor': '#fff'},
            {'img': 'pic-2-0098.jpg', 'title': 'photos from now and then', 'text': 'The printer doesn’t just work in real time. Use it to easily print your favorite pictures from the time vaults of Facebook, Instagram, and Google Photos.', 'button': '', 'bgcolor': '#ff6901', 'titlecolor': '#fff'},
            {'img': 'pic-2-0099.jpg', 'title': 'this paper is magic', 'text': 'Print your beautiful 2x3” photos on the spot with the included ZINK® Zero-Ink® Paper—no ink needed. Once they print, peel off the adhesive backs and stick them anywhere to share the fun.', 'button': '', 'bgcolor': '#ff6901', 'titlecolor': '#fff'},
        ]
    }},
    // BLOCO TEXTO NA ESQUERDA
    { 'info-left': {
        'title': 'filter life is the best life', 
        'text': 'Before you print your photos, amp up the creativity by adding filters, borders, and stickers. Then pick your favorites, print, and share them with friends.', 
        'img': 'pic-2-0100.jpg',
        'bgcolor': '#378edb', 
        'paddingBox': '3vh 30vh 3vh',
    }},
    // BLOCO TEXTO NA ESQUERDA
    { 'info-left': {
        'title': 'stock up on the best part', 
        'text': 'When you run out of ZINK® Zero-Ink® Paper, it’s easy to order more, so you can keep snapping and sharing. The inkless 2x3” photo paper has a peel-off, adhesive back to make showing off your favorite pics even easier.<br><br><br><a class="cta-library-wrap White-on-Blue" href="https://intl.motorola.com/2x3-zink-paper/" target="_blank"><span style="background-color: #3e8edd;border: 1px solid #3e8edd;color: #fff; padding: 10px;">Buy now</span></a>', 
        'img': 'pic-2-0101.jpg',
        'bgcolor': '#FF6B01',
        'paddingBox': '3vh 30vh 3vh',
    }},
    // SPECIFICATION MORE
    { 'specification-btn-more': {
        'title': 'specifications', 
        'img': 'pic-2-0102.jpg', 
        'bgcolor': '#f5f5f5',
        'titlecolor': '#000',
        'col1': '<h5>Capture + print in real time</h5><p class="p1">The instant fun of a Polaroid camera—now palm-sized. Exclusively for <strong>moto z</strong>.</p><h5>Get your click on</h5><p class="p1">A long press on the physical capture button launches your phone’s camera. A short press snaps a picture.</p><h5>Share memories</h5><p class="p1">Print your favorite photos from Facebook, Instagram, and Google Photos.</p>',
        'col2': '<h5>Stick ‘em up</h5><p class="p1">Your 2x3” prints come out on ZINK™ Zero-Ink™ Paper with peel-off, adhesive backs, so you can stick them anywhere.</p><h5>#mofilters, mo’ fun</h5><p class="p1">Add filters, borders, and designs to personalize your pics before you print them.</p>',
        'more': {
            'img': 'pic-2-0103.jpg', 
            'bgcolor': '#fff',
            'titlecolor': '#000',
            'col1': '<h5>Dimensions</h5><p class="p1">153.6 x 73.8 x 20.4 mm</p><h5>Weight</h5><p class="p1">188 g</p><h5>Integrated battery</h5><p class="p1">yes</p><h5>Battery size</h5><p class="p1">500 mAh</p><h5>Battery life</h5><p class="p1">20 prints per charge</p>',
            'col2': '<h5>External charging</h5><p class="p1">Yes, USB-C</p><h5>Charge rate</h5><p class="p1">up to 10W rate</p><h5>Paper storage</h5><p class="p1">Holds up to 10 sheets of paper (1 pack)</p><h5>Paper compatibility</h5><p class="p1">Polaroid 2x3" Premium ZINK® Paper</p><h5>Compatibility</h5><p class="p1">Polaroid Insta-Share Printer is compatible with any phone in the <a href="http://www.motorola.com/us/products/moto-z-family" style="color: #0090a6">moto z family.</a></p><h5>Included in box</h5><p class="p1">Polaroid Insta-Share Printer<br>1 pack of ZINK® paper (10 sheets)</p>',
        }
    }},
    // CARD 3
    { 'cards3': {
        'title': 'go beyond your smartphone', 
        'bgcolor': '#eff2f3',
        'titlecolor': '#000',
        'paddingBox': '0vh 25px 0px',
        'cards': [
            {'img': 'pic-2-0104.jpg', 'title': 'moto z family', 'text': 'Thinner, lighter, faster premium smartphones that transform to support your interests with Moto Mods.', 'button': '<a href="https://www.motorola.com/us/products/moto-z-family" style="background-color: #fff; border: 1px solid #fff; color: #0090a6;">Learn more</a>', 'bgcolor': '#46c7e1', 'titlecolor': '#fff'},
            {'img': 'pic-2-0105.jpg', 'title': 'moto mods family', 'text': 'Your phone becomes a movie projector, stereo speaker, battery powerhouse, and more, in a snap.', 'button': '<a href="https://www.motorola.com/us/products/moto-mods" style="background-color: #fff; border: 1px solid #fff; color: #0090a6;">Learn more</a>', 'bgcolor': '#46c7e1', 'titlecolor': '#fff'},
            {'img': 'pic-2-0106.jpg', 'title': 'moto mod development kit', 'text': 'Dream. Make. Ship. It’s that simple to bring your ideas to life and transform the smartphone in a new way.', 'button': '<a href="https://developer.motorola.com/" style="background-color: #fff; border: 1px solid #fff; color: #0090a6;">Learn more</a>', 'bgcolor': '#46c7e1', 'titlecolor': '#fff'},
        ]
    }},
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '48 MP rear camera sensor** with 4x greater light sensitivity', 'bgcolor': '#0d0f23', 'text': 'Our largest-ever sensor with Quad Pixel technology combines four pixels into one large 1.6µm pixel for vastly improved low light performance. In other words, you get incredibly sharp 12 MP photos—brighter shots with way less noise.', 'img': 'pic-1-0086.jpg'} },
        { 'info-right': {'title': 'capture the night', 'bgcolor': '#0d0f23', 'text': 'Switch to Night Vision mode in low light conditions to bring more details out of the dark. Capture bright, vivid photos with stunning clarity and more accurate colors.', 'img': 'pic-1-0087.jpg'} },
        { 'info-left': {'title': 'shoot photos like a pro with AI', 'bgcolor': '#0d0f23', 'text': 'With new features like Smart Composition, Auto Smile Capture, and AI Portrait Lighting, artificial intelligence (AI) inside the moto z4 looks for ways to give you the most professional-looking photos possible.', 'img': 'pic-1-0088.jpg'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}