// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#000', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#fff', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#fff', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#000', // COR DE FUNDO DO SLIDER
    'cards-bg': '#000', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'get projected speeds 10 times faster than today’s wireless technology*', 
        'text': '<div style="padding-bottom: 10vh">Claim a part of history with the first ever use of Millimeter Wave technology for a smartphone. <a class="family-hero-cta" href="https://www.qualcomm.com/invention/5g" style="text-decoration: underline;text-decoration;color: white;" target="_blank"><strong>Qualcomm® 5G</strong></a> and a first-of-its-kind antenna array connect you to a network designed for pure speed.<sup>**</sup> Think more devices, more data, and more people—connecting faster than ever.</div>',
        'img': 'pic-1-0131.png',
        'paddingBox': '2vh 25px 10vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'download an entire 4K movie in seconds*', 
        'text': '<div style="padding-bottom: 10vh">The <strong>5G moto mod</strong> connects you with greater bandwidth than ever before. Have more fun. Get more done. Imagine the possibilities with the fastest wireless network ever.</span></div>',
        'paddingBox': '2vh 25px 2vh',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': '', 
        'yt_id': 'qa5D7e2jNuE', 
        'btn_icon': 'icon_circleplay_100@2xvermelho.png', 
        'bg': 'pic-1-0132.jpg', 
        'height': '430px',
        'bgcolor': '#000',
        'titlecolor': '#fff',
        'paddingBox': '11vh 25px',
    }},
     // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'play cloud-based games without the wait', 
        'text': '<div style="padding-bottom: 10vh">With reduced lag time on the 5G network, you never miss a moment. Websites load instantly. Apps run seamlessly.</div>',
        'img': 'pic-1-0133.jpg',
        'paddingBox': '10vh 25px 2vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'enjoy a smooth, responsive experience with augmented and virtual reality', 
        'text': 'Snap <strong>moto 5G</strong> on your <strong>moto z<sup>3</sup></strong> for dramatically lower latency, and unlock completely new possibilities on a network that keeps up with you.',
        'paddingBox': '10vh 25px 10vh',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': '', 
        'yt_id': 'fbna0O6EDYA', 
        'btn_icon': 'icon_circleplay_100@2xvermelho.png', 
        'bg': 'pic-1-0134.jpg', 
        'height': '430px',
        'bgcolor': '#000',
        'titlecolor': '#fff',
        'paddingBox': '11vh 25px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'connect all your devices without compromising speed', 
        'text': '<div style="padding-bottom: 10vh">Use <strong>moto z<sup>3</sup></strong> with <strong>moto 5G</strong> as a blazing fast Wi-Fi hotspot for friends or co-workers.<sup>‡</sup> Or, use a compatible USB 3.1 cable (included) to tether directly to your laptop, supercharging productivity with 5G speed.<sup>*</sup></div>',
        'img': 'pic-1-0135.jpg',
        'paddingBox': '10vh 25px 2vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'don’t stress about draining your phone’s power', 
        'text': '<div style="padding-bottom: 10vh">The <strong>moto mod</strong> powers 5G speed on your phone with a built-in 2000 mAh battery. Plus, it has its own USB-C port for charging.</div>',
        'img': 'pic-1-0136.jpg',
        'paddingBox': '10vh 25px 2vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'turn your <span style="color:#E1140A">moto z</span> into a 5G phone', 
        'text': '<div style="padding-bottom: 10vh">It’s seamless. Simply snap <strong>moto 5G</strong> on your <strong>moto z<sup>3</sup></strong> to connect to Verizon’s 5G network, or detach to instantly return to 4G LTE. Plus, with backwards compatibility you will soon be able to turn yesterday’s <strong>moto z</strong> into a 5G phone.<sup>‡</sup><br><br><br><a class="cta-library-wrap Custom-rounded-76632-40742" href="https://www.motorola.com/us/products/moto-z-gen-3 " target="_self"><span style="border: 1px solid #FFFFFF; background-color: rgba(0,0,0,1.0); border-radius: 25px; padding: 12px 25px; font-weight: 500 !important; color: #FFFFFF;">Get moto z³</span></a></div>',
        'img': 'pic-1-0137.jpg',
        'paddingBox': '10vh 25px 2vh',
    }},
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'bgcolor': '#000', 'paddingBox': '40px 25px 5vh', 'specifications': [
        {'title': 'Fuel your future', 'text': 'Simply snap <strong>moto 5G</strong> onto your <strong>moto z³</strong> to be the first to access the Verizon 5G network.<sup>†</sup> Soon, you will even be able to turn yesterday’s <strong>moto z</strong> into a 5G phone.<sup>‡</sup>', 'icon': 'icon-081.png'},
        {'title': 'Only on Verizon', 'text': 'Take the best coverage in the nation to a whole new level.', 'icon': 'icon-082.png'},
        {'title': 'Faster than ever before', 'text': '5G transfers data a projected 10 times faster than today’s wireless technology, so your favorite content is only seconds away.*', 'icon': 'icon-083.png'},
        {'title': 'Goodbye, lag time', 'text': 'Websites load instantaneously. Graphics-heavy apps run seamlessly.*', 'icon': 'icon-084.png'},
        {'title': 'Bandwidth to share', 'text': 'Your phone’s WiFi hotspot gets a lightning fast boost when you snap on the <strong>5G moto mod</strong>.<sup>†</sup>', 'icon': 'icon-085.png'},
        {'title': 'Built-in battery', 'text': 'Don’t slow down while your data revs up. The <strong>5G moto mod</strong> is powered by a 2000 mAh battery.', 'icon': 'icon-086.png'},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '48 MP rear camera sensor** with 4x greater light sensitivity', 'bgcolor': '#0d0f23', 'text': 'Our largest-ever sensor with Quad Pixel technology combines four pixels into one large 1.6µm pixel for vastly improved low light performance. In other words, you get incredibly sharp 12 MP photos—brighter shots with way less noise.', 'img': 'pic-1-0086.jpg'} },
        { 'info-right': {'title': 'capture the night', 'bgcolor': '#0d0f23', 'text': 'Switch to Night Vision mode in low light conditions to bring more details out of the dark. Capture bright, vivid photos with stunning clarity and more accurate colors.', 'img': 'pic-1-0087.jpg'} },
        { 'info-left': {'title': 'shoot photos like a pro with AI', 'bgcolor': '#0d0f23', 'text': 'With new features like Smart Composition, Auto Smile Capture, and AI Portrait Lighting, artificial intelligence (AI) inside the moto z4 looks for ways to give you the most professional-looking photos possible.', 'img': 'pic-1-0088.jpg'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}