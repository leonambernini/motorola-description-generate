// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#fff', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#fff', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#0d0f23', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // SPECIFICATION MORE
    { 'specification-btn-more': {
        'title': 'specifications', 
        'img': 'pic-1-0142.jpg', 
        'bgcolor': '#fff',
        'titlecolor': '#000',
        'col1': '<h5>up to 30-hour battery<sup>†</sup></h5><p class="p1">Plus, get up to 8 hours of power in 15 minutes with TurboPower<sup>†</sup></p><h5>focus faster</h5><p class="p1">Dual autofocus pixels plus laser autofocus for amazing photos in any light</p><h5>fast memory and tons of space for music, movies, and photos<sup>‡</sup></h5><p class="p1">Enjoy 64 GB and 4 GB RAM, or 32 GB and 3 GB RAM. Plus, add up to 2 TB more with a microSD card.<sup>††</sup></p>',
        'col2': '<h5>Ultra-low light selfie cam</h5><p class="p1">Always look your best thanks to a 16 MP front camera and a selfie flash.</p><h5>Precision-crafted glass and metal</h5><p class="p1">Easy to hold and easy on the eyes, featuring a beautiful, contoured design.</p><h5>Blazing-fast TurboPower™ charging</h5><p class="p1">Get up to 6 hours of power in just 15 minutes.<sup>†</sup></p><h5>Qualcomm<sup>®</sup> Snapdragon™ processor</h5><p class="p1">Get octa-core performance without sacrificing style.</p>',
        'more': {
            'img': 'pic-1-0127.png', 
            'bgcolor': '#fff',
            'titlecolor': '#000',
            'col1': '<h5><h5>carrier versions</h5><p class="p1">Available in an unlocked version for major U.S. carriers like Verizon, Sprint<sup>§§</sup>, AT&amp;T, and T-Mobile. Also available as a Verizon-exclusive. <a href="https://www.motorola.com/us/carrier-compatibility" style="color: #0090a6">Check carrier compatibility</a></p><h5>operating system</h5><p class="p1">Android™ 8.0, Oreo</p><h5>system architecture/processor</h5><p class="p1">Motorola Mobile Computing System, including Qualcomm® Snapdragon™ 626 processor with up to 2.2 GHz Octa-Core CPU and Adreno 506 GPU<br>Natural Language Processor<br>Contextual Computing Processor</p><h5>memory (RAM)</h5><p class="p1">3GB LPDDR3 / 4GB LPDDR3</p><h5>storage (ROM)</h5><p class="p1">32GB / 64GB<sup>‡</sup><br>microSD Card slot with UHS-I support (up to 2 TB)<sup>††</sup></p><h5>dimensions</h5><p class="p1">76.2 x 156.2 x 5.99 mm</p><h5>weight</h5><p class="p1">145g</p><h5>display</h5><p class="p1">5.5 Full HD 1080p (1920 x 1080) Super AMOLED<br>Up to 16 million colors. 401 PPI<br>Corning<sup>®</sup>Gorilla<sup>®</sup> Glass</p><h5>battery</h5><p class="p1">3000 mAh, up to 30 hours*<br>TurboPower charging for up to 8 hours of use in 15 minutes (50% charge in 30 minutes)<sup>†</sup></p><h5>water protection</h5><p class="p1">Water repellent nano-coating**</p><h5>network bands</h5><p class="p1">CDMA (850, <span class="text-nowrap">1900 MHz</span>)<br>GSM/GPRS/EDGE (850, 900, 1800, <span class="text-nowrap">1900 MHz</span>)<br>UMTS/HSPA+ (850, 900, 1700, 1900, <span class="text-nowrap">2100 MHz</span>)<br>4G LTE (B1, 2, 3, 4, 5, 7, 8, 12, 13, 17, 20, 25, 26, 28, 29, 30, 38, 41, 66)</p><h5>SIM card</h5><p class="p1">Nano-SIM</p><h5>Bluetooth® technology</h5><p class="p1">Bluetooth version 4.2 LE + EDR</p><h5>sensors</h5><p class="p1" style="margin-bottom: 10px;">Fingerprint Sensor<br>Accelerometer<br>Ambient Light<br>Gyroscope<br>Magnetometer<br>Proximity<br>Ultrasonic</p>',
            'col2': '<h5><h5>rear camera</h5><p class="p1">12MP 1.4um Dual Autofocus Pixel<br>ƒ/1.7<br>Laser Autofocus (Up to 5 meters)<br>Phase Detection Auto Focus (PDAF)<br>Color Correlated Temperature (CCT) dual LED flash<br>Professional mode<br>Auto Night mode<br>Video Encode: 480p (30fps) 720p (120fps) 1080p (60fps), 4K (30fps)</p><h5>front camera</h5><p class="p1">5MP 1.4um<br>ƒ/2.2<br>Wide-Angle lens<br>Color Correlated Temperature (CCT) dual LED flash<br>Auto Night mode<br>Beautification software<br>Professional mode<br>Video Encode: 480p (30fps) 720p (120fps) 1080p (30fps)</p><h5>connectivity</h5><p class="p1">Moto Mods™ connector<br>USB-C™ port with USB 3.1 support<br>3.5mm headset port</p><h5>FM radio</h5><p class="p1">Yes</p><h5>speakers/microphones</h5><p class="p1">Front-ported loud speaker<br>3 Mics</p><h5>video playback</h5><p class="p1">480p (30fps) 720p (120fps) 1080p (60fps), 4K (30fps)</p><h5>NFC</h5><p class="p1">Yes</p><h5>location services</h5><p class="p1">A-GPS, GLONASS</p><h5 style="font-family: &quot;Gotham A&quot;, &quot;Gotham B&quot;, ruble; color: rgb(34, 44, 48);">Wi-Fi</h5><p class="p1" style="margin-bottom: 10px;">802.11 a/b/g/n 2.4 GHz + 5 GHz</p><h5>colors</h5><p class="p1">Lunar Gray<br>Fine Gold</p><h5>moto mods</h5><p class="p1">Compatible with all Moto Mods. <a href="https://www.motorola.com/us/moto-mods" style="color: #0090a6">Learn more</a></p><h5>moto experiences</h5><p class="p1">Moto Display, Moto Voice, Moto Actions<br><a href="https://www.motorola.com/us/software-and-apps" style="color: #0090a6">Learn more</a></p>',
        }
    }},
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '48 MP rear camera sensor** with 4x greater light sensitivity', 'bgcolor': '#0d0f23', 'text': 'Our largest-ever sensor with Quad Pixel technology combines four pixels into one large 1.6µm pixel for vastly improved low light performance. In other words, you get incredibly sharp 12 MP photos—brighter shots with way less noise.', 'img': 'pic-1-0086.jpg'} },
        { 'info-right': {'title': 'capture the night', 'bgcolor': '#0d0f23', 'text': 'Switch to Night Vision mode in low light conditions to bring more details out of the dark. Capture bright, vivid photos with stunning clarity and more accurate colors.', 'img': 'pic-1-0087.jpg'} },
        { 'info-left': {'title': 'shoot photos like a pro with AI', 'bgcolor': '#0d0f23', 'text': 'With new features like Smart Composition, Auto Smile Capture, and AI Portrait Lighting, artificial intelligence (AI) inside the moto z4 looks for ways to give you the most professional-looking photos possible.', 'img': 'pic-1-0088.jpg'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}