// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#fff', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#fff', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#0d0f23', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [

    // BLOCO VIDEO
    { 'movie': {
        'title': 'Watch the video', 
        'yt_id': 'n8j04xLMP_E', 
        'btn_icon': 'icon_circleplay_100@2x.png', 
        'bg': 'pic-1-0061.jpg', 
        'height': '720',
        'bgcolor': '#000',
        'titlecolor': '#fff',
    }},

    // BLOCO TEXTO NA DIREITA
    { 'info-right': {
        'title': 'our largest camera sensor. your best photos.', 
        'text': 'Pictures turn out great even when the lighting isn’t, thanks to a huge 48 MP rear camera sensor** with new Night Vision mode. Plus, there’s optical image stabilization (OIS) to automatically steady unwanted camera movement, and artificial intelligence features to help you shoot like a pro.', 
        'img': 'pic-1-0062.jpg', 
        'bgcolor': '#0d0f23',
        'btn_more': 'More camera features +', 
        'more_info': 'more-1' // REMOVER ESSA LINHA CASO NÃO TENHA VER MAIS
    }},

    // BLOCO DE SLIDER
    { 'slider': {'title': 'more moto z<sup>⁴</sup> camera features', 'titlecolor': '#fff', 'sliders': [
            {'img': 'pic-1-0063.jpg', 'text': '<p><strong>Night Vision</strong><br><br>Switch to Night Vision mode in low light conditions to bring more details out of the dark. Capture bright, vivid photos with stunning clarity and more accurate colors.</p>'},
            {'img': 'pic-1-0064.jpg', 'text': '<p><strong>Spot Color</strong><br><br>Pick one color to keep, and then turn everything else in the photo to black & white.</p>'},
            {'img': 'pic-1-0065.jpg', 'yt_id': 'E8RkWBzvgRI', 'btn_icon': 'icon_circleplay_100@2xbranco.png', 'text': '<p><strong>Cinemagraph</strong><br><br>Keep a portion of your shot in motion while freezing everything else for a unique effect. </p>'},
            {'img': 'pic-1-0066.jpg', 'yt_id': 'Xlpta3moPkQ', 'btn_icon': 'icon_circleplay_100@2xbranco.png', 'text': '<p><strong>Hyperlapse</strong><br><br>Shoot a timelapse while in motion and smooth out unwanted jerks, shakes and bounces.</p>'},
            {'img': 'pic-1-0067.jpg', 'yt_id': 'GRej6nD5Bg8', 'btn_icon': 'icon_circleplay_100@2xbranco.png', 'text': '<p><strong>AR Stickers</strong><br><br>Add fun, animated characters to the scenes in your photos and videos using augmented reality (AR) stickers. Coming soon via Play Store update!</p>'},
            {'img': 'pic-1-0068.jpg', 'yt_id': 'jN6sumC_lh8', 'btn_icon': 'icon_circleplay_100@2xbranco.png', 'text': '<p><strong>Optical Image Stabilization</strong><br><br>Optical Image Stabilization (OIS) technology automatically reduces unwanted camera movements that otherwise cause blurred  images and video.</p>'},
            {'img': 'pic-1-0069.jpg', 'text': '<p><strong>moto mods</strong><br><br>Do things you never thought possible with a camera phone just by snapping on a moto mod<sup>§</sup>. Capture 360-degree photos and videos or add a Polaroid printer, just like that. <a href="https://www.motorola.com/us/products/moto-mods" style="color: #fff;" target="_blank" tabindex="0"><u>Learn more about all camera features</u></a></p>'},
    ]} },

    // BLOCO TEXTO NA ESQUERDA
    { 'info-left': {
        'title': '25 MP selfie camera', 
        'text': 'Shoot with one of the highest-resolution selfie cameras around. Quad Pixel mode gives you incredibly sharp 6 MP selfies in low light. Plus, use selfie portrait mode for an artistic blur effect in the background, and control touch-ups with AI-based beautification.', 
        'img': 'pic-1-0070.jpg', 
        'btn_more': 'Learn more +',
        'bgcolor': '#0d0f23', 
        'more_info': 'more-2' // REMOVER ESSA LINHA CASO NÃO TENHA VER MAIS
    }},

    // BLOCO TEXTO NA DIREITA
    { 'info-right': {
        'title': 'up to 2 days of battery life', 
        'text': 'Get through the weekend on a single charge with a 3600 mAh battery<sup>‡‡</sup>, the largest ever on a moto z. Stretch your phone’s life even further adding <a href="https://www.motorola.com/us/products/moto-mods/moto-power-pack#buy " style="color: #ffffff;" tabindex="0" target="_blank"><u>battery moto mods</u></a><sup>§</sup>. Plus, get hours of power in just minutes of <strong>turbopower™</strong> charging<sup>†</sup>.', 
        'img': 'pic-1-0071.jpg', 
        'bgcolor': '#0d0f23',
        'paddingBox': '5vh 10vh 5vh',
    }},
    // BLOCO TEXTO NA ESQUERDA
    { 'info-left': {
        'title': 'experience the brilliance of an OLED display', 
        'text': 'Enjoy movies, games, and more with vibrant colors and sharp details on an edge-to-edge 6.4" Max Vision display.', 
        'img': 'pic-1-0072.jpg', 
        'bgcolor': '#0d0f23', 
        'paddingBox': '5vh 10vh 5vh',
    }},
    // BLOCO TEXTO NA DIREITA
    { 'info-right': {
        'title': 'integrated fingerprint sensor', 
        'text': 'New optical sensing technology can read your fingerprint through the display glass, so it appears on-screen only when you need it, then discreetly fades away. <a class="family-hero-cta" href="https://support.motorola.com/us/en/products/cell-phones/moto-zfamily/moto-z4/documents/MS140565" style="text-decoration: underline;text-decoration;color: white;" target="_blank">Click here</a> to learn more about fingerprint security and to see a list of compatible screen protectors.', 
        'img': 'pic-1-0073.jpg', 
        'bgcolor': '#0d0f23',
        'paddingBox': '5vh 10vh 5vh',
    }},
    // BLOCO TEXTO NA ESQUERDA
    { 'info-left': {
        'title': 'Qualcomm<sup>®</sup> Snapdragon<sup>™</sup> mobile platform', 
        'text': 'Feel the speed of a Qualcomm<sup>®</sup> Snapdragon<sup>™</sup> 675 processor that’s 57% faster than moto z3 play. It’s a phone built to keep up with you.', 
        'img': 'pic-1-0074.jpg', 
        'bgcolor': '#0d0f23', 
        'paddingBox': '5vh 10vh 5vh',
    }},
    // BLOCO TEXTO NA DIREITA
    { 'info-right': {
        'title': 'customize your experience', 
        'text': 'Make every night movie night with a 70" projector. Capture the whole scene with a 360 degree camera. Instantly turn up the music with a stereo speaker. Whatever you’re into, there’s a <strong>moto mod</strong>™§ for you.<br><a href="https://www.motorola.com/us/products/moto-mods/" style="color:#fff;">Learn more</a>', 
        'img': 'pic-1-0075.jpg', 
        'bgcolor': '#0d0f23',
        'paddingBox': '5vh 10vh 5vh',
    }},

    // BLOCO DE CARDS
    { 'cards': {'title': 'more features you’ll love', 'bgcolor': '#0d0f23','cards': [
        {'img': 'icon-035.jpg', 'title': 'Headphone jack', 'text': 'You asked. We listened. The 3.5mm headset jack is back, so you can listen to music, books, podcasts, and more without an adapter.'},
        {'img': 'icon-036.jpg', 'title': 'Clutter-free Android', 'text': 'Experience Android as it was intended with no duplicate apps, no clunky UI skins, and software that doesn’t slow you down.'},
        {'img': 'icon-037.jpg', 'title': 'Exclusive moto experiences', 'text': 'Exclusive Moto Experiences make using your moto z4 easier than ever. Preview notifications from the lock screen, or navigate with intuitive gestures.<br><a href="https://www.motorola.com/us/software-and-apps" style="color:#fff;">Learn more</a>'},
        {'img': 'icon-038.jpg', 'title': 'Water repellent. Worry proof.', 'text': 'Whether you’re sweating it out at the gym or taking calls in the rain, a water-repellent design keeps your phone protected inside and out.'},
    ]} },
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'paddingBox': '40px 25px 5vh', 'bgcolor': '#0d0f23','specifications': [
        {'title': '48 MP sensor** with Night Vision', 'text': 'Shoot amazing photos even in the dark with Quad Pixel technology, optical image stabilization, and Night Vision mode.', 'icon': ''},
        {'title': '25 MP selfies', 'text': 'With Quad Pixel technology, get 4x better low light sensitivity for stunning selfies.', 'icon': ''},
        {'title': 'Up to 2 days of battery life<sup>‡‡</sup><', 'text': 'Go way beyond a full day on a single charge. Then get hours of power in just minutes with turbopower™† charging.', 'icon': ''},
        {'title': '6.4" OLED display with on-screen fingerprint reader', 'text': 'Bring videos and photos to life with a brilliant OLED display, and instantly unlock with a fingerprint reader that only appears when you need it.', 'icon': ''},
        {'title': 'Personalize with moto mods<sup>™</sup>', 'text': 'Boost your battery, audio, or camera in ways that other smartphones can’t.', 'icon': ''},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '48 MP rear camera sensor** with 4x greater light sensitivity', 'paddingBox': '5vh 30vh 5vh', 'bgcolor': '#0d0f23', 'text': 'Our largest-ever sensor with Quad Pixel technology combines four pixels into one large 1.6µm pixel for vastly improved low light performance. In other words, you get incredibly sharp 12 MP photos—brighter shots with way less noise.', 'img': 'pic-1-0086.jpg'} },
        { 'info-right': {'title': 'capture the night', 'bgcolor': '#0d0f23', 'paddingBox': '5vh 30vh 5vh', 'text': 'Switch to Night Vision mode in low light conditions to bring more details out of the dark. Capture bright, vivid photos with stunning clarity and more accurate colors.', 'img': 'pic-1-0087.jpg'} },
        { 'info-left': {'title': 'shoot photos like a pro with AI', 'paddingBox': '5vh 30vh 5vh', 'bgcolor': '#0d0f23', 'text': 'With new features like Smart Composition, Auto Smile Capture, and AI Portrait Lighting, artificial intelligence (AI) inside the moto z4 looks for ways to give you the most professional-looking photos possible.', 'img': 'pic-1-0088.jpg'} },
    ],
    'more-2': [
        { 'slider': {'title': 'explore selfie camera features', 'sliders': [
            {'img': 'pic-1-0089.jpg', 'text': '<p><strong>AI Portrait Lighting</strong><br><br>Easily apply unique and artistic lighting effects, like sunshine, stage light, and color pop, to photos taken in portrait mode.</p>'},
            {'img': 'pic-1-0090.jpg', 'text': '<p><strong>Portrait</strong><br><br>Add an artistic blur effect in the background for professional-looking close-up (available on rear and front camera)</p>'},
            {'img': 'pic-1-0091.jpg', 'text': '<p><strong>Auto Smile Capture</strong><br><br>Take the perfect group photo without lifting a finger. The camera analyzes your subjects and automatically captures a photo when everyone in the frame is smiling.</p>'},
        ]} },
    ]
}