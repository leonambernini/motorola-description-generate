// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#0d0f23', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
     // SPECIFICATION MORE
    { 'specification-btn-more': {
        'title': 'specifications', 
        'img': 'pic-1-0150.png', 
        'bgcolor': '#f5f5f5',
        'titlecolor': '#000',
        'col1': '<h5>Protect your phone</h5><p>Chuck your phone in your pocket or bag and go. <strong>moto folio</strong> protects against everyday scratches and bumps.</p><h5>Travel lighter</h5><p>The internal card slot holds one credit card or ID. A secure magnetic closure keeps the case from popping open.</p><h5>Express your style</h5><p>Available in Super Black, Fine Gold, Grape Juice, Felt Grey, Melange Pink, and Oxford Blue designs.</p>',
        'col2': '<h5>Dimensions</h5><p>156.1 x 74 x 3.5 mm</p><h5>Weight</h5><p>55 g</p><h5>Compatibility</h5><p><strong>moto folio</strong> is compatible with any phone in the <a href="http://www.motorola.com/us/products/moto-z-family"><strong style="color: #0090a6">moto z family</strong></a>.</p>',
    }},
    // CARD 3
    { 'cards3': {
        'title': 'go beyond your smartphone', 
        'bgcolor': '#fff',
        'titlecolor': '#000',
        'cards': [
            {'img': 'pic-1-0147.jpg', 'title': 'moto z family', 'text': 'Thinner, lighter, faster premium smartphones that transform to support your interests with Moto Mods.', 'button': '<a href="https://www.motorola.com/us/products/moto-z-family" style="background-color: #fff; border: 1px solid #fff; color: #0090a6;">Learn more</a>', 'bgcolor': '#46c7e1', 'titlecolor': '#fff'},
            {'img': 'pic-1-0148.jpg', 'title': 'moto mods family', 'text': 'Your phone becomes a movie projector, stereo speaker, battery powerhouse, and more, in a snap.', 'button': '<a href="https://www.motorola.com/us/products/moto-mods" style="background-color: #fff; border: 1px solid #fff; color: #0090a6;">Learn more</a>', 'bgcolor': '#46c7e1', 'titlecolor': '#fff'},
            {'img': 'pic-1-0149.jpg', 'title': 'moto mod development kit', 'text': 'Dream. Make. Ship. It’s that simple to bring your ideas to life and transform the smartphone in a new way.', 'button': '<a href="https://developer.motorola.com/" style="background-color: #fff; border: 1px solid #fff; color: #0090a6;">Learn more</a>', 'bgcolor': '#46c7e1', 'titlecolor': '#fff'},
        ]
    }},
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '48 MP rear camera sensor** with 4x greater light sensitivity', 'bgcolor': '#0d0f23', 'text': 'Our largest-ever sensor with Quad Pixel technology combines four pixels into one large 1.6µm pixel for vastly improved low light performance. In other words, you get incredibly sharp 12 MP photos—brighter shots with way less noise.', 'img': 'pic-1-0086.jpg'} },
        { 'info-right': {'title': 'capture the night', 'bgcolor': '#0d0f23', 'text': 'Switch to Night Vision mode in low light conditions to bring more details out of the dark. Capture bright, vivid photos with stunning clarity and more accurate colors.', 'img': 'pic-1-0087.jpg'} },
        { 'info-left': {'title': 'shoot photos like a pro with AI', 'bgcolor': '#0d0f23', 'text': 'With new features like Smart Composition, Auto Smile Capture, and AI Portrait Lighting, artificial intelligence (AI) inside the moto z4 looks for ways to give you the most professional-looking photos possible.', 'img': 'pic-1-0088.jpg'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}