// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#fff', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [

    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'introducing motorola one', 
        'text': 'Big screen entertainment. Stunning portrait shots and selfies. All-day battery plus <strong>turbopower™</strong> charging. And the latest upgrades, innovations, and experiences from <strong>motorola </strong>and Google. So you’re always ready.',
        'paddingBox': '10vh 25px 10vh',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': 'Watch the video', 
        'yt_id': '7AE_eJJU-pc', 
        'btn_icon': 'icon_circleplay_100@2x.png', 
        'bg': 'pic-1-0102.jpg', 
        'height': '1200px',
        'bgcolor': '#000',
        'titlecolor': '#fee500',
        'paddingBox': '0vh 25px 0vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': '', 
        'text': '', 
        'img': 'pic-1-0103.jpg',
        'paddingBox': '0vh 25px 0vh',
        'bgcolor': '#3c287d',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'extraordinary photos', 
        'text': '<a href="https://www.motorola.com/us/products/motorola-one/camera" target="_self" style="border: 2px solid #ff6a00; background-color: #fff; border-radius: 25px; padding: 8px 25px; font-weight: 500 !important; color: #ff6a00;"><span class="cta-library-label">+ More</span></a>', 
        'img': 'pic-1-0104.jpg',
        'paddingBox': '10vh 25px 10vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'powers work and play', 
        'text': '<div style="padding-bottom: 12vh">Go up to a full day on a single charge with the 3000 mAh battery. Recharge with <strong>turbopower™</strong> for up to 6 hours of power in just 20 minutes.<sup>†</sup></div>', 
        'img': 'pic-1-0105.jpg',
        'paddingBox': '5vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'fresh OS', 
        'text': '<div style="padding-bottom: 12vh">Receive upgrades to the latest version of Android.<sup>‡ </sup>You’ll have access to the latest innovations, while knowing your phone is running as smoothly as ever.</div>', 
        'img': 'pic-1-0106.jpg', 
        'paddingBox': '10vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'your phone, fortified', 
        'text': '<div style="padding-bottom: 12vh">Protect against online threats with monthly security updates.<sup>‡</sup> Security is built into every layer of Android One to keep your device and personal data safe. Google Play Protect scans and verifies over 50 billion apps each day, using Google’s machine learning to help keep your phone secure.</div>', 
        'img': 'pic-1-0107.jpg',
        'paddingBox': '12vh 25px 0px', 
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'meet your Google Assistant', 
        'text': '<div style="padding-bottom: 12vh">With the Google Assistant, you can get things done using just your voice. Just start with “Ok Google” to get started, then ask it questions and tell it to do things.</div>', 
        'img': 'pic-1-0108.jpg',
        'paddingBox': '12vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'search what you see', 
        'text': '<div style="padding-bottom: 12vh">Use Google Lens to search what you see, get stuff done faster, and interact with the real world.</div>', 
        'img': 'pic-1-0109.jpg',
        'paddingBox': '12vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'seamless software and app experiences', 
        'text': '',
        'paddingBox': '10vh 25px 5vh',
    }},
    // BLOCO DE SLIDER
    { 'slider': {'title': '', 'paddingBox': '0vh 25px 5vh', 'titlecolor': '#000', 'sliders': [
            {'img': 'pic-1-0110.jpg', 'text': '<p><strong>Google Play</strong></p>'},
            {'img': 'pic-1-0111.jpg', 'text': '<p><strong>Google Duo</strong></p>'},
            {'img': 'pic-1-0112.jpg', 'text': '<p><strong>Moto Actions</strong></p>'},
            {'img': 'pic-1-0113.jpg', 'text': '<p><strong>Moto Display</strong></p>'},
    ]} },
        // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'always ready', 
        'text': '',
        'paddingBox': '15vh 25px 10vh',
    }},
    // BLOCO DE CARDS
    { 'cards': {'title': '', 'paddingBox': '2vh 25px 0px', 'cards': [
        {'img': 'icon-059.png', 'title': 'Performance powerhouse', 'text': 'With 4 GB of memory and a Qualcomm® Snapdragon™ octa-core processor, it responds to your touch in an instant. You’ll feel the difference playing games, watching movies, and doing whatever you need to do.'},
        {'img': 'icon-060.png', 'title': 'Room for more', 'text': 'Google Photos gives you free unlimited photo storage.<sup>†††</sup> And with 64 GB of storage on the phone, there’s plenty of room for songs, videos, and movies. Plus, you can add up to 256 GB with a microSD card.<sup>§&#8203;</sup>'},
        {'img': 'icon-061.png', 'title': 'Dual SIM technology', 'text': 'Dual SIM cards. One smart phone. Use different SIM cards for different carriers to save money. With dual nano SIM support and 4G LTE connectivity, you’ll stay connected at fast speeds and never miss a beat.'},
    ]} },
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'paddingBox': '40px 25px 5vh', 'specifications': [
        {'title': 'Unlocked for GSM carriers', 'text': 'Get the freedom to switch carriers and plans, but keep your <strong>motorola one</strong>. It is unlocked for GSM carriers like T-Mobile and AT&amp;T. Check with your carrier to confirm compatibility.', 'icon': 'icon-062.png'},
        {'title': '5.9" Max Vision (19:9) HD+ display', 'text': 'Immerse yourself in games, books, and movies with a stylish glass design you can hold comfortably with just one hand.', 'icon': 'icon-063.png'},
        {'title': 'Picture perfect cameras', 'text': 'Get creative with the depth-sensing camera’s new photo features, including cinemagraphs and spot color, and capture stunning selfies with blurred backgrounds.', 'icon': 'icon-064.png'},
        {'title': 'Blazing-fast charging', 'text': 'Get up to 6 hours of power in just 20 minutes of <strong>turbopower</strong><sup>™</sup> charging.<sup>†</sup>', 'icon': 'icon-065.png'},
        {'title': 'Android™ upgrades and built-in security', 'text': 'Receive upgrades to the latest Android<sup>™</sup> OS and be ready with new innovations; and stay ready with monthly security updates that help keep your phone secure.<sup>‡</sup>', 'icon': 'icon-066.png'},
        {'title': 'Latest AI-powered innovations', 'text': 'Search what you see with Google Lens, get voice-activated help from the Google Assistant, and more.', 'icon': 'icon-067.png'},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}