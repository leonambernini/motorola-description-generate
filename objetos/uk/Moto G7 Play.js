// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#fff', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [

    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'compact design. huge performance.', 
        'text': '<div style="padding-bottom: 10vh">Dont let the small size fool you. With a Qualcomm<sup>®</sup> Snapdragon<sup>™</sup> 632 octa-core processor and an ultrawide 5.7" Max Vision HD+ display, your games, movies, and photos load fast and look stunning. And you will never miss a moment with a fast-focusing 13 MP camera.</div>',
        'paddingBox': '10vh 25px 0px',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': 'Watch the video', 
        'yt_id': '1RIDM3Ea-c4', 
        'btn_icon': 'icon_circleplay_100@2x.png', 
        'bg': 'pic-2-0107.jpg', 
        'height': '430px',
        'bgcolor': '#fff',
        'titlecolor': '#fff'
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': '60% faster performance', 
        'text': '<div style="padding-bottom: 10vh">Say goodbye to annoying delays and interruptions while playing games or watching videos. With a Qualcomm<sup>®</sup> Snapdragon<sup>™</sup> 632 octa-core processor, <strong>moto g<sup>⁷</sup> play</strong> is 60% faster than before.<sup>†</sup> It’s designed for lag-free performance in everything you do.</div>', 
        'img': 'pic-1-0036.jpg',
        'paddingBox': '10vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'brilliant ultrawide Max Vision display', 
        'text': '<div style="padding-bottom: 10vh">Enjoy expansive views on an edge-to-edge, 5.7” HD+ display that still fits comfortably in your hand. The 19:9 aspect ratio provides a big screen viewing experience, fully immersing you in movies, games, photos, and more.</div>', 
        'img': 'pic-1-0037.jpg',
        'paddingBox': '10vh 25px 0px',
    }},
    // BLOCO TEXTO NA ESQUERDA
    { 'info-left': {
        'title': '<div style="text-align: center">clear and bright photos—anytime, anywhere</div>', 
        'text': '<div style="text-align: center">The 13 MP rear camera uses phase detection autofocus (PDAF) to capture your subject in an instant. And with a built-in flash on the 8 MP front camera, selfies turn out great even in low light conditions.</div>', 
        'img': 'pic-1-0038.jpg', 
        'paddingBox': '0vh 25px 5vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': '40-hour battery with fast charging', 
        'text': '<div style="padding-bottom: 10vh">The 3000 mAh battery lasts more than a full day on a single charge. When you’re low on power, the charger gives you hours of power in just minutes.*</div>', 
        'img': 'pic-1-0039.jpg',
        'paddingBox': '10vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'discreet fingerprint sensor', 
        'text': '<div style="padding-bottom: 10vh">Touch the fingerprint reader to wake up and unlock your phone. The sensor is hidden within the logo on the back.</div>', 
        'img': 'pic-2-0108.jpg',
        'paddingBox': '10vh 25px 0vh',
    }},

    // BLOCO COM EFEITO DE FADE
    { 'effect': {
        'title': 'exclusive moto experiences', 
        'text': 'With One Button Nav, navigate with a virtual bar. With Lift to Unlock, pick up your phone to wake the display. Look for more on the <strong>Moto app</strong>.', 
        'img': 'pic-1-0040.jpg',
        'paddingBox': '0vh 25px 0px',
    }},
    // BLOCO DE CARDS
    { 'cards': {'title': '<div style="padding-bottom: 10vh">more of what you love</div>', 'cards': [
        {'img': 'icon-013.jpg', 'title': 'Expanded storage', 'text': 'Use dual SIM cards.†† Plus, store more photos, songs, and movies with a dedicated microSD card slot.**'},
        {'img': 'icon-014.jpg', 'title': 'Face unlock', 'text': 'With facial recognition software in the front-facing camera, you can unlock your phone with a glance.'},
    ]} },
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'paddingBox': '3vh 25px 0px', 'specifications': [
        {'title': 'Octa-core processor', 'text': 'With a Qualcomm<sup>®</sup> Snapdragon<sup>™</sup> 632 octa-core 1.8GHz processor, it’s 60% faster than before.<sup>†</sup>', 'icon': 'icon-006.png'},
        {'title': '5.7” HD+ Max Vision display', 'text': 'Immerse yourself in the edge-to-edge 5.7" HD+ Max Vision display with a 19:9 aspect ratio.', 'icon': 'icon-005.png'},
        {'title': '13 MP camera with PDAF', 'text': 'Never miss a moment with a fast-focusing 13 MP rear camera and 8 MP front camera with built-in screen flash.', 'icon': 'icon-056.svg'},
        {'title': '40 hours of power', 'text': 'Play all day with a 3000 mAh battery that goes for hours and hours on a single charge.*', 'icon': 'icon-007.png'},
        {'title': 'Fingerprint reader', 'text': 'Touch the fingerprint reader to wake and unlock your phone instantly.', 'icon': 'icon-016.png'},
        {'title': 'motorola-exclusive shortcuts', 'text': 'Use natural gestures to access your favorite features and apps quickly and intuitively with moto experiences.', 'icon': 'icon-009.png'},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}