// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#fff', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [

    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'picture what you can do', 
        'text': 'Introducing <strong>moto g⁶ plus</strong>, with a new smart camera system, a 5.9" Full HD+ Max Vision display with an 18:9 aspect ratio, and a blazing-fast 2.2 GHz processor.',
        'paddingBox': '',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': 'Watch the video', 
        'yt_id': 'CDzFdPhwkZM', 
        'btn_icon': 'icon_circleplay_100@2x.png', 
        'bg': 'pic-2-0120.jpg', 
        'height': '430px',
        'bgcolor': '#fff',
        'titlecolor': '#fee500',
        'paddingBox': '10vh 25px 15vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'the camera with a higher IQ', 
        'text': '<div style="padding-bottom: 10vh">Dual rear cameras focus fast for clear shots anytime, anywhere. <strong>moto g⁶ plus</strong>, also features our new smart camera system with face-unlock and landmark recognition, so it can do way more than just shoot great pics.<br><br><strong><a href="https://www.motorola.co.uk/products/moto-g-plus-gen-6/camera" style="color:#000000; text-decoration: underline">Explore the smart camera</a></strong></div>', 
        'img': 'pic-2-0121.jpg',
        'paddingBox': '13vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'it’s showtime', 
        'text': '<div style="padding-bottom: 10vh">Enjoy videos and games on a breathtaking 5.9" Full HD+ Max Vision display. Playback is more immersive than ever, thanks to a wider 18:9 aspect ratio. And with integrated Dolby Audio<sup>™</sup> preset modes, your favourite content always sounds its best.</div>', 
        'img': 'pic-2-0122.jpg',
        'paddingBox': '13vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'stunning and strong', 
        'text': '<div style="padding-bottom: 10vh"><strong>moto g⁶ plus</strong> is completely wrapped in scratch-resistant Corning<sup>®</sup> Gorilla<sup>®</sup> Glass with a 3D contoured back for a comfortable grip.</div>', 
        'img': 'pic-2-0123.jpg',
        'paddingBox': '13vh 25px 0px',
    }},
     // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'speed things up', 
        'text': '<div style="padding-bottom: 10vh">Enjoy your favourite apps, games, and videos without lag and interruptions on a blazing-fast Qualcomm<sup>®</sup> SnapdragonTM 2.2 GHz octa-core processor. With powerful graphics capabilities and 4G speed, you don’t have to sacrifice video quality for a faster load time.</div>', 
        'img': 'pic-2-0124.jpg',
        'paddingBox': '13vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'make a splash', 
        'text': '<div style="padding-bottom: 10vh">A water-repellent coating helps protect the phone from accidental splashes or light rain.‡</div>', 
        'img': 'pic-1-0101.jpg',
        'paddingBox': '13vh 25px 0px',
    }},
    { 'info-top': {
        'title': 'power moves', 
        'text': '<div style="padding-bottom: 10vh">Stay unplugged for longer. The 3200 mAh battery is designed to last all day. And when it’s time to recharge, the included&nbsp;<strong>turbopower™</strong> charger gives you hours of battery life in just minutes of charging.*</div>', 
        'img': 'pic-2-0125.jpg',
        'paddingBox': '13vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'engineered for your life', 
        'text': '<div style="padding-bottom: 10vh"><strong>moto g⁶ plus&nbsp;</strong>is built to help you access features and content quickly, easily, and securely, so you can have more peace of mind.</div>',
        'paddingBox': '15vh 25px 0px',
    }},
    // BLOCO DE CARDS
    { 'cards': {'title': '', 'paddingBox': '2vh 25px 0px', 'cards': [
        {'img': 'icon-115.jpg', 'title': 'Multi-function fingerprint reader', 'text': 'Use one long press on the fingerprint reader to lock and unlock your phone. Then swipe left to go back, swipe right to access recent apps, or tap once to go home.'},
        {'img': 'icon-053.jpg', 'title': 'Exclusive moto experiences', 'text': 'Need some space? Flip your phone face down to activate do not disturb mode. Need a light? Chop down twice to turn on the torch. <strong>moto actions</strong> make life easy.'},
        {'img': 'icon-019.jpg', 'title': 'Updates at a glance', 'text': '<strong>moto display </strong>gives you a quick preview of notifications and updates, so you can see what’s going on without unlocking your phone. You can even reply to messages right from the lockscreen.<sup>§§</sup>'},
    ]} },
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'paddingBox': '20vh 25px 5vh', 'specifications': [
        {'title': 'New smart camera system', 'text': 'Sharp pictures in any light, plus built-in intelligence that recognises landmarks and face-unlocks your phone.', 'icon': 'icon-116.svg'},
        {'title': 'Designed to impress', 'text': 'Enjoy your favourite content on a stunning new edge-to-edge 5.9" Max Vision display with 18:9 aspect ratio.', 'icon': 'icon-117.svg'},
        {'title': 'Faster than the speed of life', 'text': 'Enjoy your favourite apps and games without lag on powerful Qualcomm<sup>®</sup> Snapdragon<sup>™</sup> 2.2 GHz octa-core processor.', 'icon': 'icon-006.png'},
        {'title': 'All-day battery + turbopower™ charging', 'text': 'Stay unplugged for longer, then get hours of power in just minutes of charging.*', 'icon': 'icon-022.png'},
        {'title': 'Multi-function fingerprint reader', 'text': 'Unlock your phone and make mobile payments at participating retailers with just a touch.**', 'icon': 'icon-016.png'},
        {'title': 'Goof-proof design', 'text': 'A water-repellent coating helps protect the phone, inside and out.‡', 'icon': 'icon-024.png'},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}