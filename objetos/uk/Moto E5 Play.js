// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#fff', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'big screen. slim body.', 
        'text': '<div style="padding-bottom: 10vh">Packed into a compact design, the 5.3" Max Vision display gives you an expansive view with a narrow bezel. Unlike traditional displays, Max Vision has a unique 18:9 aspect ratio, so you can see more at a glance. Do less scrolling on websites. Read articles and blogs on a single screen. And get a crazy-wide landscape view of your favorite games.</div>', 
        'img': 'pic-2-0115.jpg',
        'paddingBox': '10vh 25px 7vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'smart and secure', 
        'text': '<div style="padding-bottom: 12vh">Touch the fingerprint reader to wake up, unlock, and lock your phone instantly. It’s discreetly located within the phone’s iconic logo, so you hardly know it’s there until you use it.</div>', 
        'img': 'pic-2-0116.jpg',
        'paddingBox': '7vh 25px 7vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'clear pics. super selfies.', 
        'text': '<div style="padding-bottom: 12vh">You’re always seen in the best light with the 5 MP selfie cam and flash. Plus, the 8 MP rear-facing camera focuses in an instant, so you don’t miss a moment. Your photos look great indoors and on dark cloudy days, thanks to auto enhancement.</div>', 
        'img': 'pic-1-0118.jpg',
        'paddingBox': '7vh 25px 7vh',
    }},
        // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'super sweet features', 
        'text': '',
        'paddingBox': '7vh 25px 7vh',
    }},
    // BLOCO DE CARDS
    { 'cards': {'title': '', 'paddingBox': '2vh 25px 0px', 'cards': [
        {'img': 'icon-105.jpg', 'title': 'Android™ Oreo™ (Go edition)', 'text': 'Save space with fewer pre-installed apps, smaller app sizes for many popular apps, and more storage. Enjoy a built-in data saver, plus built-in protection with Google Play Protect.'},
        {'img': 'icon-106.jpg', 'title': 'Dedicated microSD card slot', 'text': 'Take more of your favorite media with you—and still carry a second SIM card. Thanks to a dedicated microSD card slot, you can add up to 128 GB of photos, songs, and movies.†'},
        {'img': 'icon-107.jpg', 'title': 'Qualcomm® Snapdragon™ quad-core processor', 'text': 'When it comes to performance, we don’t play games. With a Qualcomm® Snapdragon™ quad-core processor, you can feel the difference in games, music, and videos.'},
    ]} },
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'paddingBox': '20vhpx 25px 5vh', 'specifications': [
        {'title': '5.3" Max Vision display', 'text': 'Get a big screen in a slim body for a better view of websites, games, and more.', 'icon': 'icon-108.svg'},
        {'title': 'Fingerprint reader', 'text': 'Never mind remembering a passcode. Your fingerprint unlocks your phone.', 'icon': 'icon-109.svg'},
        {'title': '8 MP camera with autofocus', 'text': 'Capture sharp photos even in low light conditions—and even when there’s no time to focus.', 'icon': 'icon-110.svg'},
        {'title': 'Android™ Oreo™ (Go edition)', 'text': 'Enjoy the best of Android for better performance, peace of mind, and protection.', 'icon': 'icon-111.svg'},
        {'title': 'Expandable storage', 'text': 'Make room for more photos, songs, and videos thanks to an additional microSD card slot.†', 'icon': 'icon-112.svg'},
        {'title': 'Fast performance', 'text': 'Get all the speed you need with a quad-core processor.', 'icon': 'icon-113.svg'},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}