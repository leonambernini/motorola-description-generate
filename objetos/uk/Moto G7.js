// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#fff', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [

    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'see it. capture it. interact with it.', 
        'text': '<div style="padding-bottom: 10vh">Capture creative portraits and outdoor scenes with depth effects on a 12 MP dual camera system. Search what you see with Google Lens™. Plus, enjoy an ultrawide 6.2" Max Vision Full HD+ display.</div>',
        'paddingBox': '10vh 25px 0px',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': 'Watch the video', 
        'yt_id': '1aiMLNtIKM8', 
        'btn_icon': 'icon_circleplay_100@2x.png', 
        'bg': 'pic-2-0109.jpg', 
        'height': '430px',
        'bgcolor': '#fff',
        'titlecolor': '#fff'
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'stunning re-engineered Max Vision display', 
        'text': '<div style="padding-bottom: 10vh">Enjoy maximum big screen entertainment on a 6.2" Full HD+ display—our largest ever on a <strong>moto g</strong>. The 19:9 aspect ratio and new minimalist U design create an ultrawide, immersive viewing experience for movies, games, photos, and more.</div>', 
        'img': 'pic-1-0006.jpg',
        'paddingBox': '10vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'striking portraits. stunning low-light shots.', 
        'text': '<div style="padding-bottom: 10vh">Capture artistic depth effects with a 12 MP dual camera system, big 1.25um pixels, a large f/1.8 aperture, and creative photo software. Plus, take better low-light selfies with an 8 MP front camera and a new screen flash.</div>', 
        'img': 'pic-1-0001.jpg',
        'paddingBox': '10vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'introducing high res zoom', 
        'text': '<div style="padding-bottom: 10vh">High res zoom automatically restores the details and image clarity that are typically lost with digital zooming.§§ So your photos look clear and detailed, even when subjects are far away.<br><strong>See the difference below: without (left) vs. with (right). </strong></div>', 
        'img': 'pic-1-0002.jpg',
        'paddingBox': '10vh 25px 0px',
    }},
    // BLOCO TEXTO NA ESQUERDA
    { 'info-left': {
        'title': '<div style="text-align: center">AI-powered camera software: your built-in photo assistant</div>', 
        'text': '<div style="text-align: center">Group photos are easier than ever with auto smile capture, an algorithm that triggers the shot when everyone in the frame is smiling.** Plus, with Google Lens™, it’s like having an encyclopedia in your viewfinder.</div>', 
        'img': 'pic-1-0003.jpg', 
        'paddingBox': '0vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'camera feature: screen flash', 
        'text': '<div style="padding-bottom: 10vh">Compensate for low-light with a screen flash that triggers automatically for bright, clear selfies.</div>', 
        'img': 'pic-1-0004.jpg',
        'paddingBox': '10vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'camera feature: cinemagraph', 
        'text': 'Make cinemagraphs, keeping a portion of your shot in motion while freezing everything else.',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': 'Watch the video', 
        'yt_id': '7cHuqHxveDM', 
        'btn_icon': 'icon_circleplay_100@2x.png', 
        'bg': 'pic-1-0005-2.jpg',
        'height': '430px',
        'bgcolor': '#fff',
        'titlecolor': '#fff'
    }},
    // BLOCO DE SLIDER COM VÍDEO
    { 'slider': {'title': 'explore even more <span style="color:#49bbd4">moto g⁷</span> camera features', 'titlecolor': '#000', 'sliders': [
            {'img': 'pic-2-0110.jpg', 'yt_id': 'LinKACQWwVE', 'btn_icon': 'icon_circleplay_100@2xbranco.png', 'text': '<p><strong>timelapse</strong><br><br>Shoot dramatic timelapse videos with all the action whizzing past at 4x, 8x, 16x, or 32x speed.</p>'},
            {'img': 'pic-2-0111.jpg', 'yt_id': 'p7r8K0x3OwM', 'btn_icon': 'icon_circleplay_100@2xbranco.png', 'text': '<p><strong>hyperlapse</strong><br><br>Shoot a timelapse video in motion and smooth out any unwanted shakiness with post-capture stabilisation.</p>'},
            {'img': 'pic-2-0112.jpg', 'text': '<p><strong>portrait mode</strong><br><br>Add an artistic blur effect in the background for professional-looking close-up or portrait shots.</p>'},
            {'img': 'pic-2-0113.jpg', 'text': '<p><strong>Google Lens</strong><br><br>Recognise landmarks, objects, foreign languages, and more with Google Lens.</p>'},
            {'img': 'pic-3-0009.jpg', 'text': '<p><strong>Google Photos</strong><br><br>Free, unlimited, high-quality storage with Google Photos.<sup>§</sup></p>'},
    ]} },
        // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': '50% faster performance', 
        'text': '<div style="padding-bottom: 10vh">Feel a boost in performance in everything you do, from editing videos, to playing 3D games. With a responsive Qualcomm<sup>®</sup> Snapdragon<sup>™</sup> 632 octa-core processor, <strong>moto g⁷</strong> is 50% faster than the previous generation.<sup>‡‡</sup></div>', 
        'img': 'pic-1-0008.jpg',
        'paddingBox': '5vh 25px 7vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'all-day battery + <span style="color:#49bbd4">turbopower™</span> charging', 
        'text': '<div style="padding-bottom: 10vh">Get 9 hours of power in just 15 minutes of charging. After a full charge, the 3000 mAh battery lasts all day.*</div>', 
        'img': 'pic-1-0007.jpg',
        'paddingBox': '10vh 25px 7vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'water-repellent. scratch-resistant.', 
        'text': '<div style="padding-bottom: 10vh">Enjoy a comfortable grip with 3D contoured Corning® Gorilla® Glass that protects both sides of the phone against nicks and scratches. A water-repellent design protects against accidental spills, sweat, and light rain.†</div>', 
        'img': 'pic-1-0009.jpg',
        'paddingBox': '10vh 25px 7vh',
    }},
    // BLOCO COM EFEITO DE FADE
    { 'effect': {
        'title': 'exclusive moto experiences', 
        'text': 'With One Button Nav, navigate with a virtual bar. With Lift to Unlock, pick up your phone to wake the display. Look for more on the <strong>Moto app</strong>.', 
        'img': 'pic-1-0010.jpg',
        'paddingBox': '0vh 25px 7vh',
    }},
    // BLOCO DE CARDS
    { 'cards': {'title': 'more of what you love', 'cards': [
        {'img': 'icon-104.jpg', 'title': 'Mobile payments', 'text': 'Making secure purchases is fast and easy. Just place your phone near an NFC terminal and use your fingerprint.**'},
        {'img': 'icon-001.jpg', 'title': 'Expanded storage', 'text': 'Use dual SIM cards.††† Plus, store more photos, songs, and movies with a dedicated microSD card slot.††'},
        {'img': 'icon-002.jpg', 'title': 'Face unlock + fingerprint reader', 'text': 'Unlock your phone with facial recognition software in the front-facing camera, or use the fingerprint reader on the back.'},
        {'img': 'icon-003.jpg', 'title': 'personal audio. professional sound.', 'text': 'Dolby Audio<sup>™</sup> gives you more control over your sound preferences with preset modes for all sorts of content, from music to movies.'},
    ]} },
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'your best shot', 
        'text': '<div style="padding-bottom: 0vh">Here are some of our favorite clicks from everyday <strong>moto g⁷</strong> photographers just like you.</div>',
        'paddingBox': '10vh 25px 0vh',
    }},
    // BLOCO DE SLIDER
    { 'slider': {'title': '', 'titlecolor': '#000', 'paddingBox': '0vh 25px 0vh', 'sliders': [
            {'img': 'pic-1-0011.jpg', 'text': '<p>Photo by: Jacqueline Marques, Finance Analyst, São Paulo, Brazil</p>'},
            {'img': 'pic-1-0012.jpg', 'text': '<p>Photo by: Natalia Falcao, Globalization QA Lead, Sao Paolo, Brazil</p>'},
            {'img': 'pic-1-0013.jpg', 'text': '<p>Photo by: Keith Gifford, Software Engineer, North Carolina, USA</p>'},
            {'img': 'pic-1-0014.jpg', 'text': '<p>Photo by: Sunny Joseph Anthony Cruz, Social Media Analyst, Bangalore, India</p>'},
            {'img': 'pic-1-0015.jpg', 'text': '<p>Photo by: Varun Shrivastava, Software Engineer, Bangalore, India</p>'},
    ]} },
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'specifications': [
        {'title': '12 MP + 5 MP dual camera system', 'text': 'Go beyond photos with a creative 12 MP + 5 MP dual camera system, creative photo software, and Google Lens™.', 'icon': 'icon-004.png'},
        {'title': '6.2" Full HD+ Max Vision display', 'text': 'Our largest <strong>moto g</strong> display ever has a 19:9 aspect ratio for a widescreen viewing experience with movies and games.', 'icon': 'icon-005.png'},
        {'title': '50% faster performance', 'text': 'An octa-core Qualcomm® Snapdragon™ processor gives a performance boost in everything you do.<sup>‡‡</sup>', 'icon': 'icon-006.png'},
        {'title': '15W turbopower™ charging', 'text': 'Get 9 hours of power in just 15 minutes, and go all day on a full charge.*', 'icon': 'icon-007.png'},
        {'title': 'Water-repellent, 3D glass design', 'text': 'Enjoy a comfortable grip with a scratch-resistant, contoured 3D Corning® Gorilla® Glass design.<sup>†</sup>', 'icon': 'icon-008.png'},
        {'title': 'motorola-exclusive shortcuts', 'text': 'Use natural gestures to access your favorite features and apps quickly and intuitively with <strong>moto experiences</strong>.', 'icon': 'icon-009.png'},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}