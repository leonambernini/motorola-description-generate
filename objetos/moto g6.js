// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#fff', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [

    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'envision the possibilities', 
        'text': '<div style="padding-bottom: 7vh">Meet <strong>moto g⁶</strong>. With a 5.7" Full HD+ Max Vision display, advanced imaging software, and a long-lasting battery,* it’s impressive any way you look at it.</div>',
        'paddingBox': '7vh 25px 0vh',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': '<strong><div style="color: #fee500">Watch the video</div></strong>', 
        'yt_id': '20vwZ-ipZfM', 
        'btn_icon': 'icon_circleplay_100@2x.png', 
        'bg': 'pic-1-0041.jpg', 
        'height': '430px',
        'bgcolor': '#fff',
        'titlecolor': '#fee500',
        'paddingBox': '7vh 25px 0vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'more screen, less bezel', 
        'text': '<div style="padding-bottom: 7vh">With a new edge-to-edge Full HD+ Max Vision display, nothing stands between you and your favorite content. Immerse yourself in games and movies with an ultra-wide 18:9 aspect ratio. And with integrated Dolby Audio™ preset modes, your favorite content always sounds its best.</div>', 
        'img': 'pic-4-0004.jpg',
        'paddingBox': '10vh 25px 0vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'power play', 
        'text': '<div style="padding-bottom: 7vh">Get through a full day on a single charge with a 3000 mAh battery. When it’s time to power up, don’t slow down. The included <strong>turbopower™</strong> charger gives you hours of battery life in just minutes of charging.<sup>*</sup></div>', 
        'img': 'pic-4-0007.jpg',
        'paddingBox': '10vh 25px 0vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'studio-quality portraits, and then some', 
        'text': '<div style="padding-bottom: 7vh">Unleash your inner shutterbug with dual rear cameras and advanced photo software. Shoot timelapse video. Take wide-angle selfies with fun face filters on the 8 MP front camera. Plus, use the smart camera system to learn more about objects and landmarks around you.<br><br><strong><a href="https://www.motorola.com/us/products/moto-g-gen-6/camera" style="color:#000000;">Explore all camera features</a></strong></div>', 
        'img': 'pic-1-0044.jpg',
        'paddingBox': '10vh 25px 0vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'faster streaming, less lag', 
        'text': '<div style="padding-bottom: 7vh">Enjoy your favorite apps, games, and videos without lags and interruptions on a blazing-fast Qualcomm<sup>®</sup> Snapdragon™ 1.8 GHz octa-core processor. With powerful graphics capabilities and 4G speed, you don’t have to sacrifice video quality for a faster load time.</div>', 
        'img': 'pic-4-0005.jpg',
        'paddingBox': '10vh 25px 0vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'sleek and sturdy', 
        'text': '<div style="padding-bottom: 13vh"><strong>moto g⁶ </strong>is completely wrapped in scratch-resistant Corning® Gorilla® Glass with a 3D contoured back for a comfortable grip.</div>', 
        'img': 'pic-1-0046.jpg',
        'paddingBox': '10vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'make a splash', 
        'text': '<div style="padding-bottom: 13vh">A water-repellent coating helps protect the phone from accidental splashes or light rain.<sup>‡</sup></div>', 
        'img': 'pic-4-0006.jpg',
        'paddingBox': '10vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'engineered for your life', 
        'text': '<div style="padding-bottom: 3vh"><strong>moto g⁶</strong> is built to help you access features and content quickly, easily, and securely, so you can have more peace of mind.</div>',
        'paddingBox': '15vh 25px 0vh',
    }},
    // BLOCO DE CARDS
    { 'cards': {'title': '', 'paddingBox': '4vh 25px 10vh', 'cards': [
        {'img': 'icon-017.jpg', 'title': 'Multi-function fingerprint reader', 'text': 'Use one long press on the fingerprint reader to lock and unlock your phone. Then swipe left to go back, swipe right to access recent apps, or tap once to go home.'},
        {'img': 'icon-018.jpg', 'title': 'Exclusive moto experiences', 'text': 'Need some space? Flip your phone face down to activate do not disturb mode. Need a light? Chop down twice to turn on the torch. <strong>moto actions</strong> make life easy.<sup>‡‡</sup>'},
        {'img': 'icon-019.jpg', 'title': 'Updates at a glance', 'text': '<strong>moto display</strong> gives you a quick preview of notifications and updates, so you can see what’s going on without unlocking your phone. You can even reply to messages right from the lockscreen.<sup>§§</sup>'},
    ]} },
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'paddingBox': '40px 25px 5vh', 'specifications': [
        {'title': 'Edge-to-edge display', 'text': 'Enjoy your favorite entertainment on a new 5.7" Full HD+ display with an 18:9 aspect ratio.', 'icon': 'icon-020.png'},
        {'title': 'Advanced imaging software', 'text': 'Unleash your creativity with dual rear cameras and tools for everything from stunning portrait shots to hilarious face filters.', 'icon': 'icon-021.png'},
        {'title': 'All-day battery + turbopower™ charging', 'text': 'Stay unplugged longer, then get hours of power in just minutes of charging.*', 'icon': 'icon-022.png'},
        {'title': 'Powerful performance', 'text': 'Keep it moving with a 1.8 GHz octa-core processor and 4G speed.', 'icon': 'icon-023.png'},
        {'title': 'Multi-function fingerprint reader', 'text': 'Instantly unlock your phone with just a touch.**', 'icon': 'icon-016.png'},
        {'title': 'Goof-proof design', 'text': 'A water-repellent coating helps protect the phone, inside and out.<sup>‡</sup>', 'icon': 'icon-024.png'},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}