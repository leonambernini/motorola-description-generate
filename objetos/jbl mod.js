// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#0d0f23', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // MOSAIC
    { 'mosaic': {
        'title': '<div style="padding-top: 2vh; padding-bottom: 2vh">high-quality JBL audio, in a snap</div>',
        'items': [
            {'width': '50%', 'height': '700px', 'bg': 'pic-2-0073.jpg', 'title': 'JBL audio', 'text': 'Give your phone—and the party—a boost. Powerful stereo sound. Instantly.', 'btn': 'Learn more'},
            {'width': '50%', 'height': '350px', 'bg': 'pic-2-0074.jpg', 'title': 'features', 'text': 'JBL SoundBoost comes with everything you need to keep the volume up, longer', 'btn': 'Learn more'},
            {'width': '50%', 'height': '350px', 'bg': 'pic-2-0075.jpg', 'title': 'moto mods™', 'text': 'Your phone becomes a movie projector, a boombox, a battery powerhouse, and more. In a snap.', 'btn': 'Learn more'},
        ],
        'modalbg': '#ff6901',
        'modalcolor': '#fff',
        'modalBtnClose': 'icon_xCLose_50.svg',
        'modalItems': [
            {'model': 'text-image', 'title': 'high-quality JBL audio. anywhere.', 'text': 'The JBL SoundBoost speaker snaps easily onto your Moto Z so you can immediately pump up the volume with powerful stereo sound. No pairing. No hassle. Perfect for the beach, the backyard, and everywhere in between.', 'img': 'pic-2-0076.jpg', 'colText': '5', 'bg': '#6ac347', 'colImage': '7'},
            {'model': 'text-image', 'title': 'immersive audio-visual experience', 'text': 'With a battery boost and a built-in kickstand, prop up your phone and keep the music or videos going longer. JBL SoundBoost includes 10 hours of battery life so you can listen longer without using your phone`s battery.†', 'img': 'pic-2-0077.jpg', 'colText': '5', 'bg': '#6ac347', 'colImage': '7'},
            {'model': 'text-movie', 'title': 'transformation’s never been easier', 'text': 'With Moto Mods™ like JBL SoundBoost, do things you never thought were possible with a phone. The interchangeable backs snap onto your phone with powerful integrated magnets, so you can make your imagination reality. Whatever your passion, there’s a Moto&nbsp;Mod for you. <a href="http://www.motorola.com/us/moto-mods" style="text-decoration:underline; color:#fff">Learn more</a>', 'img': '', 'bg': '#6ac347', 'colText': '5', 'colImage': '7', 'movie': {
                'title': '', 
                'yt_id': 'OBVCZ-P1GCk', 
                'btn_icon': 'icon_circleplay_100@2x.png', 
                'bg': 'pic-2-0078.jpg', 
                'height': '100%',
                'bgcolor': '#2d2a26',
                'titlecolor': '#fff',
            }},
        ],
    }},

    // SPECIFICATION MORE
    { 'specification-btn-more': {
        'title': 'specifications', 
        'img': 'pic-2-0079.jpg', 
        'bgcolor': '#fff',
        'titlecolor': '#000',
        'col1': '<h5>works with any moto z</h5><p class="p1">Compatible with the <a href="http://www.motorola.com/us/products/moto-z-family" style="color: #0090a6; text-decoration: underline">Moto Z Family</a> of phones.</p><h5>high-quality JBL&nbsp;audio</h5><p class="p1">Give your smartphone a boost with powerful stereo sound.</p><h5>listen in a snap</h5><p class="p1">Simply snap on the speaker and it plays instantly. No pairing. No hassle.</p>',
        'col2': '<h5>built-in kickstand</h5><p class="p1" style="margin-bottom: 10px;">Prop up your smartphone to make videos and music more immersive.</p><h5>built-in battery</h5><p class="p1">JBL SoundBoost includes 10 hours of battery life so you can listen longer without using your phone`s battery.<sup>†</sup></p><h5>speakerphone</h5><p class="p1">Calls come in loud and clear wherever you are.</p>',
        'more': {
            'img': 'pic-2-0080.jpg', 
            'bgcolor': '#fff',
            'titlecolor': '#000',
            'col1': '<h5>compatible phones</h5><p>The <a href="http://www.motorola.com/us/products/moto-z-family" style="color: #0090a6; text-decoration: underline;">Moto Z Family</a> of phones</p><h5>dimensions</h5><p class="p1">152 x 73 x 13 mm</p><h5>weight</h5><p class="p1">145 g</p><h5>number of speakers</h5<p class="p1">2 speakers @ 27 mm diameter, stereo sound</p><h5>speaker power</h5><p class="p1">3W per speaker, 6W total</p><h5>frequency response range</h5><p class="p1">200 Hz—20 kHz</p><h5>loudness</h5><p class="p1">80 dBSPL @ 0,5 m</p>',
            'col2': '<h5>speakerphone support</h5><p class="p1">Yes</p><h5>integrated battery</h5><p class="p1">Yes</p><h5>battery size</h5><p class="p1">1000 mAh</p><h5>battery life</h5><p class="p1">10 hours</p><h5>external charging</h5><p class="p1">USB Type C</p><h5>charge rate</h5><p class="p1">1A, 5W</p>',
        }
    }},
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '48 MP rear camera sensor** with 4x greater light sensitivity', 'bgcolor': '#0d0f23', 'text': 'Our largest-ever sensor with Quad Pixel technology combines four pixels into one large 1.6µm pixel for vastly improved low light performance. In other words, you get incredibly sharp 12 MP photos—brighter shots with way less noise.', 'img': 'pic-1-0086.jpg'} },
        { 'info-right': {'title': 'capture the night', 'bgcolor': '#0d0f23', 'text': 'Switch to Night Vision mode in low light conditions to bring more details out of the dark. Capture bright, vivid photos with stunning clarity and more accurate colors.', 'img': 'pic-1-0087.jpg'} },
        { 'info-left': {'title': 'shoot photos like a pro with AI', 'bgcolor': '#0d0f23', 'text': 'With new features like Smart Composition, Auto Smile Capture, and AI Portrait Lighting, artificial intelligence (AI) inside the moto z4 looks for ways to give you the most professional-looking photos possible.', 'img': 'pic-1-0088.jpg'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}