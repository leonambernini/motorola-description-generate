// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#0d0f23', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // MOSAIC
    { 'mosaic': {
        'title': '<div style="padding-bottom: 2vh; padding-top: 2vh">amazon apps + more at your fingertips</div>',
        'items': [
            {'width': '50%', 'height': '700px', 'bg': 'pic-2-0088.jpg', 'title': 'amazon features', 'text': 'Designed for Amazon Prime members', 'btn': 'Learn more'},
            {'width': '50%', 'height': '700px', 'bg': 'pic-2-0089.jpg', 'title': 'moto features', 'text': 'Captivating design. Unlimited performance. Unlocked for major U.S. carriers.', 'btn': 'Learn more'},
        ],
        'modalbg': '#ff6901',
        'modalcolor': '#fff',
        'modalBtnClose': 'icon_xCLose_50.svg',
        'modalItems': [
            {'model': 'text-image', 'title': 'calling all amazon prime members', 'text': 'The Amazon Prime Exclusive Moto G⁵ Plus is perfect for the ultimate Prime member and comes at a special price.', 'img': 'pic-2-0090.jpg', 'colText': '5', 'bg': '#6ac347', 'colImage': '7'},
            {'model': 'cards', 'title': '', 'text': '', 'cards': [
                {'img': 'pic-2-0091.jpg', 'title': 'precision-crafted metal design', 'text': 'The new Moto G Plus comes with a high-grade aluminum exterior that’s been diamond cut and finished to perfection. It looks as great as it performs.', 'bg': '#6ac347', 'col': '4'},
                {'img': 'pic-2-0092.jpg', 'title': 'most advanced camera in its class', 'text': 'The new Moto G Plus is the first camera in its class with Dual Autofocus Pixels. Utilizing 10 times more pixels on the sensor, the camera is able to lock onto your subject up to 60% faster than the previous generation Moto G Plus. Fewer missed shots, more stunning photos.', 'bg': '#6ac347', 'col': '4'},
                {'img': 'pic-2-0093.jpg', 'title': 'unlocked and carrier-friendly', 'text': 'Go further with a phone that is unlocked and ready to go. Without being tied to one particular carrier, you have the freedom to take your phone to any major U.S. carrier you want.', 'bg': '#6ac347', 'col': '4'},
            ]},
        ],
    }},

    // SPECIFICATION MORE
    { 'specification-btn-more': {
        'title': 'specifications', 
        'img': 'pic-2-0094.jpg', 
        'bgcolor': '#fff',
        'titlecolor': '#000',
        'col1': '<h5>designed for amazon prime members</h5><p class="p1">Integrated Amazon experience and seamless access to Prime benefits.</p><h5>outstanding cameras</h5><p class="p1">The 12 MP rear camera focuses up to 60% faster than ever before.* Switch to the wide-angle front camera for group selfies.</p><h5>precision-crafted metal design</h5><p class="p1">One of the first new Moto G phones made from high grade aluminum, it looks as great as it performs.</p>',
        'col2': '<h5>fuel up fast</h5><p class="p1">All-day battery and up to 6 hours of battery life in just 15 minutes with TurboPower™ charging.<sup>†</sup></p><h5>fast octa-core processor</h5><p class="p1">Apps run smoothly thanks to a blazing-fast Qualcomm<sup>®</sup> Snapdragon™ 2.0 GHz octa-core processor.</p><h5>fingerprint reader</h5><p class="p1">Instantly unlock your phone. No passcode required.</p>',
        'more': {
            'img': 'pic-2-0087.jpg', 
            'bgcolor': '#fff',
            'titlecolor': '#000',
            'col1': '<h5>carrier network</h5><p class="p1">Moto G⁵ Plus Amazon Prime Exclusive is unlocked for all major U.S. networks.</p><h5>operating system</h5><p class="p1">Android™ 7.0, Nougat</p><h5>system architecture/processor</h5><p class="p1">Qualcomm® Snapdragon™ 625 processor with 2.0 GHz octa-core CPU and 650 MHz Adreno 506 GPU</p><h5>memory (RAM)</h5><p class="p1">2 GB/4 GB</p><h5>storage (ROM)</h5><p class="p1">32 GB/64 GB internal, up to 128 GB microSD Card support</p><h5>dimensions</h5><p class="p1">Height: 150.2 mm<br>Width: 74.0 mm<br>Depth: 7.7 mm to 9.7 mm</p><h5>weight</h5><p class="p1">155 g</p><h5>display</h5><p class="p1">5.2”<br>Full HD 1080p (1920 x 1080)<br>424 ppi<br>Corning™ Gorilla™ Glass 3</p><h5>battery</h5><p class="p1">All-day battery<sup>†</sup> (3000 mAh)<br>TurboPower™ for up to 6 hours of power in 15 minutes of charging<sup>†</sup></p><h5>water protection</h5><p class="p1">Water repellent nano-coating<sup>‡</sup></p><h5>networks</h5><p class="p1">4G LTE (Cat 6)<br>CDMA/EVDO Rev A<br>UMTS/HSPA+<br>GSM/EDGE</p><h5>bands (by model)</h5><p class="p1">Moto G Plus - XT1687<br>CDMA (850, 850+,1900 MHz)<br>GSM/GPRS/EDGE (850, 900, 1800, 1900 MHz)<br>UMTS/HSPA+ (850, 900, 1700, 1900, 2100 MHz)<br>4G LTE (B1, 2, 3, 4, 5, 7, 8, 12, 13, 17, 25, 26, 38, 41, 66)</p><p class="p1">Band coverage varies by model, country, and carrier.</p>',
            'col2': '<h5><h5>rear camera</h5><p class="p1">12 MP with Dual Autofocus Pixels<br>ƒ/1.7 aperture<br>Color balancing dual LED flash<br>8X digital zoom for photos, 4X for video<br>Drag to focus &amp; exposure<br>Quick Capture<br>Tap (anywhere) to capture<br>Best Shot<br>Professional Mode<br>Burst mode<br>Auto HDR<br>Panorama<br>Video stabilization<br>4K Ultra HD video capture (30 fps)<br>Slow Motion video</p><h5>front camera</h5><p class="p1">5 MP<br>Wide-angle lens<br>ƒ/2.2 aperture<br>Display flash<br>Professional mode<br>Beautification mode</p><h5>video capture</h5><p class="p1">4K Ultra HD (30 fps)</p><h5>speakers/microphones</h5><p class="p1">Front-ported loud speaker<br>2-Mics</p><h5>SIM Card</h5><p class="p1">Nano-SIM</p><h5>connectivity</h5><p class="p1">Micro USB, 3.5 mm headset jack</p><h5>Bluetooth® technology</h5><p class="p1">Bluetooth version 4.2</p><h5>Wi-Fi</h5><p class="p1">802.11 a/b/g/n (2.4 GHz + 5 GHz)</p><h5>location services</h5><p class="p1">GPS<br>A-GPS<br>GLONASS</p><h5>NFC</h5><p class="p1">No</p><h5>sensors</h5><p class="p1">Fingerprint reader<br>Acceleromter<br>Gyroscope<br>Magnetometer<br>Ambient Light<br>Proximity</p><h5>base color</h5><p class="p1">Lunar Gray</p>',
        }
    }},
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '48 MP rear camera sensor** with 4x greater light sensitivity', 'bgcolor': '#0d0f23', 'text': 'Our largest-ever sensor with Quad Pixel technology combines four pixels into one large 1.6µm pixel for vastly improved low light performance. In other words, you get incredibly sharp 12 MP photos—brighter shots with way less noise.', 'img': 'pic-1-0086.jpg'} },
        { 'info-right': {'title': 'capture the night', 'bgcolor': '#0d0f23', 'text': 'Switch to Night Vision mode in low light conditions to bring more details out of the dark. Capture bright, vivid photos with stunning clarity and more accurate colors.', 'img': 'pic-1-0087.jpg'} },
        { 'info-left': {'title': 'shoot photos like a pro with AI', 'bgcolor': '#0d0f23', 'text': 'With new features like Smart Composition, Auto Smile Capture, and AI Portrait Lighting, artificial intelligence (AI) inside the moto z4 looks for ways to give you the most professional-looking photos possible.', 'img': 'pic-1-0088.jpg'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}