// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#0d0f23', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'stereo on the go', 
        'text': 'Snap it onto your <strong>moto z</strong> and press play. No pairing, no cords.<br>Enjoying powerful stereo sound on your smartphone is that simple.',
        'paddingBox': '11vh 25px 5vh',
    }},
    // CARD 3
    { 'cards3': {
        'title': '', 
        'bgcolor': '#fff',
        'titlecolor': '#000',
        'cards': [
            {'img': 'pic-1-0143.jpg', 'title': '<span style="text-align: center; display: block">Loud and clear</span>', 'text': '<span style="text-align: center; display: block">Hands-free speakerphone calls and video chats have better vocal clarity.</span>', 'button': '', 'bgcolor': '#fff', 'titlecolor': '#000'},
            {'img': 'pic-1-0144.jpg', 'title': '<span style="text-align: center; display: block">Immersive audio</span>', 'text': '<span style="text-align: center; display: block">The built-in kickstand creates an immersive audio-visual experience so you can kick back and watch a movie.</span>', 'button': '', 'bgcolor': '#fff', 'titlecolor': '#000'},
            {'img': 'pic-1-0145.jpg', 'title': '<span style="text-align: center; display: block">Keep going</span>', 'text': '<span style="text-align: center; display: block">Your phone’s battery powers the speaker, so you never need to charge it.</span>', 'button': '', 'bgcolor': '#fff', 'titlecolor': '#000'},
        ]
    }},
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'paddingBox': '40px 25px 5vh', 'specifications': [
        {'title': 'Powerful stereo sound', 'text': 'Enjoy music and videos with instantly louder audio and richer bass.', 'icon': 'icon-087.svg'},
        {'title': 'Built-in kickstand', 'text': 'Prop up your phone to experience more immersive music and movies.', 'icon': 'icon-088.svg'},
        {'title': 'No charging required', 'text': 'Your phone’s battery powers the speaker.', 'icon': 'icon-089.svg'},
        {'title': 'No pairing needed', 'text': 'Just snap it on and crank up the sound. No hassle.', 'icon': 'icon-090.svg'},
        {'title': 'Loud and clear speakerphone calls', 'text': 'Go hands-free without compromising vocal clarity.', 'icon': 'icon-091.svg'},
        {'title': 'Works on any moto z', 'text': 'Get stereo sound, just like that, with any phone in the <a href="https://www.motorola.com/us/products/moto-z-family" style="color:#000000;"><u><strong>moto z family</strong></u></a>.', 'icon': 'icon-092.svg'},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '48 MP rear camera sensor** with 4x greater light sensitivity', 'bgcolor': '#0d0f23', 'text': 'Our largest-ever sensor with Quad Pixel technology combines four pixels into one large 1.6µm pixel for vastly improved low light performance. In other words, you get incredibly sharp 12 MP photos—brighter shots with way less noise.', 'img': 'pic-1-0086.jpg'} },
        { 'info-right': {'title': 'capture the night', 'bgcolor': '#0d0f23', 'text': 'Switch to Night Vision mode in low light conditions to bring more details out of the dark. Capture bright, vivid photos with stunning clarity and more accurate colors.', 'img': 'pic-1-0087.jpg'} },
        { 'info-left': {'title': 'shoot photos like a pro with AI', 'bgcolor': '#0d0f23', 'text': 'With new features like Smart Composition, Auto Smile Capture, and AI Portrait Lighting, artificial intelligence (AI) inside the moto z4 looks for ways to give you the most professional-looking photos possible.', 'img': 'pic-1-0088.jpg'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}