// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#fff', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'so powerful', 
        'text': '<div style="padding-bottom: 10vh">GWith a huge 5000 mAh battery, you get up to 1.5 days of battery life.* And when it’s time to power up, don’t slow down. The <strong>turbopower™</strong> charger gives you up to 6 hours of battery life in just 15 minutes of charging.<sup>§</sup></div>', 
        'img': 'pic-1-0161.jpg',
        'paddingBox': '10vh 25px 0vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'impressive display', 
        'text': '<div style="padding-bottom: 10vh">Enjoy a 6" Max Vision display you can use comfortably with just one hand. Do less scrolling on websites. Get a crazy-wide landscape view of games. Even multitask effortlessly on a split screen – if you’re into that sort of thing.</div>', 
        'img': 'pic-1-0162.jpg',
        'paddingBox': '10vh 25px 10vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'sharp photos', 
        'text': '<div style="padding-bottom: 10vh">The 12 MP rear camera focuses in an instant with laser autofocus technology and lets in more light with large, 1.25 μm pixels. Turn it around and the 8 MP selfie camera snaps you and your friends in superb lighting, day or night, with the dedicated front flash.</div>', 
        'img': 'pic-1-0163.jpg',
        'paddingBox': '0vh 25px 10vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'stand out', 
        'text': '<div style="padding-bottom: 10vh">Designed with a reflective wave pattern and an arched back for a look as great as it feels. On the front, the rounded corner glass panel provides smooth edges, and in the back, the aerial is hidden away.</div>', 
        'img': 'pic-1-0164.jpg',
        'paddingBox': '0vh 25px 10vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'worry-free enjoyment', 
        'text': '<div style="padding-bottom: 10vh">A water-repellent design safeguards your phone inside and out. So, don’t worry about accidental spills and splashes.**</div>', 
        'img': 'pic-1-0165.jpg',
        'paddingBox': '0vh 25px 10vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'exclusive interactions', 
        'text': '<div style="padding-bottom: 10vh">Only <strong>motorola </strong>phones react to simple gestures to instantly reveal a function or communication you need at that moment. Customise which <strong>moto experiences</strong> you’d like to use by simply visiting the <strong>moto app</strong> on your new phone.</div>', 
        'img': 'pic-1-0166.jpg',
        'paddingBox': '0vh 25px 10vh',
    }},
        // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'fits into your life', 
        'text': 'Engineered to save you time without sacrificing security, to amp up your favourite music, and to grow when you need it, so there’s no limit to your entertainment.',
        'paddingBox': '5vh 25px 0px',
    }},
    // BLOCO DE CARDS
    { 'cards': {'title': '', 'paddingBox': '10vh 25px 0px', 'cards': [
        {'img': 'icon-068.jpg', 'title': 'Smart, secure fingerprint reader', 'text': 'Touch the fingerprint reader to wake up and unlock your phone instantly, and lock it again when you are done. It is discreetly located within the phone’s iconic logo, so you hardly know it’s there until you use it.'},
        {'img': 'icon-095.jpg', 'title': 'Crystal clear sound', 'text': 'With the front-ported speaker, hear conference calls and your favourite music and movies perfectly, even when the phone’s lying on its back.'},
        {'img': 'icon-096.jpg', 'title': 'You can have it all', 'text': 'Add up to 256 GB more photos, songs, and movies thanks to a dedicated microSD card slot. Now you don’t have to choose between adding a second SIM card for work or adding more storage.<sup>‡</sup>'},
    ]} },
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'paddingBox': '50px 25px 5vh', 'specifications': [
        {'title': '1.5-day battery', 'text': 'Go up to a full day-and-a-half on a single charge with a 5000 mAh battery.*', 'icon': 'icon-047.svg'},
        {'title': '6" edge-to-edge display', 'text': 'Get a big screen in a slim body. The 6" Max Vision display gives you a better view of websites, games, and more.', 'icon': 'icon-097.svg'},
        {'title': '12 MP camera with laser autofocus', 'text': 'Capture sharp photos even in low light conditions – and even when there’s no time to focus.', 'icon': 'icon-032.png'},
        {'title': 'Distinctive design', 'text': 'With a reflective wave pattern and an arched back, it looks as great as it feels.', 'icon': 'icon-098.svg'},
        {'title': 'Fingerprint reader', 'text': 'Never mind remembering a passcode. One touch wakes up your phone and unlocks it instantly.', 'icon': 'icon-057.svg'},
        {'title': 'Expandable storage', 'text': 'Make room for more photos, songs, and movies thanks to an extra microSD card slot.<sup>‡</sup>', 'icon': 'icon-073.svg'},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}