// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#fff', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [

    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'see it. capture it. interact with it.', 
        'text': 'Capture creative portraits and outdoor scenes with depth effects on a 12 MP dual camera system. Search what you see with Google Lens™. Plus, enjoy an ultrawide 6.2" Max Vision Full HD+ display.',
        'paddingBox': '',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': 'Watch the video', 
        'yt_id': '1aiMLNtIKM8', 
        'btn_icon': 'icon_circleplay_100@2x.png', 
        'bg': 'pic-1-0154.jpg', 
        'height': '430px',
        'bgcolor': '#fff',
        'titlecolor': '#fff',
        'paddingBox': '0vh 25px 0vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'striking portraits. stunning low-light shots.', 
        'text': '<div style="padding-bottom: 10vh">Capture artistic depth effects with a 12 MP dual camera system, big 1.25um pixels, a large f/1.8 aperture, and creative photo software. Plus, take better low-light selfies with an 8 MP front camera and a new screen flash.</div>', 
        'img': 'pic-1-0155.jpg',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'introducing high res zoom', 
        'text': '<div style="padding-bottom: 2vh">High res zoom automatically restores the details and image clarity that are typically lost with digital zooming.§§ So your photos look clear and detailed, even when subjects are far away.<br><strong>See the difference below: without (left) vs. with (right). </strong></div>', 
        'img': 'pic-1-0002.jpg',
        'paddingBox': '0vh 30px 0px',
    }},
    // BLOCO TEXTO NA ESQUERDA
    { 'info-left': {
        'title': '<div style="text-align: center">AI-powered camera software: your built-in photo assistant</device>', 
        'text': '<div style="text-align: center">Group photos are easier than ever with auto smile capture, an algorithm that triggers the shot when everyone in the frame is smiling.** Plus, with Google Lens™, it’s like having an encyclopedia in your viewfinder.</div>', 
        'img': 'pic-1-0003.jpg', 
        'paddingBox': '0vh 25px 0px',
    }},
     // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': '<img src="arquivos/icon-093.svg" alt="" class="d-block" style="margin: 15px auto;" width="100" height="100">the latest device to join the Google Fi family', 
        'text': '<div style="padding-bottom: 10vh">With the new <strong>moto g⁷</strong> on Google Fi, get all your calls, texts, and data on one simple—and smart—plan. With Google Fi, your <strong>moto g7</strong> will seamlessly switch between networks to put you on the fastest signal the best signal, and when you travel abroad, you’ll enjoy high-speed data at the same rate you pay at home.<br><br><br><a style="border: 2px solid #4cc2e2; background-color: rgba(0,0,0,0); border-radius: 25px; padding: 13px 25px; font-weight: 500 !important; font-size: 16px !important; color: #4cc2e2;" href="https://fi.google.com/about/">Learn more</a></div>',
        'img': 'pic-1-0156.jpg',
        'paddingBox': '10vh 25px 0vh',
    }},
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'paddingBox': '5vh 25px 5vh', 'specifications': [
        {'title': 'Ready for Google Fi', 'text': 'Only phones designed for Fi, like<strong> moto g<sup>7</sup></strong>, can intelligently shift among mobile networks and Wi-Fi to give you clear calls and fast data — at home and around the world.', 'icon': 'icon-004.png'},
        {'title': '12 MP + 5 MP dual camera system', 'text': 'Go beyond photos with a creative 12 MP + 5 MP dual camera system, creative photo software, and Google Lens™.', 'icon': 'icon-004.png'},
        {'title': '6.2" Full HD+ Max Vision display', 'text': 'Our largest <strong>moto g</strong> display ever has a 19:9 aspect ratio for a widescreen viewing experience with movies and games.', 'icon': 'icon-005.png'},
        {'title': '50% faster performance', 'text': 'An octa-core Qualcomm® Snapdragon™ processor gives a performance boost in everything you do.<sup>‡‡</sup>', 'icon': 'icon-006.png'},
        {'title': '15W turbopower™ charging', 'text': 'Get 9 hours of power in just 15 minutes, and go all day on a full charge.*', 'icon': 'icon-007.png'},
        {'title': 'Water-repellent, 3D glass design', 'text': 'Enjoy a comfortable grip with a scratch-resistant, contoured 3D Corning® Gorilla® Glass design.<sup>†</sup>', 'icon': 'icon-008.png'},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}