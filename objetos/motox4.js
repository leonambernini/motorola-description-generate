// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#fff', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#fff', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#0d0f23', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // CARD 3
    { 'cards3': {
        'title': '', 
        'bgcolor': '#eff2f4',
        'titlecolor': '#000',
        'cards': [
            {'img': 'pic-1-0138.jpg', 'title': 'a best-in-class Google experience', 'text': 'Whether it is the fully optimized Google Assistant, free unlimited high quality photo storage or video calling on Google Duo, the <strong>moto x<sup>4</sup> Android One</strong> includes the best-in-class experiences from Google right of of the box.', 'button': '', 'bgcolor': '#3e8edd', 'titlecolor': '#fff'},
            {'img': 'pic-1-0139.jpg', 'title': 'a refreshingly simple user interface', 'text': 'A streamlined, thoughtfully designed user interface from Google that comes with just what you need and nothing you don’t. With a small set of pre-installed apps and free unlimited storage from Google Photos, <strong>moto x<sup>4</sup> Android One</strong> users can use their storage the way they want and make space when needed.', 'button': '', 'bgcolor': '#3e8edd', 'titlecolor': '#fff'},
            {'img': 'pic-1-0140.jpg', 'title': 'built-in security', 'text': 'Android One phones are among the most secure out there. The <strong>moto x<sup>4</sup> Android One</strong> gets monthly security updates and the added security protections of Google Play Protect: built-in malware protection that keeps your phone clean, fast, and high-performing.', 'button': '', 'bgcolor': '#3e8edd', 'titlecolor': '#fff'},
        ]
    }},
    // BLOCO TEXTO NA ESQUERDA
    { 'info-left': {
        'title': 'always fresh with early access to software upgrades', 
        'text': 'Android One devices are among the first to access the latest Android OS updates and platform innovations. With <strong>moto x<sup>4</sup> Android One</strong>, you’ll receive at least two years of OS upgrades, including an update to Android™ 9.0 Pie™, available now.<br><br><br><a class="cta-library-wrap Custom-rounded-17448-14651" href="https://www.motorola.com/us/software-and-apps/android " target="_self"><span style="border: 1px solid #ffffff;background-color: rgba(255,255,255,1.0);border-radius: 25px;padding: 13px 25px;font-weight: 500 !important;color: #79c257;">Learn more</span></a>', 
        'img': 'pic-1-0141.jpg',
        'bgcolor': '#79c257', 
    }},
    // SPECIFICATION MORE
    { 'specification-btn-more': {
        'title': 'specifications', 
        'img': 'pic-1-0142.jpg', 
        'bgcolor': '#fff',
        'titlecolor': '#000',
        'col1': '<h5>The best from Google</h5><p class="p1">Enjoy the latest Google software and security, a refreshingly simple user interface, and the smart Google Fi network.</p><h5>IP68 water resistant</h5><p class="p1">Keep your phone protected from accidental spills, splashes and even puddles.*</p><h5>Dual rear cameras</h5><p class="p1">Take photos worth bragging about with an advanced camera system.</p>',
        'col2': '<h5>Ultra-low light selfie cam</h5><p class="p1">Always look your best thanks to a 16 MP front camera and a selfie flash.</p><h5>Precision-crafted glass and metal</h5><p class="p1">Easy to hold and easy on the eyes, featuring a beautiful, contoured design.</p><h5>Blazing-fast TurboPower™ charging</h5><p class="p1">Get up to 6 hours of power in just 15 minutes.<sup>†</sup></p><h5>Qualcomm<sup>®</sup> Snapdragon™ processor</h5><p class="p1">Get octa-core performance without sacrificing style.</p>',
        'more': {
            'img': 'pic-1-0127.png', 
            'bgcolor': '#fff',
            'titlecolor': '#000',
            'col1': '<h5>Carrier compatibility</h5><p>Available unlocked for major U.S. networks like Verizon, AT&amp;T, T-Mobile, Sprint<sup>†††</sup>, and Google Fi.</p><p class="p1"><a href="https://www.motorola.com/us/carrier-compatibility">View carrier compatibility chart</a></p><h5>Operating system</h5><p class="p1">Android™ 7.1, Nougat upgradable to Android™ 9.0, Pie. <a href="https://support.motorola.com/us/en/softwareupgrade">Check for upgrades</a></p><h5>System architecture/Processor</h5><p class="p1">Qualcomm<sup>®</sup> Snapdragon™ 630 processor with 2.2 GHz Octa-core CPU and 650 MHz Adreno 508 GPU</p><h5>Memory (RAM)</h5><p class="p1">3 GB</p><h5>Storage (ROM)</h5><p class="p1">32 GB&nbsp;with microSD Card support (up to 2 TB)<sup>‡</sup></p><h5>Dimensions</h5><p class="p1">148.35 x 73.4 x 7.99 mm (9.45 mm at camera bump)</p><h5>Weight</h5><p class="p1">163 g</p><h5>Display</h5><p class="p1">5.2” FHD (1080×1920)<br>424 PPI<br>LTPS IPS<br>Corning<sup>®</sup> Gorilla<sup>®</sup> Glass</p><h5>Battery</h5><p class="p1">All-day battery<sup>†</sup><br>3000mAh, Non-removable</p><h5>Charging</h5><p>15W TurboPower for 6 hours of power in 15 minutes<sup>†</sup></p><h5>Water protection</h5><p class="p1">IP68 Water Resistant<sup>*</sup></p><h5>Network bands (by model)</h5><p class="p1">4G LTE (Cat11 DL, Cat5 UL)<br>CDMA / EVDO Rev A<br>UMTS / HSPA+<br>GSM / EDGE<br>2G: GSM band 2/3/5/8 CDMA BC 0/1/10<br>3G: WCDMA band 1/2/4/5/8<br>4G: FDD LTE band 1/2/3/4/5/7/8/12/13/17/20/25/26/28/66<br>TDD LTE band 38/41</p><h5>SIM card</h5><p class="p1">Single Nano SIM/Dual Nano SIM</p><h5>Sensors</h5><p class="p1">Fingerprint Reader<br>Gravity<br>Proximity<br>Accelerometer<br>Ambient Light<br>Magnetometer<br>Gyroscope<br>Sensor Hub</p><h5>Wi-Fi</h5><p class="p1">Wi-Fi 802.11a/b/g/n/ac, 2.4GHz + 5GHz, Wi-Fi hotspot</p>',
            'col2': '<h5>Rear camera</h5><p class="p1">12MP Dual Autofocus Pixel sensor (f2.0, 1.4um)<br>8MP ultra-wide angle with 120° field of view sensor (f2.2, 1.12um)<br>Color Correlated Temperature (CCT) dual LED flash<br>Phase Detection AutoFocus (PDAF)<br>Ultra-wide angle shot<br>Professional mode<br>Depth detection and depth effects<br>Selective Focus<br>Selective Black &amp; White (beta)<br>Background Replacement (beta)<br>Spot Color<br>Landmark/Object Recognition<br>Scan barcodes/QR codes/Business cards<br>Panorama<br>Slo-motion video<br>Best shot</p><h5>Front camera</h5><p class="p1">16MP (f2.0, 1um)<br>Selfie flash / light<br>4 MP adaptive low light mode<sup>§</sup><br>Selfie panorama<br>Face filters<br>Beautification mode<br>Professional mode</p><h5>Video capture</h5><p class="p1">2160P/4K (30fps), 1080P (60fps), 720P (30fps), 480P (30fps)</p><h5>Connectivity</h5><p class="p1">USB Type-CTM Port<br>3.5 mm headphone jack</p><h5>FM radio</h5><p>Yes</p><h5>Bluetooth<sup>®</sup> technology</h5><p class="p1">Bluetooth<sup>®</sup> 5.0 BR/EDR + BLE</p><h5>Speakers/microphones</h5><p class="p1">Front-ported speaker<br>3-Mics</p><h5>Video playback</h5><p>720p (120fps), 1080p (60fps), 4K (30fps)</p><h5>Location services</h5><p class="p1">GPS, GLONASS</p><h5>NFC</h5><p>Yes</p><h5>Colors</h5><p class="p1">Super Black, Sterling Blue</p><h5>moto mods</h5><p>No</p><h5>moto experiences</h5><p>moto actions, moto display</p>',
        }
    }},
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '48 MP rear camera sensor** with 4x greater light sensitivity', 'bgcolor': '#0d0f23', 'text': 'Our largest-ever sensor with Quad Pixel technology combines four pixels into one large 1.6µm pixel for vastly improved low light performance. In other words, you get incredibly sharp 12 MP photos—brighter shots with way less noise.', 'img': 'pic-1-0086.jpg'} },
        { 'info-right': {'title': 'capture the night', 'bgcolor': '#0d0f23', 'text': 'Switch to Night Vision mode in low light conditions to bring more details out of the dark. Capture bright, vivid photos with stunning clarity and more accurate colors.', 'img': 'pic-1-0087.jpg'} },
        { 'info-left': {'title': 'shoot photos like a pro with AI', 'bgcolor': '#0d0f23', 'text': 'With new features like Smart Composition, Auto Smile Capture, and AI Portrait Lighting, artificial intelligence (AI) inside the moto z4 looks for ways to give you the most professional-looking photos possible.', 'img': 'pic-1-0088.jpg'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}