// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#fff', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [

    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'unshakable image quality', 
        'text': '<div style="padding-bottom: 7vh">Take your photos and videos to the next level with optical image stabilisation (OIS) and AI-powered camera software. Plus, get the fastest <strong>turbopower™</strong> charging for <strong>moto g</strong>,* an ultrawide 6.2" Max Vision Full HD+ display, and stereo speakers tuned by Dolby Audio™.</div>',
        'paddingBox': '7vh 25px 0px',
        'movie': {
            'yt_id': '8kjf6ExuEso', 
            'height': '430px',
        }
    }},
    { 'info-2-text': {'title': 'A', 'text': 'aaaaa', 'paddingBox1': '30px 60px', 'title2': 'B', 'text2': 'bbbbb', 'paddingBox2': '30px 50px'} },
    // BLOCO VIDEO
    { 'movie': {
        'title': 'Watch the video', 
        'yt_id': '8kjf6ExuEso', 
        'btn_icon': 'icon_circleplay_100@2x.png', 
        'bg': 'pic-2-0005.jpg', 
        'height': '430px',
        'bgcolor': '#fff',
        'titlecolor': '#fff'
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'sharp, detailed low-light photography', 
        'text': '<div style="padding-bottom: 7vh">The 16 MP + 5 MP dual camera system uses big pixels and a large f/1.7 aperture to capture artistic depth effects—even outdoors and in low light. Plus, take better self-portraits on the 12 MP front camera with built-in screen flash.</div>', 
        'img': 'pic-2-0006.jpg',
        'bgcolor': '#000',
        'titlecolor': '#fff',
        'paddingBox': '7vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'introducing optical image stabilisation', 
        'text': '<div style="padding-bottom: 7vh">New for <strong>moto g</strong>, OIS improves your night shots by reducing the motion blur from shaky hands or accidental movement. <strong>See the difference below: without (left) vs. with (right)</strong>.</div>', 
        'img': 'pic-2-0007.jpg',
        'paddingBox': '7vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'AI-powered camera software: your built-in photo assistant', 
        'text': '<div style="padding-bottom: 7vh">When capturing people, smart composition recongnises potential improvements in the frame and automatically generates an optimised second image. Auto smile capture triggers the shot when everyone’s smiling.<sup>‡‡</sup> Plus, search what you see with Google Lens<sup>™</sup>. <strong>See the smart composition difference below: without (left) vs. with (right).</strong></div>', 
        'img': 'pic-2-0008.jpg',
        'paddingBox': '7vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'camera feature: high res zoom', 
        'text': '<div style="padding-bottom: 7vh">High res zoom automatically restores the details and clarity typically lost with digital zooming.§§</div>', 
        'img': 'pic-2-0009.jpg',
        'paddingBox': '7vh 25px 0px',
    }},
    // BLOCO DE SLIDER COM VÍDEO
    { 'slider': {'title': 'explore even more <span style="color:#e76a26">moto g⁷ plus</span> camera features', 'paddingBox': '5vh 25px 5vh', 'titlecolor': '#000', 'sliders': [
            {'img': 'pic-2-0010.jpg', 'text': '<p><strong>auto smile capture</strong><br><br>Auto smile capture uses an algorithm to capture group shots when everyone’s smiling.<sup>‡‡</sup></p>'},
            {'img': 'pic-2-0011.jpg', 'yt_id': '3OqQa1leYI4', 'btn_icon': 'icon_circleplay_100@2xbranco.png', 'text': '<p><strong>cinemagraph</strong><br><br>Make cinemagraphs, keeping a portion of your shot in motion while freezing everything else.</p>'},
            {'img': 'pic-2-0012.jpg', 'text': '<p><strong>spot colour</strong><br><br>Add a pop art effect by selecting a single colour and making the rest of the photo black &amp; white.</p>'},
            {'img': 'pic-2-0013.jpg', 'text': '<p><strong>portrait mode</strong><br><br>Photo by: Daniel Fendek, Senior Business Developer, Bratislava, Slovakia</p>'},
            {'img': 'pic-2-0014.jpg', 'text': '<p><strong>Google Lens</strong><br><br>Recognise landmarks, objects, foreign languages, and more with Google Lens.</p>'},
            {'img': 'pic-2-0015.jpg', 'text': '<p><strong>Google Photos</strong><br><br>Free, unlimited, high-quality storage with Google Photos.'},
    ]} },
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'the quickest-charging <span style="color:#e76a26">moto g</span> ever', 
        'text': 'Get 12 hours of power in just 15 minutes of charging with 27W <strong>turbopower™</strong> charging. After a full charge, the 3000 mAh battery lasts all day.<sup>*</sup>', 
        'img': 'pic-2-0016.jpg',
        'bgcolor': '#000',
        'titlecolor': '#fff',
        'paddingBox': '10vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'stunning re-engineered Max Vision display', 
        'text': '<div style="padding-bottom: 7vh">Enjoy maximum big screen entertainment on a 6.2" Full HD+ display—our largest ever on a moto g. The 19:9 aspect ratio and new minimalist U design create an ultrawide, immersive viewing experience for movies, games, photos, and more.</div>', 
        'img': 'pic-2-0017.jpg',
        'bgcolor': '#000',
        'titlecolor': '#fff',
        'paddingBox': '7vh 25px 0vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'stereo speakers tuned by Dolby™', 
        'text': 'Dolby Audio™ gives you unparalleled control over your entertainment. Enjoy clear dialogue and realistic surround sound on powerful stereo speakers.', 
        'img': 'pic-2-0018.jpg',
        'bgcolor': '#000',
        'titlecolor': '#fff',
        'paddingBox': '5vh 25px 0px',
    }},
    // BLOCO TEXTO NA ESQUERDA
    { 'info-left': {
        'title': '<div style="text-align: center">the fastest <span style="color:#e76a26">moto g</span> ever</div>', 
        'text': '<div style="text-align: center">An ultra-responsive Qualcomm® Snapdragon™ 636 octa-core processor boosts all-around performance, from 3D games, to video editing.</div>', 
        // 'img': 'pic-2-0019.jpg', 
        'paddingBox': '0vh 0px 0vh 25px',
        'movie': {
            'yt_id': '8kjf6ExuEso', 
            'height': '430px',
        }
        
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'scratch-resistant Corning® Gorilla® Glass', 
        'text': '<div style="padding-bottom: 7vh">Enjoy a comfortable grip with 3D contoured Corning® Gorilla® Glass that protects both sides of the phone against nicks and scratches.</div>', 
        'img': 'pic-2-0020.jpg',
        'bgcolor': '#000',
        'titlecolor': '#fff',
        'paddingBox': '7vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'water-repellent, inside and out', 
        'text': '<div style="padding-bottom: 7vh">A water-repellent design protects against accidental spills, sweat, and light rain.<sup>†</sup></div>', 
        'img': 'pic-2-0021.jpg',
        'paddingBox': '7vh 25px 0px',
    }},
    // BLOCO DE CARDS
    { 'cards': {'title': '<div style="padding-bottom: 7vh">more of what you love</div>', 'paddingBox': '10vh 25px 0px', 'cards': [
        {'img': 'icon-099.jpg', 'title': 'motorola-exclusive shortcuts', 'text': 'Use natural gestures to access your favorite features and apps quickly and intuitively with <strong>moto experiences</strong>.'},
        {'img': 'icon-100.jpg', 'title': 'Mobile payments', 'text': 'Making secure purchases is fast and easy. Just place your phone near an NFC terminal and use your fingerprint.**'},
        {'img': 'icon-101.jpg', 'title': 'Expanded storage', 'text': 'Store more photos, songs, and movies with a dedicated microSD card slot.<sup>††</sup>'},
        {'img': 'icon-102.jpg', 'title': 'Bluetooth 5.0', 'text': 'Enjoy faster data speeds, longer wireless range, and improved power efficiency with Bluetooth® 5.0.'},
    ]} },
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'your best shot', 
        'text': 'Here are some of our favorite clicks from everyday <strong> moto g⁷ plus</strong> photographers just like you.',
        'paddingBox': '10vh 25px 0vh',
    }},
    // BLOCO DE SLIDER
    { 'slider': {'title': '', 'titlecolor': '#000', 'paddingBox': '0vh 25px 0vh', 'sliders': [
            {'img': 'pic-2-0022.jpg', 'text': '<p>Photo by: Jose Correa, Hardware Engineer, São Paulo, Brazil</p>'},
            {'img': 'pic-2-0023.jpg', 'text': '<p>Photo by: Adilson Costa, Reliability Manager, São Paulo, Brazil</p>'},
            {'img': 'pic-2-0024.jpg', 'text': '<p>Photo by: Arne Gnoerich, Channel Sales Account Executive, Rome, Italy</p>'},
            {'img': 'pic-2-0025.jpg', 'text': '<p>Photo by: Edilson Silva, Software Development Manager, São Paulo, Brazild</p>'},
            {'img': 'pic-2-0026.jpg', 'text': '<p>Photo by: Fabiano Matassa, Engineer, Hampshire, UK</p>'},
            {'img': 'pic-2-0027.jpg', 'text': '<p>Photo by: Fabio Felipe Mira Machuca, Software Engineer, Campinas, Brazil</p>'},
    ]} },
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'specifications': [
        {'title': 'Dual camera system with OIS', 'text': 'Capture clear, detailed images in any lighting conditions with a 16 MP + 5 MP dual camera system featuring optical image stabilisation.', 'icon': 'icon-031.png'},
        {'title': '12 MP front camera', 'text': 'Take better self-portraits in any light on the 12 MP front camera with built-in screen flash.', 'icon': 'icon-004.png'},
        {'title': '27W turbopower™ charging', 'text': 'Get 12 hours of power in just 15 minutes, and go all day on a full charge.*', 'icon': 'icon-047.svg'},
        {'title': '6.2" Full HD+ Max Vision display', 'text': 'Our largest <strong>moto g </strong>display ever has a 19:9 aspect ratio for a widescreen viewing experience with movies and games.', 'icon': 'icon-005.png'},
        {'title': 'Dolby Audio™ tuned stereo speakers', 'text': 'Enjoy loud volume, crystal clear dialogue, and realistic surround sound.', 'icon': 'icon-103.svg'},
        {'title': 'The fastest moto g ever', 'text': 'An octa-core Qualcomm® Snapdragon™ processor gives a performance boost to everything you do.', 'icon': 'icon-006.png'},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-2-text': {'title': 'A', 'text': 'aaaaa', 'title2': 'B', 'text2': 'bbbbb'} },
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}