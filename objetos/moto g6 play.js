// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#fff', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [

    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'big battery. total freedom.', 
        'text': 'Do more of what you love with up to 36 hours of battery life.* Watch videos on an edge-to-edge 5.7" HD+ Max Vision display. Capture great photos with a 13 MP rapid-focus camera. Unlock your phone with your fingerprint. And do it all fast on a 1.4 GHz quad-core processor.<sup>††</sup>',
        'paddingBox': '10vh 25px 10vh',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': '<div style="color: #fee500"><strong>Watch the product video</strong></div>', 
        'yt_id': 'BNIosf0wZ-k', 
        'btn_icon': 'icon_circleplay_100@2x.png', 
        'bg': 'pic-1-0093.jpg', 
        'height': '430px',
        'bgcolor': '#fff',
        'titlecolor': '#fee500'
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'stay unplugged longer. refuel faster.', 
        'text': '<div style="padding-bottom: 12vh">Go up to 36 hours on a single charge thanks to a 4000 mAh battery. When it’s time to power up, don’t slow down. The included 10W rapid charger gives you hours of power in just minutes. Or, charge even faster with a 15W <strong>turbopower™</strong> charger, sold separately.*</div>', 
        'img': 'pic-1-0094.jpg',
        'paddingBox': '12vh 25px 10px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'big screen, small body', 
        'text': '<div style="padding-bottom: 12vh">Our new 5.7" Max Vision display spans from edge to edge and still fits comfortably in your hand. The 18:9 aspect ratio gives you a much wider viewing area than traditional displays, so entertainment never looked better.</div>', 
        'img': 'pic-1-0095.jpg',
        'paddingBox': '10vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'just point and shoot', 
        'text': '<div style="padding-bottom: 14vh">The 13 MP rear camera uses phase detection autofocus to focus fast, so you never miss a moment. Plus, you always get the best pictures – <strong>moto g⁶ play</strong> automatically takes multiple shots and recommends the best one to keep.*</div>', 
        'img': 'pic-1-0096.jpg',
        'paddingBox': '14vh 25px 0px',
    }},
    // BLOCO TEXTO NA ESQUERDA
    { 'info-left': {
        'title': 'simply stunning selfies', 
        'text': '<div style="padding-bottom: 12vh">The 5 MP front camera with LED flash includes beautification mode, which smoothes skin and reduces blemishes and wrinkles.</div>', 
        'img': 'pic-1-0097.jpg',
        'paddingBox': '10vh 25px 0px', 
    }},
    // BLOCO TEXTO NA DIREITA
    { 'info-right': {
        'title': 'unlimited high quality storage', 
        'text': 'Google Photos is the default gallery for all your photos and videos. Your memories are automatically backed up, searchable and can easily be accessed, shared and edited from any device.†††', 
        'img': 'pic-1-0098.jpg', 
        'paddingBox': '5vh 25px 0px',
    }},
    // BLOCO TEXTO NA ESQUERDA
    { 'info-left': {
        'title': 'fingerprint reader', 
        'text': 'Use one long press on the fingerprint reader to lock and unlock your phone.', 
        'img': 'pic-1-0099.jpg', 
        'bgcolor': '#69c347',
        'paddingBox': '5vh 25px 0px',
        'titlecolor': '#fff',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'why wait for the good stuff?', 
        'text': '<div style="padding-bottom: 14vh"><strong>moto g⁶ play</strong> is backed by a speedy Qualcomm® Snapdragon™ 1.4 GHz quad-core processor<sup>†† </sup>and powerful graphics capabilities. Enjoy your favorite apps, games, and videos without annoying lag or stuttering, and browse the web or stream music at fast 4G speed.</div>', 
        'img': 'pic-1-0100.jpg',
        'paddingBox': '12vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'dance in the rain', 
        'text': '<div style="padding-bottom: 14vh"><strong>moto g⁶ play</strong> sports a 3D glass back packed with light-reflecting particles, so it looks great from every angle. Best of all, a water-repellent coating helps protect the phone from accidental splashes or light rain.<sup>‡</sup></div>', 
        'img': 'pic-1-0101.jpg',
        'paddingBox': '12vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'engineered for your life', 
        'text': '<strong>moto g⁶ play</strong> is built to help you access features and content quickly,<br>easily, and securely, so you can have more peace of mind.',
        'paddingBox': '12vh 25px 0px',
    }},
    // BLOCO DE CARDS
    { 'cards': {'title': '', 'paddingBox': '10vh 25px 0px', 'cards': [
        {'img': 'icon-019.jpg', 'title': 'Updates at a glance', 'text': '<strong>moto display</strong> lets you preview notifications quickly and easily, without unlocking your phone. Just give it a nudge to see what’s happening.'},
        {'img': 'icon-053.jpg', 'title': 'Exclusive moto experiences', 'text': 'Need some space? Flip your phone face down to activate do not disturb mode. Need a light? Chop down twice to turn on the torch. <strong>moto actions</strong> make life easy.<sup>‡‡</sup>'},
        {'img': 'icon-054.jpg', 'title': 'You can have it all', 'text': 'Add up to 128 GB more photos, songs, and movies thanks to a dedicated microSD card slot. Now you don’t have to choose between adding a second SIM card for work or adding more storage.<sup>§</sup>'},
    ]} },
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'paddingBox': '10vh 25px 5vh', 'specifications': [
        {'title': '4000 mAh battery and fast charging', 'text': 'Go up to 36 hours on a single charge, then get hours of power in just minutes with the included 10W rapid charger, or with a 15W <strong>turbopower</strong> charger, sold separately.*', 'icon': 'icon-047.svg'},
        {'title': 'Edge-to-edge display', 'text': 'Enjoy your favourite entertainment on a 5.7" HD+ Max Vision display.', 'icon': 'icon-055.svg'},
        {'title': '13 MP rapid-focus camera', 'text': 'Never miss a great shot with a camera that focuses as fast as you do.', 'icon': 'icon-056.svg'},
        {'title': 'Fast performance', 'text': 'Get all the speed you need with a 1.4 GHz quad-core processor†† and plenty of RAM.', 'icon': 'icon-023.png'},
        {'title': 'Secure fingerprint reader', 'text': 'Unlock your phone with a touch.', 'icon': 'icon-057.svg'},
        {'title': 'Exclusive moto experiences', 'text': 'Get shortcuts to the features you use most, like turning on the camera with a twist of your wrist.', 'icon': 'icon-058.svg'},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}