// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#0d0f23', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // MOSAIC
    { 'mosaic': {
        'title': '<div style="padding-bottom: 2vh; padding-top: 2vh">your phone transforms into a projector, in a snap</div>',
        'items': [
            {'width': '50%', 'height': '700px', 'bg': 'pic-2-0081.jpg', 'title': 'project + share', 'text': 'Life-size entertainment in places you never imagined', 'btn': 'Learn more'},
            {'width': '50%', 'height': '350px', 'bg': 'pic-2-0082.jpg', 'title': 'features', 'text': 'Packed with built-ins to make sharing awesome and simple', 'btn': 'Learn more'},
            {'width': '50%', 'height': '350px', 'bg': 'pic-2-0083.jpg', 'title': 'moto mods™', 'text': 'Your phone becomes a movie projector, a boombox, a battery powerhouse, and more. In a snap.', 'btn': 'Watch video'},
        ],
        'modalbg': '#ff6901',
        'modalcolor': '#fff',
        'modalBtnClose': 'icon_xCLose_50.svg',
        'modalItems': [
            {'model': 'text-image', 'title': 'take the theater with you', 'text': 'The Moto Insta-Share Projector snaps easily onto your Moto Z phone so you can immediately start sharing vacation photos or that video everyone’s talking about. It projects up to 70” on any flat surface so the big screen’s always with you.', 'img': 'pic-2-0084.jpg', 'colText': '5', 'bg': '#2d2a26', 'colImage': '7'},
            {'model': 'text-image', 'title': 'smart, convenient design', 'text': 'The ultra-thin Moto Insta-Share Projector comes with an integrated kickstand so you can easily project at any angle. Plus, it includes up to one hour of battery life so you can project longer without using your phone`s battery.†', 'img': 'pic-2-0085.jpg', 'colText': '5', 'bg': '#2d2a26', 'colImage': '7'},
            {'model': 'text-movie', 'title': 'transformation’s never been easier', 'text': 'With Moto Mods™ like Moto Insta-Share Projector, do things you never thought were possible with a phone. The interchangeable backs snap onto your phone with powerful integrated magnets, so you can make your imagination reality. Whatever your passion, there’s a Moto Mod™ for you.', 'img': '', 'bg': '#2d2a26', 'colText': '5', 'colImage': '7', 'movie': {
                'title': '', 
                'yt_id': 'OBVCZ-P1GCk', 
                'btn_icon': 'icon_circleplay_100@2x.png', 
                'bg': 'pic-2-0078.jpg', 
                'height': '100%',
                'bgcolor': '#2d2a26',
                'titlecolor': '#fff',
            }},
        ],
    }},

    // SPECIFICATION MORE
    { 'specification-btn-more': {
        'title': 'specifications', 
        'img': 'pic-2-0086.jpg', 
        'bgcolor': '#fff',
        'titlecolor': '#000',
        'col1': '<h5><h5>works with any moto z</h5><p class="p1">Compatible with the <a href="http://www.motorola.com/us/products/moto-z-family" style="color: #0090a6; text-decoration: underline;">Moto Z Family</a> of phones.</p><h5>project up to 70"</h5><p class="p1">Instantly turn any flat surface into a 70" big screen on the fly.</p><h5>up to one extra hour of screen time</h5><p class="p1" style="margin-bottom: 10px;">Watch more before using your phone’s battery.<sup>†</sup></p>',
        'col2': '<h5>display at any angle, anytime</h5><p class="p1" style="margin-bottom: 10px;">Take the ultra-thin projector anywhere and share onto just about any flat surface. Use the integrated kickstand to project at any angle.</p><h5>share videos, photos, and more</h5><p class="p1">Gather friends around to share your vacation photos, watch your favorite shows, or tune into the big game.</p>',
        'more': {
            'img': 'pic-2-0087.jpg', 
            'bgcolor': '#fff',
            'titlecolor': '#000',
            'col1': '<h5>compatible phones</h5><p>The <a href="http://www.motorola.com/us/products/moto-z-family" style="color: #0090a6; text-decoration: underline;">Moto Z Family</a> of phones</p><h5>dimensions</h5><p>153 x 74 x 11mm</p><h5>weight</h5><p>125g</p><h5>resolution</h5><p>854x480 WVGA (480p)</p><h5>projector technology</h5><p>DLP</p><h5>brightness</h5><p>50 lumens nominal</p><h5>contrast ratio</h5><p>400:1</p><h5>throw ratio</h5><p>1.2</p><h5>image size</h5><p>Up to 70" diagonal</p><h5>aspect ratio</h5><p>16:9</p><h5>wireless</h5><p>Moto Z phone cellular and wifi data</p>',
            'col2': '<h5>lamp life rating</h5><p>10,000 hours</p><h5>keystone correction</h5><p>-40º to 40º</p><h5>physical button control</h5><p>Power/settings, Manual focus</p><h5>settings options</h5><p>Image tilt, Brightness</p><h5>battery size</h5><p>1100 mAh</p><h5>battery life</h5><p>Adds up to 60 minutes of projection time²</p><h5>external charging</h5><p>USB Type C</p><h5>audio output</h5><p>Speaker, Bluetooth or USB-C from Moto Z phone</p><h5>display input</h5><p>Moto Z phone</p><h5>carrying pouch</h5><p>Included</p>',
        }
    }},
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '48 MP rear camera sensor** with 4x greater light sensitivity', 'bgcolor': '#0d0f23', 'text': 'Our largest-ever sensor with Quad Pixel technology combines four pixels into one large 1.6µm pixel for vastly improved low light performance. In other words, you get incredibly sharp 12 MP photos—brighter shots with way less noise.', 'img': 'pic-1-0086.jpg'} },
        { 'info-right': {'title': 'capture the night', 'bgcolor': '#0d0f23', 'text': 'Switch to Night Vision mode in low light conditions to bring more details out of the dark. Capture bright, vivid photos with stunning clarity and more accurate colors.', 'img': 'pic-1-0087.jpg'} },
        { 'info-left': {'title': 'shoot photos like a pro with AI', 'bgcolor': '#0d0f23', 'text': 'With new features like Smart Composition, Auto Smile Capture, and AI Portrait Lighting, artificial intelligence (AI) inside the moto z4 looks for ways to give you the most professional-looking photos possible.', 'img': 'pic-1-0088.jpg'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}