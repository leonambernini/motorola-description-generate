// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#fff', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [

    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'a CES 2019 Innovation Awards honoree', 
        'text': 'The CES Innovation Awards recognizes the new <strong>moto z³</strong> in the category of Wireless Devices, Accessories and Services.<br><br><br><a style="border: 1px solid #E1140A; background-color: rgba(225,20,10,1.0); border-radius: 25px; padding: 13px 25px; font-weight: 500 !important; font-size: 16px !important; color: #FFFFFF;" href=" https://www.ces.tech/Events-Programs/Innovation-Awards/Honorees.aspx" target="_self">Learn more</a>',
        'paddingBox': '5vh 25px 10vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'race past expectations', 
        'text': '<div style="padding-bottom: 10vh">With the new <strong>moto z<sup>3</sup></strong>, be the first to upgrade to Verizon’s 5G network just by snapping on a <strong>moto mod<sup>™</sup></strong>.<sup>*</sup> Plus, get an all-day battery,<sup>†</sup> dual depth-sensing smart cameras, and more.</div>',
        'paddingBox': '10vh 25px 0px',
        'bgcolor': '#e8e8e8',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': 'Watch the video', 
        'yt_id': 'tWaGF1pz1uo', 
        'btn_icon': 'icon_circleplay_100@2xvermelho.png', 
        'bg': 'pic-1-0049.jpg', 
        'height': '1199px',
        'bgcolor': '#e8e8e8',
        'titlecolor': '#fff',
        'paddingBox': '11vh 25px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': '<img src="/arquivos/icon-039.svg" alt="" class="d-block" style="margin: 15px auto;" width="100" height="100">accelerate your mobile experience', 
        'text': '<div style="padding-bottom: 10vh">Introducing the first ever use of millimeter wave technology for a smartphone, only on Verizon with the <strong>5G moto mod™</strong>,<sup>*</sup> sold separately. With projected speeds 10 times faster than today’s wireless technology, say goodbye to buffering and hello to the fastest network ever.<sup>****</sup><br><br><br><a style="border: 1px solid #E1140A; background-color: rgba(225,20,10,1.0); border-radius: 25px; padding: 13px 25px; font-weight: 500 !important; font-size: 16px !important; color: #FFFFFF;" href="https://www.motorola.com/us/products/moto-mods/moto-5g" target="_self">Learn more</a></div>',
        'paddingBox': '',
        'img': 'pic-1-0050.jpg',
        'paddingBox': '5vh 25px 0vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': '<img src="/arquivos/icon-040.png" alt="" class="d-block" style="margin: 15px auto;" width="100" height="100">go all day and recharge faster', 
        'text': '<div style="padding-bottom: 10vh">Stay unplugged longer with up to 24 hours of built-in battery life. When you’re running low, get half a day’s power in half an hour of charging with <strong>turbopower<sup>™</sup></strong>.<sup>†</sup></div>',
        'img': 'pic-1-0051.jpg',
        'paddingBox': '5vh 25px 5vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': '<img src="/arquivos/icon-039.svg" alt="" class="d-block" style="margin: 15px auto;" width="100" height="100"><strong>customize your<br><span style="color:#E1140A">moto z<sup>3</sup></span></strong>', 
        'text': '<div style="padding-bottom: 10vh">Print real Polaroid photos right from your phone, turn any room into a DJ booth with immersive stereo sound, and more. Whatever you’re into, there’s a <strong>moto mod<sup>™</sup></strong> for you.<br><br><strong><a href="https://www.motorola.com/us/moto-mods" style="color:#000000;" target="_blank">Learn more</a></strong>&nbsp;&nbsp;&nbsp;<strong><a href="https://www.motorola.com/us/products/moto-mods" style="color:#000000;" target="_blank">See them all</a></strong></div>',
        'img': 'pic-1-0052.jpg',
        'paddingBox': '5vh 25px 10vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': '<img src="/arquivos/icon-041.svg" alt="" class="d-block" style="margin: 15px auto;" width="100" height="100">dual depth-sensing smart cameras', 
        'text': 'Snap stunning images under any conditions with dual 12 MP low light sensors, including one monochrome sensor. Use advanced imaging software to adjust layers and colors, and even animate moments with cinemagraphs. Plus, discover your world with integrated Google Lens tools, like text translation and object recognition.<sup>††</sup><br><br><strong><a href="https://www.motorola.com/us/products/moto-z-gen-3/camera" style="color:#000000;" target="_blank">Explore all camera features</a></strong>',
        'paddingBox': '5vh 25px 0px',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': '', 
        'yt_id': 'Ye-nH0Y-JKs', 
        'btn_icon': 'icon_circleplay_100@2xvermelho.png', 
        'bg': 'pic-1-0053.jpg', 
        'height': '720px',
        'bgcolor': '#fff',
        'titlecolor': '#fff',
        'paddingBox': '5vh 25px 15vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': '<img src="/arquivos/icon-042.svg" alt="" class="d-block" style="margin: 15px auto;" width="100" height="100">a near bezel-less display', 
        'text': '<div style="padding-bottom: 10vh">Binge-watch a show or scroll through your newsfeed on an edge-to-edge 6” Full HD OLED display. Text and images pop with deep blacks and vivid colors. And with a Max Vision 18:9 aspect ratio, you see more and scroll less.</div>',
        'img': 'pic-1-0054.jpg',
        'paddingBox': '5vh 25px 10px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': '<img src="/arquivos/icon-042.svg" alt="" class="d-block" style="margin: 15px auto;" width="100" height="100">super-slim design', 
        'text': '<div style="padding-bottom: 10vh">At just 6.75mm, <strong>moto z<sup>3</sup></strong> is one of the thinnest smartphones around. With an aluminum body, 2.5D shaped Corning<sup>Ⓡ</sup> Gorilla<sup>Ⓡ</sup> Glass display, and smooth rounded corners, it looks as great as it feels in your hand.</div>',
        'img': 'pic-1-0055.jpg',
        'paddingBox': '5vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': '<img src="/arquivos/icon-042.svg" alt="" class="d-block" style="margin: 15px auto;" width="100" height="100">goof-proof construction', 
        'text': '<div style="padding-bottom: 10vh">Never let spills, splashes, or a little rain get in your way. A water-repellent coating helps protect your phone inside and out.<sup>‡</sup></div>',
        'img': 'pic-1-0056.jpg',
        'paddingBox': '5vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': '<img src="/arquivos/icon-043.svg" alt="" class="d-block" style="margin: 15px auto;" width="100" height="100">declutter your screen', 
        'text': 'Want an even better view? Turn on <strong>one button nav</strong> to replace the Android buttons with a simple bar and navigate your phone using intuitive gestures. Swipe left to go back or right to access recent apps, tap to go home, and more.',
        'paddingBox': '5vh 25px 0px',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': '', 
        'yt_id': '_ZNds0oraq4', 
        'btn_icon': 'icon_circleplay_100@2xvermelho.png', 
        'bg': 'pic-1-0057.jpg', 
        'height': '720px',
        'bgcolor': '#fff',
        'titlecolor': '#fff',
        'paddingBox': '5vh 25px 15vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': '<img src="/arquivos/icon-043.svg" alt="" class="d-block" style="margin: 15px auto;" width="100" height="100">not your average lock screen', 
        'text': '<div style="padding-bottom: 10vh">Intuitive shortcuts help you access your favorite features quickly and easily—without unlocking your phone. Use <strong>moto actions</strong> to launch the camera or turn on the flashlight with a simple gesture. Or try nudging your phone to preview and respond to notifications on <strong>moto display</strong>.<sup>§</sup><br><br><strong><a href="https://www.motorola.com/us/software-and-apps" style="color:#000000;" target="_blank">Learn more</a></strong></div>',
        'img': 'pic-1-0058.jpg',
        'paddingBox': '5vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': '<img src="/arquivos/icon-044.svg" alt="" class="d-block" style="margin: 15px auto;" width="100" height="100">security on your side', 
        'text': '<div style="padding-bottom: 10vh">Unlock in less than a second with just a touch on the fingerprint sensor, now located on the side for more comfortable access. Authenticate apps<sup>‡‡</sup> and make payments in an instant with your unique fingerprint.<sup>‡‡‡</sup></div>',
        'img': 'pic-1-0059.jpg',
        'paddingBox': '5vh 25px 0px',
    }},
     // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': '<img src="/arquivos/icon-044.svg" alt="" class="d-block" style="margin: 15px auto;" width="100" height="100">here’s looking at you', 
        'text': 'Unlock your phone simply by letting the front-facing camera see your face. Thanks to facial recognition software, you don’t need to enter your password.',
        'img': 'pic-1-0060.jpg',
        'paddingBox': '5vh 25px 10vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'more of what you love', 
        'paddingBox': '5vh 25px 10px',
    }},
    // BLOCO DE CARDS
    { 'cards': {'title': '', 'cards': [
        {'img': 'icon-026.jpg', 'title': 'Get some space', 'text': 'With 64 GB of storage, you have plenty of space for music, movies, and photos.<sup>§§</sup>'},
        {'img': 'icon-027.jpg', 'title': 'Go high speed', 'text': 'The Qualcomm<sup>®</sup> Snapdragon<sup>™</sup> 835 octa-core processor with 4 GB of memory keeps up with everything you want to do.'},
        {'img': 'icon-028.png', 'title': 'Upgrade on us', 'text': 'Receive two future upgrades to the latest Android<sup>®</sup> OS, absolutely free.'},
    ]} },
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'paddingBox': '40px 25px 5vh', 'specifications': [
        {'title': 'Get ready for 5G', 'text': 'Be the first to get on Verizon’s 5G network when it launches by adding the optional <strong>5G moto mod™</strong>.<sup>*</sup><br><strong style="text-decoration: underline"><a href="https://www.motorola.com/us/products/moto-mods/moto-5g" style="color:#000000;" target="_blank">Learn more</a></strong>', 'icon': 'icon-029.png'},
        {'title': 'All-day battery', 'text': 'Go from morning to night with a 3000 mAh battery. Plus, get half a day’s power in half an hour of charging with <strong>turbopower™</strong>.', 'icon': 'icon-007.png'},
        {'title': 'Endless possibilities', 'text': 'moto mods let you customize your <strong>moto z<sup>3</sup></strong>with features that reflect your passions and interests.<br><strong style="text-decoration: underline"><a href="https://www.motorola.com/us/products/moto-mods" style="color:#000000;" target="_blank">Learn more</a></strong>', 'icon': 'icon-030.png'},
        {'title': 'Dual depth-sensing rear cameras', 'text': 'Capture and edit brilliant photos and self portraits in any light. The smart cameras even recognize objects and text.', 'icon': 'icon-031.png'},
        {'title': '6” edge-to-edge display', 'text': 'Enjoy your favorite content in living color on a Max Vision Full HD+ display with an 18:9 aspect ratio.', 'icon': 'icon-032.png'},
        {'title': 'Exclusive moto experiences', 'text': 'Simple shortcuts and natural hand gestures help you access your favorite features and content quickly, easily, and securely.', 'icon': 'icon-034.png'},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}