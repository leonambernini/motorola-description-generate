// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#fff', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'Amazon Alexa, right on your phone', 
        'text': '<div style="padding-bottom: 5vh">Whether you are at home, work, a friends house, or out and about, just double press the power button when your phone is unlocked and ask Alexa to check the weather, play music, and more.</div>', 
        'img': 'pic-1-0158.jpg',
        'paddingBox': '5vh 25px 10vh',
    }},
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'features', 'paddingBox': '5vh 25px 5vh', 'specifications': [
        {'title': 'Effortless access to Amazon', 'text': 'Popular pre-installed Amazon apps allow quick access to your content, order history, wish list, shipment updates, and more. Plus, shop a personalized selection of daily deals right from the home screen using the Amazon Widget.', 'icon': ''},
        {'title': 'Prime benefits at your fingertips', 'text': 'A single sign-on experience provides Prime members with easy access to tens of thousands of movies and TV episodes, over two million songs, unlimited photo storage and backup, free 2-day shipping, and more.', 'icon': ''},
        {'title': 'Amazon Alexa enabled', 'text': 'Take the life-enhancing capabilities of Alexa anywhere you take your phone. Ask questions, plan your day, listen to music, and more. Wherever, whenever', 'icon': ''},
        {'title': 'Unlocked for carrier freedom', 'text': 'The new <strong>moto z³ play</strong> is unlocked so you can change carriers and keep your phone by just swapping SIM cards. Compatible with AT&amp;T, T-Mobile, Sprint,and Verizon networks.', 'icon': ''},
        {'title': 'Battery for the weekend', 'text': 'Get up to 40 combined hours of battery life when you snap on the <strong>moto power pack</strong>.<sup>†</sup>', 'icon': ''},
        {'title': 'Endless possibilities', 'text': 'Let <strong>moto mods </strong>help you customize your phone with features that reflect your passions and interests. <u><a href="https://www.motorola.com/us/products/moto-mods" style="color:#000000;" target="_blank">See them all</a></u>', 'icon': ''},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}