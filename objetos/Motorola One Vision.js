// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#000', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#fff', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#fff', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#000', // COR DE FUNDO DO SLIDER
    'cards-bg': '#000', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'picture brilliance', 
        'text': 'Introducing <strong>motorola one vision</strong>. With an advanced 48MP<sup>††</sup> camera system powered by AI and Quad Pixel technology, you can shoot like a pro in any light.</span>', 
        'paddingBox': '15vh 25px 0vh',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': 'Watch the video', 
        'yt_id': 'L_EtLjMKnUU', 
        'btn_icon': 'icon_circleplay_100@2x.png', 
        'bg': 'pic-2-0117.jpg', 
        'height': '1080',
        'paddingBox': '0vh 25px 10vh',
    }},
    // BLOCO TEXTO DIREITA
    { 'info-right': {
        'title': 'relax. we’ve got Android here to protect you.', 
        'text': 'The <strong>motorola one vision</strong> is powered by Android 9™(Pie). That means you’re safe, secure, and with two guaranteed OS updates, ready for the future**.', 
        'img': 'pic-2-0118.jpg',
        'paddingBox': '0vh 25px 10vh',
    }},
    // BLOCO TEXTO ESQUERDA
    { 'info-left': {
        'title': 'ultra-wide 21:9 CinemaVision display', 
        'text': 'Enjoy immersive movies and gaming on a 6.3" CinemaVision (21:9) Full HD+ display that stretches from edge to edge. Slimmer than most phones with this screen size, it’s still easy to use with just one hand.', 
        'img': 'pic-2-0119.jpg',
        'paddingBox': '0vh 25px 10vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'clear pics. super selfies.', 
        'text': 'You’re always seen in the best light with the 5 MP selfie cam and flash. Plus, the 8 MP rear-facing camera focuses in an instant, so you don’t miss a moment. Your photos look great indoors and on dark cloudy days, thanks to auto enhancement.', 
        'img': 'pic-1-0118.jpg',
        'paddingBox': '0vh 25px 10vh',
    }},
        // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'super sweet features', 
        'text': '',
        'paddingBox': '15vh 25px 0px',
    }},
    // BLOCO DE CARDS
    { 'cards': {'title': '', 'paddingBox': '2vh 25px 0px', 'cards': [
        {'img': 'icon-105.jpg', 'title': 'Android™ Oreo™ (Go edition)', 'text': 'Save space with fewer pre-installed apps, smaller app sizes for many popular apps, and more storage. Enjoy a built-in data saver, plus built-in protection with Google Play Protect.'},
        {'img': 'icon-106.jpg', 'title': 'Dedicated microSD card slot', 'text': 'Take more of your favorite media with you—and still carry a second SIM card. Thanks to a dedicated microSD card slot, you can add up to 128 GB of photos, songs, and movies.†'},
        {'img': 'icon-107.jpg', 'title': 'Qualcomm® Snapdragon™ quad-core processor', 'text': 'When it comes to performance, we don’t play games. With a Qualcomm® Snapdragon™ quad-core processor, you can feel the difference in games, music, and videos.'},
    ]} },
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'paddingBox': '40px 25px 5vh', 'specifications': [
        {'title': '5.3" Max Vision display', 'text': 'Get a big screen in a slim body for a better view of websites, games, and more.', 'icon': 'icon-108.svg'},
        {'title': 'Fingerprint reader', 'text': 'Never mind remembering a passcode. Your fingerprint unlocks your phone.', 'icon': 'icon-109.svg'},
        {'title': '8 MP camera with autofocus', 'text': 'Capture sharp photos even in low light conditions—and even when there’s no time to focus.', 'icon': 'icon-110.svg'},
        {'title': 'Android™ Oreo™ (Go edition)', 'text': 'Enjoy the best of Android for better performance, peace of mind, and protection.', 'icon': 'icon-111.svg'},
        {'title': 'Expandable storage', 'text': 'Make room for more photos, songs, and videos thanks to an additional microSD card slot.†', 'icon': 'icon-112.svg'},
        {'title': 'Fast performance', 'text': 'Get all the speed you need with a quad-core processor.', 'icon': 'icon-113.svg'},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}