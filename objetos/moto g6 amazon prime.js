// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#fff', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'envision the possibilities with ultimate Amazon Prime access', 
        'text': '<div style="padding-bottom: 5vh">Meet <strong>moto g<sup>6</sup>.</strong> With a 5.7" Full HD+ Max Vision display, advanced imaging software, and a long-lasting battery,* it’s impressive any way you look at it. Plus, with a pre-installed selection of Amazon apps, including the Amazon Widget, enjoy easy access to daily deals, Prime movies and TV shows, Prime Music, Prime Photos storage, and more, with a single sign-on experience.</div>',
        'paddingBox': '5vh 25px 0vh',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': '<div style="color: #fee500">Watch the video</div>', 
        'yt_id': '20vwZ-ipZfM', 
        'btn_icon': 'icon_circleplay_100@2xvermelho.png', 
        'bg': 'pic-1-0041.jpg', 
        'height': '430px',
        'bgcolor': '#fff',
        'titlecolor': '#fff'
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'Amazon Alexa, right on your phone', 
        'text': '<div style="padding-bottom: 5vh">Whether you’re in a hotel, a friend’s house or simply out and about, the features and benefits of Alexa are always with you.</div>', 
        'img': 'pic-1-0159.jpg',
        'paddingBox': '5vh 25px 0px',
    }},
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'features', 'paddingBox': '5vh 25px 5vh', 'specifications': [
        {'title': 'Effortless access to Amazon', 'text': 'Popular pre-installed Amazon apps allow quick access to your content, order history, wish list, shipment updates, and more. Plus, shop a personalized selection of daily deals right from the home screen using the Amazon Widget.', 'icon': ''},
        {'title': 'Prime benefits at your fingertips', 'text': 'A single sign-on experience provides Prime members with easy access to tens of thousands of movies and TV episodes, over two million songs, unlimited photo storage and backup, free 2-day shipping, and more.', 'icon': ''},
        {'title': 'Amazon Alexa enabled', 'text': 'Take the life-enhancing capabilities of Alexa anywhere you take your phone. Ask questions, plan your day, listen to music, and more. Wherever, whenever', 'icon': ''},
        {'title': 'Unlocked for carrier freedom', 'text': 'The new <strong>moto g</strong> is unlocked so you can change carriers and keep your phone by just swapping SIM cards. Compatible with AT&amp;T, T-Mobile, Sprint,and Verizon networks.', 'icon': ''},
        {'title': 'Max Vision display', 'text': 'Enjoy edge-to-edge views on a new 5.7" Full HD+ display with an 18:9 aspect ratio.', 'icon': ''},
        {'title': 'Advanced imaging software', 'text': 'Unleash your creativity with dual rear cameras and tools for everything from stunning portrait shots to hilarious face filters.', 'icon': ''},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}