// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#fff', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#fff', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#0d0f23', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [

    // BLOCO TEXTO NA ESQUERDA
    { 'info-left': {
        'title': '', 
        'text': '',
        'movie': {
            'yt_id': '8kjf6ExuEso', 
            'height': '430px',
        }
    }},
    { 'info-2-text': {
        'title': 'A', 
        'text': 'aaaaa', 
        'paddingBox1': '30px 60px', 
        'title2': 'B', 
        'text2': 'bbbbb', 
        'paddingBox2': '30px 50px'
    }},
    {'text-bg': {
        'title': 'our largest camera sensor. your best photos.', 
        'text': 'Pictures turn out great even when the lighting isn’t, thanks to a huge 48 MP rear camera sensor** with new Night Vision mode. Plus, there’s optical image stabilization (OIS) to automatically steady unwanted camera movement, and artificial intelligence features to help you shoot like a pro.', 
        'bg': 'GamePad_Lifestyle_Full_Desktop_3840x700.jpg', 
        'bgcolor': '#0d0f23',
        'titlecolor': '#fff',
        'col': '4',
    }},
    // MOSAIC
    { 'mosaic': {
        'title': 'explore the moto g⁵ˢ plus',
        'items': [
            {'width': '50%', 'height': '350px', 'bg': 'pic-2-0028.jpg', 'title': 'new advanced cameras', 'text': 'Shoot like a pro with dual 13 MP rear cameras', 'btn': 'Learn more'},
            {'width': '25%', 'height': '350px', 'bg': 'pic-2-0029.jpg', 'title': 'all-metal design', 'text': 'Beautifully crafted. Stronger than ever.', 'btn': 'Learn more'},
            {'width': '25%', 'height': '350px', 'bg': 'pic-2-0030.jpg', 'title': 'big + fast', 'text': 'Get the blazing-fast speed you crave on an even bigger, full HD display', 'btn': 'Learn more'},
            {'width': '25%', 'height': '350px', 'bg': 'pic-2-0031.jpg', 'title': 'battery + turbopower™', 'text': 'All-day battery and blazing-fast charging*', 'btn': 'Learn more'},
            {'width': '25%', 'height': '350px', 'bg': 'pic-2-0032.jpg', 'title': 'fingerprint reader', 'text': 'The new fingerprint reader does way more than just unlock your phone', 'btn': 'Learn more'},
            {'width': '50%', 'height': '350px', 'bg': 'pic-2-0033.jpg', 'title': 'unlocked', 'text': 'Moto G⁵ˢ Plus is unlocked so you can use it on any major carrier', 'btn': 'Learn more'},
        ],
        'modalbg': '#ff6901',
        'modalcolor': '#fff',
        'modalBtnClose': 'icon_xCLose_50.svg',
        'modalItems': [
            {'model': 'text-image', 'title': 'double the cameras. way better photos.', 'text': 'We didn’t just improve the camera—we rebuilt it, with new dual 13 MP rear cameras and smart software that puts the power of a professional SLR camera in the palm of your hand.<br><br>Snap crystal clear shots, then add a blur effect to make portraits pop. Switch up a classic black & white snap with a splash of color.† Or, have some fun replacing the background with any image from your Google Photos Library.†††<br><br>Switch to the 8 MP wide angle front camera with LED flash for picture-perfect selfies, day or night. Use Panorama Mode to capture more of a scenic background.', 'img': 'pic-1-0023.jpg', 'colText': '5', 'colImage': '7'},
            {'model': 'text-movie', 'title': 'double the cameras. way better photos.', 'text': 'asdasdadaa das d asd sa d as d asd as d as d asd a sd asd as d sa d sa d asd sa d asd as d asd a sd as d asdasdasdasdasdasdas d asd as d as d ass', 'img': '', 'colText': '5', 'colImage': '7', 'movie': {
                'title': '', 
                'yt_id': 'n8j04xLMP_E', 
                'btn_icon': 'icon_circleplay_100@2x.png', 
                'bg': 'pic-1-0061.jpg', 
                'height': '100%',
                'bgcolor': '#000',
                'titlecolor': '#fff',
            }},
            {'model': 'cards', 'title': 'for everyone who’s always looking for more', 'text': '', 'cards': [
                {'img': 'pic-1-0024.jpg', 'title': 'quad-core processor', 'text': 'With a quad-core processor, play games, swipe through apps, and scroll through websites, without any lag.§', 'bg': '#ff6a00', 'col': '4'},
                {'img': 'pic-1-0024.jpg', 'title': 'quad-core processor', 'text': 'With a quad-core processor, play games, swipe through apps, and scroll through websites, without any lag.§', 'bg': '#ff6a00', 'col': '4'},
                {'img': 'pic-1-0024.jpg', 'title': 'quad-core processor', 'text': 'With a quad-core processor, play games, swipe through apps, and scroll through websites, without any lag.§', 'bg': '#ff6a00', 'col': '4'},
            ]},
            {'model': 'multi-text', 'texts': [
                {'type': 'col', 'col': '8', 'title': 'make a splash', 'text': 'Never let spills, splashes, or a little rain get in your way. The Moto E uses a water-repellent nanocoating to protect your phone inside and out.*', 'img': 'pic-1-0026.jpg', 'colText': '6', 'colImage': '6'},
                {'type': 'card', 'col': '4', 'title': 'one less thing to remember', 'text': 'Go ahead, forget your passcode. All you need is the touch of your finger to instantly unlock your phone.**', 'img': 'pic-1-0026.jpg'},
            ]}
        ],
    }},
    // SPECIFICATION MORE
    { 'specification-btn-more': {
        'title': 'specifications', 
        'img': 'pic-1-0062.jpg', 
        'bgcolor': '#f5f5f5',
        'titlecolor': '#000',
        'col1': '<h5>Amazon Alexa, anytime, anywhere</h5><p>Ask Alexa and get the answers you need, even when you’re not at home.</p><h5>Boombox with brains</h5><p>Turn the volume way up. Alexa responds to voice commands over music without skipping a beat.</p>',
        'col2': '<h5>Amazon Alexa, anytime, anywhere</h5><p>Ask Alexa and get the answers you need, even when you’re not at home.</p><h5>Boombox with brains</h5><p>Turn the volume way up. Alexa responds to voice commands over music without skipping a beat.</p>',
        'more': {
            'img': 'pic-1-0060.jpg', 
            'bgcolor': '#fff',
            'titlecolor': '#000',
            'col1': '<h5>Amazon Alexa, anytime, anywhere</h5><p>Ask Alexa and get the answers you need, even when you’re not at home.</p><h5>Boombox with brains</h5><p>Turn the volume way up. Alexa responds to voice commands over music without skipping a beat.</p>',
            'col2': '<h5>Amazon Alexa, anytime, anywhere</h5><p>Ask Alexa and get the answers you need, even when you’re not at home.</p><h5>Boombox with brains</h5><p>Turn the volume way up. Alexa responds to voice commands over music without skipping a beat.</p>',
        }
    }},

    // CARD 2
    { 'cards2': {
        'title': 'just ask, wherever you are', 
        'text': 'Alexa is as mobile as your phone.&nbsp;Use it wherever <br>you have 4G or Wi-Fi.<sup>‡</sup>&nbsp;<a href="https://mobilesupport.lenovo.com/us/en/solution/MS124835" target="_blank" style="color:#FFFFFF;"><strong><u>See all Alexa skills</u></strong></a>',
        'bgcolor': '#69c246',
        'titlecolor': '#fff',
        'cards': [
            {'img': 'icon-035.jpg', 'imgHeight': '', 'imgWidth': '', 'title': '', 'text': '', 'bgcolor': '#69c246', 'titlecolor': '#fff'},
            {'img': 'icon-035.jpg', 'imgHeight': '', 'imgWidth': '', 'title': '', 'text': '', 'bgcolor': '#69c246', 'titlecolor': '#fff'},
            {'img': 'icon-035.jpg', 'imgHeight': '', 'imgWidth': '', 'title': '', 'text': '', 'bgcolor': '#69c246', 'titlecolor': '#fff'},
        ]
    }},
    // CARD 3
    { 'cards3': {
        'title': 'meet our smartest, loudest speaker yet', 
        'bgcolor': '#fff',
        'titlecolor': '#000',
        'cards': [
            {'img': 'icon-035.jpg', 'imgHeight': '', 'imgWidth': '', 'title': '', 'text': '', 'bgcolor': '#69c246', 'titlecolor': '#fff'},
            {'img': 'icon-035.jpg', 'imgHeight': '', 'imgWidth': '', 'title': '', 'text': '', 'bgcolor': '#69c246', 'titlecolor': '#fff'},
            {'img': 'icon-035.jpg', 'imgHeight': '', 'imgWidth': '', 'title': '', 'text': '', 'bgcolor': '#69c246', 'titlecolor': '#fff'},
        ]
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': 'Watch the video', 
        'yt_id': 'n8j04xLMP_E', 
        'btn_icon': 'icon_circleplay_100@2x.png', 
        'bg': 'maxresdefault.jpg', 
        'height': ''
    }},
    // BLOCO VIDEO SLIDER
    { 'movie-slider': { 'movies': [
        {
            'title': 'Watch the video 2', 
            'yt_id': 'n8j04xLMP_E', 
            'btn_icon': 'icon_circleplay_100@2x.png', 
            'bg': 'pic-1-0061.jpg', 
            'height': '400',
            'bgcolor': '#000',
            'titlecolor': '#000',
        },
        {
            'title': 'Watch the video 2', 
            'yt_id': 'n8j04xLMP_E', 
            'btn_icon': 'icon_circleplay_100@2x.png', 
            'bg': 'pic-1-0061.jpg', 
            'height': '400',
            'bgcolor': '#000',
            'titlecolor': '#fff',
        }
    ]}},
    // BLOCO TEXTO NA ESQUERDA
    { 'info-left': {
        'title': '', 
        'text': '', 
        'img': 'moto-5g.png', 
        'btn_more': '', 
        // 'more_info': 'more-1', // REMOVER ESSA LINHA CASO NÃO TENHA VER MAIS
        'imgp0': true,
    }},
    // BLOCO TEXTO NA DIREITA
    { 'info-right': {
        'title': '', 
        'text': '', 
        'img': 'moto-5g.png', 
        'btn_more': '', 
        // 'more_info': 'more-1' // REMOVER ESSA LINHA CASO NÃO TENHA VER MAIS
    }},
    // BLOCO TEXTO NO TOP
    { 'info-top': {
        'title': '', 
        'text': '', 
        'img': 'moto-5g.png', 
        'btn_more': '', 
        // 'more_info': 'more-1' // REMOVER ESSA LINHA CASO NÃO TENHA VER MAIS
    }},
    // BLOCO COM EFEITO DE FADE
    { 'effect': {
        'title': '', 
        'text': '', 
        'img': 'moto-5g.png'
    }},
    // BLOCO DE SLIDER
    { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'yt_id': 'n8j04xLMP_E', 'btn_icon': 'icon_circleplay_100@2x.png'},
    ]} },
    // BLOCO DE CARDS
    { 'cards': {'title': 'more features you’ll love', 'cards': [
        {'img': 'stingray-headphone-jack.jpg', 'title': '', 'text': ''},
        {'img': 'stingray-headphone-jack.jpg', 'title': '', 'text': ''},
        {'img': 'stingray-headphone-jack.jpg', 'title': '', 'text': ''},
        {'img': 'stingray-headphone-jack.jpg', 'title': '', 'text': ''},
    ]} },
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'specifications': [
        {'title': '', 'text': '', 'icon': 'specs-performance.svg'},
        {'title': '', 'text': '', 'icon': 'specs-performance.svg'},
        {'title': '', 'text': '', 'icon': 'specs-performance.svg'},
        {'title': '', 'text': '', 'icon': 'specs-performance.svg'},
        {'title': '', 'text': '', 'icon': 'specs-performance.svg'},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '48 MP rear camera sensor** with 4x greater light sensitivity', 'bgcolor': '#0d0f23', 'text': 'Our largest-ever sensor with Quad Pixel technology combines four pixels into one large 1.6µm pixel for vastly improved low light performance. In other words, you get incredibly sharp 12 MP photos—brighter shots with way less noise.', 'img': 'pic-1-0086.jpg'} },
        { 'info-right': {'title': 'capture the night', 'bgcolor': '#0d0f23', 'text': 'Switch to Night Vision mode in low light conditions to bring more details out of the dark. Capture bright, vivid photos with stunning clarity and more accurate colors.', 'img': 'pic-1-0087.jpg'} },
        { 'info-left': {'title': 'shoot photos like a pro with AI', 'bgcolor': '#0d0f23', 'text': 'With new features like Smart Composition, Auto Smile Capture, and AI Portrait Lighting, artificial intelligence (AI) inside the moto z4 looks for ways to give you the most professional-looking photos possible.', 'img': 'pic-1-0088.jpg'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}