// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#fff', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'envision the possibilities', 
        'text': 'Meet <strong>moto g<sup>6</sup></strong>. With a 5.7" Full HD+ Max Vision display, advanced imaging software, and a long-lasting battery,* it’s impressive any way you look at it.',
        'paddingBox': '5vh 25px 5vh',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': '<div style="color: #fee500">Watch the video</div>', 
        'yt_id': '20vwZ-ipZfM', 
        'btn_icon': 'icon_circleplay_100@2xvermelho.png', 
        'bg': 'pic-1-0041.jpg', 
        'height': '430px',
        'bgcolor': '#fff',
        'titlecolor': '#fff'
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'the latest device to join the Google Fi network', 
        'text': 'With the new <strong>moto g</strong> on Google Fi, a wireless service from Google, get all your calls, texts, and data on one simple—and smart—plan. Your Fi phone automatically finds you the best signal, and when you travel abroad, you’ll enjoy high-speed data at the same rate you pay at home.<br><br><strong><a href="https://fi.google.com/about/" style="color:#000000;" target="_blank">Learn more</a></strong>', 
        'img': 'pic-1-0160.jpg',
        'paddingBox': '3vh 25px 5vh',
    }},
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'features', 'paddingBox': '3vh 25px 5vh','specifications': [
        {'title': 'Ready for Google Fi', 'text': 'Only Fi-ready phones can intelligently shift among mobile networks and Wi-Fi to give you clear calls and fast data — at home and around the world.', 'icon': ''},
        {'title': 'Edge-to-edge display', 'text': 'Enjoy your favorite entertainment on a new 5.7" Full HD+ display with an 18:9 aspect ratio.', 'icon': ''},
        {'title': 'Advanced imaging software', 'text': 'Unleash your creativity with dual rear cameras and tools for everything from stunning portrait shots to hilarious face filters.', 'icon': ''},
        {'title': 'All-day battery + turbopower™ charging', 'text': 'Stay unplugged longer, then get hours of power in just minutes of charging.*', 'icon': ''},
        {'title': 'Powerful performance', 'text': 'Keep it moving with a 1.8 GHz octa-core processor and 4G speed..', 'icon': ''},
        {'title': 'Multi-function fingerprint reader', 'text': 'Instantly unlock your phone with just a touch.**', 'icon': ''},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}