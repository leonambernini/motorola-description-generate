// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#fff', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#fff', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#0d0f23', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    
    // CARD 2
    { 'cards2': {
        'title': 'just ask, wherever you are', 
        'text': 'Alexa is as mobile as your phone.&nbsp;Use it wherever <br>you have 4G or Wi-Fi.<sup>‡</sup>&nbsp;<a href="https://mobilesupport.lenovo.com/us/en/solution/MS124835" target="_blank" style="color:#FFFFFF;"><strong><u>See all Alexa skills</u></strong></a>',
        'bgcolor': '#69c246',
        'titlecolor': '#fff',
        'paddingBox': '5vh 25px 5vh',
        'cards': [
            {'img': 'icon-077.svg', 'imgWidth': '80', 'imgHeight': '80', 'title': 'ask + answer', 'text': 'Alexa searches the web to answer your burning questions—fast.'},
            {'img': 'icon-078.svg', 'imgWidth': '80', 'imgHeight': '80', 'title': 'music + books', 'text': 'Play music and listen to audiobooks with hands-free voice control.'},
            {'img': 'icon-079.svg', 'imgWidth': '80', 'imgHeight': '80', 'title': 'smart home + skills', 'text': 'Connect and control your favorite apps and smart home devices, hands-free.'},
            {'img': 'icon-080.svg', 'imgWidth': '80', 'imgHeight': '80', 'title': 'news + search', 'text': 'Check your calendar, manage your to-do list, get the weather forecast, skim headlines, and more.'},
        ]
    }},
    // CARD 3
    { 'cards3': {
        'title': 'meet our smartest, loudest speaker yet', 
        'bgcolor': '#eff2f4',
        'titlecolor': '#000',
        'paddingBox': '5vh 25px 5vh',
        'cards': [
            {'img': 'pic-1-0122.jpg', 'title': 'smart stereo', 'text': 'With dual speakers cranking out crisp, clear stereo sound, it’s our most powerful boombox <strong>moto mod</strong> yet. And with Amazon Alexa built in, it’s our smartest, too. Whether you’re across the room or on the go,<sup>‡</sup> four powerful microphones ensure that Alexa always hears you.<sup>§</sup>', 'button': '', 'bgcolor': '#69c246', 'titlecolor': '#fff'},
            {'img': 'pic-1-0123.jpg', 'title': 'docked and loaded', 'text': 'With a low-profile dockable design and no set-up required, this <strong>moto mod</strong> slips seamlessly into the fabric of your life. Just snap it on and Alexa connects to your phone’s 4G or Wi-Fi network instantly. Perfect for your nightstand at home, or wherever you roam.', 'button': '', 'bgcolor': '#69c246', 'titlecolor': '#fff'},
            {'img': 'pic-1-0124.jpg', 'title': 'power your journey', 'text': 'Use Alexa to your heart’s content without draining your phone’s power. The built-in battery lasts up to 15 hours.* Plus, you can recharge your phone and moto smart speaker with Amazon Alexa at the same time. Just plug in the speaker and dock your phone.', 'button': '', 'bgcolor': '#69c246', 'titlecolor': '#fff'},
        ]
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': '<div style="font-size: 30px;"><strong>Smart Speaker with Amazon Alexa free</strong></div><br><br>with moto z² force edition**', 
        'yt_id': 'qSE9c8B643o', 
        'btn_icon': 'icon_circleplay_100@2x.png', 
        'bg': 'pic-1-0125.jpg', 
        'height': '430px',
        'bgcolor': '#eff2f4',
        'titlecolor': '#fff',
        'paddingBox': '11vh 25px',
    }},
    // SPECIFICATION MORE
    { 'specification-btn-more': {
        'title': 'specifications', 
        'img': 'pic-1-0126.jpg', 
        'bgcolor': '#eff2f4',
        'titlecolor': '#000',
        'col1': '<h5>Amazon Alexa, anytime, anywhere</h5><p>Ask Alexa and get the answers you need, even when you’re not at home.</p><h5>Boombox with brains</h5><p>Turn the volume way up. Alexa responds to voice commands over music without skipping a beat.</p>',
        'col2': '<h5>Updates on the fly</h5><p>Just ask Alexa to pull up your to-do list, calendar, weather forecast, music controls and more. No matter where you are.</p><h5>Dockable design</h5><p>Make any place feel like home with the convenience of a compact, dock design.</p>',
        'more': {
            'img': 'pic-1-0127.png', 
            'bgcolor': '#fff',
            'titlecolor': '#000',
            'col1': '<h5>Dimensions</h5><p>153.2 x 73.2 x 22.8 mm</p><h5>Weight</h5><p>168 g</p><h5>Number of microphones</h5><p>4 far field microphones</p><h5>Number of speakers</h5><p>2 @ 27 mm diameter, stereo sound</p><h5>Speaker power</h5><p>6W</p><h5>Frequency response range</h5><p>200 Hz - 20 kHz</p><h5>Loudness</h5><p>82 dBSPL @ 0.5m</p><h5>Speakerphone support</h5><p>Yes</p>',
            'col2': '<h5>Integrated battery</h5><p>Yes</p><h5>Battery size</h5><p>1530 mAh</p><h5>Battery life</h5><p>Up to 15 hours*</p><h5>External charging</h5><p>Yes</p><h5>Charge rate</h5><p>1.5A/7.5W rate</p><h5>Water protection</h5><p>Splashproof<sup>†</sup></p><h5>Compatibility</h5><p class="p1"><strong>moto smart speaker with Amazon Alexa</strong> is compatible with any phone in the <a href="https://www.motorola.com/us/products/moto-z-family"><strong style="color: #0090a6">moto z family</strong></a>.</p>',
        }
    }},

    // CARD 3
    { 'cards3': {
        'title': 'go beyond your smartphone', 
        'bgcolor': '#fff',
        'titlecolor': '#000',
        'cards': [
            {'img': 'pic-1-0128.jpg', 'title': 'moto z family', 'text': 'Thinner, lighter, faster premium smartphones that transform to support your interests with Moto Mods.', 'button': '<a href="https://www.motorola.com/us/products/moto-z-family" style="background-color: #fff; border: 1px solid #fff; color: #0090a6;">Learn more</a>', 'bgcolor': '#46c7e1', 'titlecolor': '#fff'},
            {'img': 'pic-1-0129.jpg', 'title': 'moto mods family', 'text': 'Your phone becomes a movie projector, stereo speaker, battery powerhouse, and more, in a snap.', 'button': '<a href="https://www.motorola.com/us/products/moto-mods" style="background-color: #fff; border: 1px solid #fff; color: #0090a6;">Learn more</a>', 'bgcolor': '#46c7e1', 'titlecolor': '#fff'},
            {'img': 'pic-1-0130.jpg', 'title': 'moto mod development kit', 'text': 'Dream. Make. Ship. It’s that simple to bring your ideas to life and transform the smartphone in a new way.', 'button': '<a href="https://developer.motorola.com/" style="background-color: #fff; border: 1px solid #fff; color: #0090a6;">Learn more</a>', 'bgcolor': '#46c7e1', 'titlecolor': '#fff'},
        ]
    }},
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '48 MP rear camera sensor** with 4x greater light sensitivity', 'bgcolor': '#0d0f23', 'text': 'Our largest-ever sensor with Quad Pixel technology combines four pixels into one large 1.6µm pixel for vastly improved low light performance. In other words, you get incredibly sharp 12 MP photos—brighter shots with way less noise.', 'img': 'pic-1-0086.jpg'} },
        { 'info-right': {'title': 'capture the night', 'bgcolor': '#0d0f23', 'text': 'Switch to Night Vision mode in low light conditions to bring more details out of the dark. Capture bright, vivid photos with stunning clarity and more accurate colors.', 'img': 'pic-1-0087.jpg'} },
        { 'info-left': {'title': 'shoot photos like a pro with AI', 'bgcolor': '#0d0f23', 'text': 'With new features like Smart Composition, Auto Smile Capture, and AI Portrait Lighting, artificial intelligence (AI) inside the moto z4 looks for ways to give you the most professional-looking photos possible.', 'img': 'pic-1-0088.jpg'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}