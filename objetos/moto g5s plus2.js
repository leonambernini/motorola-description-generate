// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#0d0f23', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // MOSAIC
    { 'mosaic': {
        'title': '<div style="padding-bottom: 2vh; padding-top: 2vh">explore the moto g5s plus</div>',
        'items': [
            {'width': '50%', 'height': '350px', 'bg': 'pic-3-0003.jpg', 'title': 'new advanced cameras', 'text': 'Shoot like a pro with dual 13 MP rear cameras', 'btn': 'Learn more'},
            {'width': '25%', 'height': '350px', 'bg': 'pic-3-0004.jpg', 'title': 'all-metal design', 'text': 'Beautifully crafted. Stronger than ever.', 'btn': 'Learn more'},
            {'width': '25%', 'height': '350px', 'bg': 'pic-3-0005.jpg', 'title': 'big + fast', 'text': 'Get the blazing-fast speed you crave on an even bigger, full HD display', 'btn': 'Learn more'},
            {'width': '25%', 'height': '350px', 'bg': 'pic-3-0006.jpg', 'title': 'battery + turbopower™', 'text': 'All-day battery and blazing-fast charging*', 'btn': 'Learn more'},
            {'width': '25%', 'height': '350px', 'bg': 'pic-3-0007.jpg', 'title': 'fingerprint reader', 'text': 'The new fingerprint reader does way more than just unlock your phone', 'btn': 'Learn more'},
            {'width': '50%', 'height': '350px', 'bg': 'pic-3-0008.jpg', 'title': 'unlocked', 'text': 'Moto G⁵ˢ Plus is unlocked so you can use it on any major carrier', 'btn': 'Learn more'},
        ],
        'modalbg': '#ff6901',
        'modalcolor': '#fff',
        'modalBtnClose': 'icon_xCLose_50.svg',
        'modalItems': [
            {'model': 'text-image', 'title': 'double the cameras. way better photos.', 'text': 'We didn’t just improve the camera—we rebuilt it, with new dual 13 MP rear cameras and smart software that puts the power of a professional SLR camera in the palm of your hand.<br><br>Snap crystal clear shots, then add a blur effect to make portraits pop. Switch up a classic black & white snap with a splash of color.† Or, have some fun replacing the background with any image from your Google Photos Library.†††<br><br>Switch to the 8 MP wide angle front camera with LED flash for picture-perfect selfies, day or night. Use Panorama Mode to capture more of a scenic background.', 'img': 'pic-2-0034.jpg', 'colText': '5', 'colImage': '7'},
            {'model': 'text-image', 'title': 'metal marvel', 'text': 'Wow the crowd with our new all-metal unibody design, precision-crafted from a single piece of high-grade aluminum. Diamond cut and painstakingly polished for a flawless finish, it’s stunningly beautiful and stronger than ever. Choose from Lunar Gray or Blush Gold designs.', 'img': 'pic-2-0035.jpg', 'colText': '5', 'colImage': '7'},
            {'model': 'text-image', 'title': 'bring the big screen with you', 'text': 'Now movie night can be anywhere. Bring blockbusters to life on an impressive 5.5" full HD display with the vivid colors and fine details you’d expect to find only on a premium phone. With a blazing-fast Qualcomm<sup>®</sup> Snapdragon™ 2.0 GHz octa-core processor, powerful graphics capabilities, and support for 4G speed, annoying lags and load times are a thing of the past.<sup>‡</sup>', 'img': 'pic-2-0036.jpg', 'colText': '5', 'colImage': '7'},
            {'model': 'text-image', 'title': 'power couple', 'text': 'Stay unplugged. The 3000 mAh all-day battery holds enough power to get you through your day. When it’s time to power up, don’t slow down. The included TurboPower™charger provides up to 6 hours of battery life with just a quick 15 minute charge.*', 'img': 'pic-2-0037.jpg', 'colText': '5', 'colImage': '7'},
            {'model': 'cards', 'title': 'one touch does it all', 'text': '', 'cards': [
                {'img': 'pic-2-0038.jpg', 'title': 'Power up your print', 'text': 'Go ahead, forget your password. With <strong>moto g⁵ˢ plus</strong>, your fingerprint unlocks your phone. Plus, store up to five fingerprints to share the wealth.', 'bg': '#ff6a00', 'col': '4'},
                {'img': 'pic-2-0039.jpg', 'title': 'Navigate with a touch', 'text': 'The new one-button nav makes old school on-screen buttons disappear, so you get a much better view. Use the fingerprint reader as a directional key: swipe left to go back, right to access recent apps, and tap to go home. Unlock or lock your phone with one long press.', 'bg': '#ff6a00', 'col': '4'},
                {'img': 'pic-2-0040.jpg', 'title': 'Make online payments with just a tap', 'text': 'With <strong>moto g⁵ˢ plus</strong>, you can login to apps and make online payments with Android Pay by simply touching the fingerprint reader. You can also shop the Google Play store and pay using your fingerprint—no password required.', 'bg': '#ff6a00', 'col': '4'},
            ]},
            {'model': 'text-image', 'title': 'freedom to choose', 'text': '<strong>moto g⁵ˢ plus</strong> is unlocked so you can switch among all major U.S. networks, like Verizon, AT&amp;T, T-Mobile, Sprintt<sup>‡</sup>, Consumer Cellular, Republic Wireless, and many more, by simply swapping SIM cards.<br><br><a href="https://www.motorola.com/us/carrier-compatibility" style="text-decoration:underline;color: #FFFFFF;">View carrier compatibility chart</a>', 'img': 'pic-2-0041.jpg', 'colText': '5', 'colImage': '7'},
        ],
    }},

    // SPECIFICATION MORE
    { 'specification-btn-more': {
        'title': 'specifications', 
        'img': 'pic-2-0042.jpg', 
        'bgcolor': '#f5f5f5',
        'titlecolor': '#000',
        'col1': '<h5>All-metal unibody design</h5><p class="p1">Painstakingly polished for a flawless finish. Stronger than ever.</p><h5>Professional-looking photos</h5><p>Dual 13 MP rear cameras with enhanced software for depth of field effects, plus a wide angle 8 MP selfie camera with LED flash.</p><h5>Bigger display, blazing-fast speed</h5><p class="p1">Movie time, anytime, with a 5.5” full HD display, fast 2.0 GHz octa-core processor, and 4G speed.</p>',
        'col2': '<h5>Fuel up fast</h5><p class="p1" style="margin-bottom: 10px;">Go all-day on a single charge. Then get up to 6 hours of power in just 15 minutes of charging.<sup>*</sup></p><h5>Multi-function fingerprint reader</h5><p class="p1">Unlock and navigate your phone with a simple touch or swipe.</p><h5>Universally unlocked</h5><p class="p1">Moto G<sup>5<strong>S</strong></sup> Plus is unlocked for all major U.S. carriers, like Verizon, AT&amp;T, T-Mobile, Sprint<sup>‡</sup>, and Consumer Cellular, so you can switch carriers and keep your phone.</p>',
        'more': {
            'img': 'pic-2-0043.jpg', 
            'bgcolor': '#fff',
            'titlecolor': '#000',
            'col1': '<h5>Carrier friendly</h5><p>Moto G<sup>5<strong>S</strong></sup> Plus is universally unlocked and works on Verizon, AT&amp;T, T-Mobile, Sprint<sup>‡</sup>, and Consumer Cellular. It is also compatible with other U.S. carriers, please contact your service provider for details.<br><a style="color: #0090a6" href="https://www.motorola.com/us/carrier-compatibility">View carrier compatibility chart</a></p><h5>Operating system</h5><p class="p1">Android™ 8.1, Oreo</p><h5>System architecture/processor</h5><p class="p1">Qualcomm<sup>®</sup> Snapdragon™ 625 processor with 2.0 GHz octa-core CPU and 650 MHz Adreno 506 GPU</p><h5>Memory (RAM)</h5><p class="p1">3 GB / 4 GB</p><h5>Storage (ROM)</h5><p class="p1">32 GB / 64GB internal, up to 128GB microSD Card support</p><h5>Dimensions</h5><p class="p1">76.2 x 153.5 x 8.0 mm (9.5 mm at camera bump)</p><h5>Weight</h5><p class="p1">168 g</p><h5>Display</h5><p class="p1">5.5”<br>Full HD 1080p (1920×1080)<br>401 ppi<br>Corning™ Gorilla™ Glass 3</p><h5>Battery</h5><p class="p1">All-day battery<span style="font-size:6.75px"> </span>(3000 mAh)<br>TurboPower™ for up to 6 hours of power in 15 minutes of charging<sup>*</sup></p><h5>Water protection</h5><p class="p1">Water repellent nano-coating<sup>§</sup></p><h5>Networks</h5><p class="p1">4G LTE (Cat 7)<br>CDMA / EVDO Rev A<br>UMTS / HSPA+<br>GSM / EDGE</p><h5>Bands (by model)</h5><p class="p1">Moto G<sup>5<strong>S</strong></sup> Plus – XT1806<br>CDMA: 850, 850+,1900MHz<br>GSM/GPRS/EDGE (850, 900, 1800, 1900 MHz)<br>UMTS/HSPA+ (850, 900, 1700, 1900, 2100 MHz)<br>4G LTE (B1, 2, 3, 4, 5, 7, 8, 12, 13, 17, 25, 26, 38, 41, 66) †</p><h5>SIM Card</h5><p class="p1">Nano-SIM</p>',
            'col2': '<h5>Rear camera</h5><p class="p1">Dual 13 MP cameras + depth editor<br>ƒ/2.0 aperture<br>Color balancing dual LED flash<br>8X digital zoom for photos, 4X for video<br>Drag to focus &amp; exposure<br>Quick Capture<br>Tap (anywhere) to capture<br>Best Shot<br>Professional Mode<br>Burst mode<br>Panorama Mode<br>Auto HDR<br>Video stabilization<br>4K Ultra HD video capture (30 fps)<br>Slow Motion video</p><h5>Front camera</h5><p class="p1">8 MP<br>Wide-angle lens<br>f/2.0 aperture<br>LED flash<br>Panorama mode<br>Professional mode<br>Beautification mode</p><h5>Connectivity</h5><p class="p1">Micro USB, 3.5 mm headset jack</p><h5>Bluetooth® technology</h5><p class="p1">Bluetooth version 4.2&nbsp;LE + EDR</p><h5>Speakers/microphones</h5><p class="p1">Bottom-port loud speaker<br>2-Mics</p><h5>Video capture</h5><p class="p1">4K Ultra HD (30 fps)</p<h5>Location services</h5><p class="p1">GPS<br>A-GPS<br>GLONASS</p><h5>NFC</h5><p>No</p><h5>Sensors</h5><p class="p1">Fingerprint reader<br>Accelerometer<br>Gyroscope<br>Ambient Light<br>Proximity</p><h5>Wi-Fi</h5><p class="p1">802.11 a/b/g/n (2.4 GHz + 5 GHz)</p><h5>Base color</h5><p class="p1">Lunar Gray,&nbsp;Blush Gold</p>',
        }
    }},
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '48 MP rear camera sensor** with 4x greater light sensitivity', 'bgcolor': '#0d0f23', 'text': 'Our largest-ever sensor with Quad Pixel technology combines four pixels into one large 1.6µm pixel for vastly improved low light performance. In other words, you get incredibly sharp 12 MP photos—brighter shots with way less noise.', 'img': 'pic-1-0086.jpg'} },
        { 'info-right': {'title': 'capture the night', 'bgcolor': '#0d0f23', 'text': 'Switch to Night Vision mode in low light conditions to bring more details out of the dark. Capture bright, vivid photos with stunning clarity and more accurate colors.', 'img': 'pic-1-0087.jpg'} },
        { 'info-left': {'title': 'shoot photos like a pro with AI', 'bgcolor': '#0d0f23', 'text': 'With new features like Smart Composition, Auto Smile Capture, and AI Portrait Lighting, artificial intelligence (AI) inside the moto z4 looks for ways to give you the most professional-looking photos possible.', 'img': 'pic-1-0088.jpg'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}