// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#fff', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [

    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'days of battery life. nonstop fun.', 
        'text': 'The new <strong>moto g⁷ power</strong> gives you up to 3 days<sup>*</sup> of battery life, plus hours of power in just minutes with <strong>turbopower™</strong> charging.<sup>‡‡</sup> Enjoy an ultrawide 6.2" HD+ display, the responsive performance of an octa-core processor, and so much more.',
        'paddingBox': '10vh 25px 10vh',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': 'Watch the video', 
        'yt_id': 'Du9Tq_oaGJ4', 
        'btn_icon': 'icon_circleplay_100@2x.png', 
        'bg': 'pic-1-0017.jpg', 
        'height': '430px',
        'bgcolor': '#fff',
        'titlecolor': '#fff',
        'paddingBox': '15vh 25px 0px',
    }},
    // BLOCO COM EFEITO DE FADE
    { 'effect': {
        'title': 'up to 3 days of power on a single charge', 
        'text': 'An industry-leading 5000 mAh battery lasts up to 3 days on one charge.<sup>*</sup> Plus, <strong>turbopower™</strong> gives you 9 hours of power in 15 minutes.<sup>‡‡</sup>', 
        'img': 'pic-1-0018.jpg',
        'paddingBox': '2vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'brilliant ultrawide Max Vision display', 
        'text': '<div style="padding-bottom: 10vh">Enjoy expansive views on a stunning 6.2" HD+ display. The 19:9 aspect ratio provides a big screen viewing experience, fully immersing you in movies, games, photos, and more.</div>', 
        'img': 'pic-2-0127.jpg',
        'paddingBox': '10vh 25px 10vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'clear scenes and selfies in any light', 
        'text': '<div style="padding-bottom: 14vh">The 12 MP rear camera uses phase detection autofocus (PDAF) to capture your subject in an instant. Plus, large 1.25um pixels let in more light, so you get better results even indoors and when it is dark out. Group shots are more fun, too, with group selfie mode on the 8 MP front camera.</div>', 
        'img': 'pic-2-0128.jpg',
        'paddingBox': '15vh 25px 0px',
    }},
    // BLOCO TEXTO NA ESQUERDA
    { 'info-left': {
        'title': '<div style="text-align: center">smart camera software</div>', 
        'text': '<div style="text-align: center">Hi-res zoom restores the details and image clarity that are typically lost with digital zooming.<sup>†</sup> A built-in screen flash helps you get the perfect selfie lighting every time. Shoot cinemagraphs, 4K video, and more. Plus, search what you see with Google Lens™.</div>', 
        'img': 'pic-2-0129.jpg', 
        'paddingBox': '10vh 25px 15px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'camera feature: portrait mode', 
        'text': '<div style="padding-bottom: 15vh">Add an artistic blur effect in the background for professional-looking close-up or portrait shots.</div>', 
        'img': 'pic-1-0022.jpg',
        'paddingBox': '20vh 25px 10vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'camera feature: cinemagraph', 
        'text': 'Make cinemagraphs, keeping a portion of your shot in motion while freezing everything else.',
        'paddingBox': '10vh 25px 5vh',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': '', 
        'yt_id': '9kGcjHtZoCg', 
        'btn_icon': 'icon_circleplay_100@2x.png', 
        'bg': 'pic-3-0002.jpg', 
        'height': '720px',
        'bgcolor': '#fff',
        'titlecolor': '#fff'
    }},
    // BLOCO DE SLIDER COM VÍDEO
    { 'slider': {'title': 'explore even more <span style="color:#5da64c">moto g⁷ power</span> camera features', 'titlecolor': '#000', 'sliders': [
            {'img': 'pic-1-0023.jpg', 'yt_id': 'Z2RHMtE8Qi8', 'btn_icon': 'icon_circleplay_100@2xbranco.png', 'text': '<p><strong>timelapse</strong><br><br>Shoot dramatic timelapse videos with all the action whizzing past at 4x, 8x, 16x, or 32x speed.</p>'},
            {'img': 'pic-1-0024.jpg', 'text': '<p><strong>Google Lens</strong><br><br>Recognize landmarks, objects and foreign languages and more, with Google Lens.</p>'},
            {'img': 'pic-1-0025.jpg', 'text': '<p><strong>Google Photos</strong><br><br>Free, unlimited, high-quality storage with Google Photos.<sup>§</sup>'},
    ]} },
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'responsive octa-core processor', 
        'text': '<div style="padding-bottom: 15vh">Edit photos, play 3D games, and run multiple apps at once. A Qualcomm® Snapdragon™ 632 octa-core 1.8GHz processor gives you the speed you need to do it all.</div>', 
        'img': 'pic-2-0136.jpg',
        'paddingBox': '5vh 25px 0px',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'water-repellent, inside and out', 
        'text': 'A water-repellent design protects against accidental spills, sweat, and light rain.**', 
        'img': 'pic-1-0027.jpg',
        'paddingBox': '15vh 25px 0px',
    }},
    // BLOCO COM EFEITO DE FADE
    { 'effect': {
        'title': 'exclusive moto experiences', 
        'text': 'With One Button Nav, navigate with a virtual bar. With Lift to Unlock, pick up your phone to wake the display. Look for more on the <strong style="font-weight: 600">Moto app</strong>. <a href="https://www.motorola.com/us/software-and-apps" target="_blank">Learn more</a>', 
        'img': 'pic-1-0028.jpg',
        'paddingBox': '10vh 25px 10vh',
    }},
    // BLOCO DE CARDS
    { 'cards': {'title': '<div style="padding-bottom: 15vh">more of what you love</div>', 'cards': [
        {'img': 'icon-010.jpg', 'title': 'Expanded storage', 'text': 'Store more photos, songs, and movies with a dedicated microSD card slot.<sup>††</sup>'},
        {'img': 'icon-011.jpg', 'title': 'Face unlock', 'text': 'With facial recognition software in the front-facing camera, you can unlock your phone with a glance.'},
        {'img': 'icon-012.jpg', 'title': 'Fingerprint sensor', 'text': 'Touch the fingerprint reader to wake up and unlock your phone. The sensor is discreetly located on the back within the logo.'},
    ]} },
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'your best shot', 
        'text': '<div style="padding-bottom: 0vh">Here are some of our favorite clicks from everyday <strong> moto g⁷ power</strong> photographers just like you.</div>',
        'paddingBox': '20vh 25px 0vh',
    }},
    // BLOCO DE SLIDER
    { 'slider': {'title': '', 'paddingBox': '10vh 25px 0px', 'titlecolor': '#000', 'sliders': [
            {'img': 'pic-1-0029.jpg', 'text': '<p>Photo by: Snehal S Bhat, Software Engineer, Karnataka, India</p>'},
            {'img': 'pic-1-0030.jpg', 'text': '<p>Photo by: Louis YK7 Li, Sourcing Manager, Beijing, China</p>'},
            {'img': 'pic-1-0031.jpg', 'text': '<p>Photo by: Carsten Thoms, Data & Infrastructure Manager, Ons-en-Bray, France</p>'},
            {'img': 'pic-1-0032.jpg', 'text': '<p>Photo by: Carsten Thoms, Data & Infrastructure Manager, Dublin, Ireland</p>'},
            {'img': 'pic-1-0034.jpg', 'yt_id': 'iws_C0zBzug', 'btn_icon': 'icon_circleplay_100@2xbranco.png', 'text': '<p>Photo by: Carsten Thoms, Data & Infrastructure Manager, Dublin, Ireland</p>'},
            {'img': 'pic-1-0033.jpg', 'text': '<p>Photo by: Photo by: Marco Novaresio, Operations Manager, Beijing, China</p>'},
    ]} },
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'specifications': [
        {'title': 'up to 3 days of power', 'text': 'Go days<sup>*</sup> without charging, and get 9 hours of power in just 15 minutes with <strong>turbopower™</strong>.<sup>‡‡</sup>', 'icon': 'icon-007.png'},
        {'title': '6.2" HD+ Max Vision display', 'text': 'The 19:9 aspect ratio gives you a widescreen viewing experience with movies and games.', 'icon': 'icon-005.png'},
        {'title': '12 MP camera with PDAF', 'text': 'Never miss a moment with a fast-focusing 12 MP rear camera and 8 MP front camera with screen flash.', 'icon': 'icon-0118.svg'},
        {'title': 'Octa-core processor', 'text': 'A Qualcomm® Snapdragon™ 632 octa-core 1.8GHz processor gives a performance boost in everything you do.', 'icon': 'icon-0119.svg'},
        {'title': 'Fingerprint reader', 'text': 'Touch the fingerprint reader to wake and unlock your phone instantly.', 'icon': 'icon-057.svg'},
        {'title': 'motorola-exclusive shortcuts', 'text': 'Use natural gestures to access your favorite features and apps quickly and intuitively with <a href="https://www.motorola.com/us/software-and-apps" style="color: #000000;"><strong><u>moto experiences</u></strong></a>.', 'icon': 'icon-009.png'},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}