// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#fff', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#fff', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#0d0f23', // COR DE FUNDO DO SLIDER
    'cards-bg': '#151228', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'Pick up a phone you’ll love without dropping a grand. Introducing the moto z⁴. With a 48 MP camera sensor*, 2 days of battery life<sup>‡‡</sup>, universal unlocking, and built-in Alexa Hands-Free, this Motorola smartphone has the features you want at a price you’ll be more than happy with.', 
        'text': '',
        'titleDegrade': true,
        'paddingBox': '5vh 25px 2vh',
        'bgcolor': '#151228',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': 'Watch the video', 
        'yt_id': '9ylcGAoSolE', 
        'btn_icon': 'icon_circleplay_100@2x.png', 
        'bg': 'pic-1-0152.jpg', 
        'height': '1200px',
        'bgcolor': '#000',
        'titlecolor': '#fee500',
        'paddingBox': '0vh 25px 0vh 50px',
    }},
    // BLOCO TEXTO NA ESQUERDA
    { 'info-left': {
        'title': 'just say "Alexa"', 
        'text': 'Alexa Hands-Free technology lets you play music, hear the news, check the weather, and more using only your voice. All you have to do is say, “Alexa” and it’ll take care of the rest.', 
        'img': 'pic-1-0153.jpg', 
        'bgcolor': '#0d0f23',
    }},
    // BLOCO TEXTO NA DIREITA   
    { 'info-right': {
        'title': '25 MP high-res selfie camera', 
        'text': 'In low light, choose 1.8µm Quad Pixel mode for incredibly sharp 6MP selfies with reduced noise. You can also use portrait mode to add an artistic blur effect in the background and control touch-ups with the help of AI-based beautification.<br><br><br><a class="cta-library-wrap Custom-rounded-89335-41285" href="https://www.motorola.com/us/products/moto-z-gen-4-unlocked" target="_blank"><span style="border: 1px solid #FFFFFF;background-color: rgba(0,0,0,0.0);border-radius: 25px;padding: 11px 25px;font-weight: 500 !important;color: #FFFFFF;">Learn more</span>', 
        'img': 'pic-1-0070.jpg', 
        'bgcolor': '#0d0f23',
    }},
    // BLOCO TEXTO NA ESQUERDA   
    { 'info-left': {
        'title': 'our largest camera sensor. your best photos.', 
        'text': 'Made with a huge new 48 MP rear camera sensor* with new Night Vision mode, your pictures will turn out perfect, even when the lighting isn’t. Optical image stabilization (OIS) automatically steadies unwanted camera movement, while artificial intelligence features let you add a professional quality to your photos.<br><br><br><a class="cta-library-wrap Custom-rounded-89335-41285" href="https://www.motorola.com/us/products/moto-z-gen-4-unlocked" target="_blank"><span style="border: 1px solid #FFFFFF;background-color: rgba(0,0,0,0.0);border-radius: 25px;padding: 11px 25px;font-weight: 500 !important;color: #FFFFFF;">Learn more</span>', 
        'img': 'pic-1-0086.jpg', 
        'bgcolor': '#0d0f23',
    }},
     // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'paddingBox': '40px 25px 5vh', 'bgcolor': '#0d0f23','specifications': [
        {'title': '48 MP sensor* with Night Vision', 'text': 'Take brilliant photos even in low light with NIght Vision mode.', 'icon': ''},
        {'title': 'Built in Alexa Hands-Free', 'text': 'With Amazon Alexa Hands-Free, simply by saying “Alexa” you can play music, hear the news, check the weather, and more. Wherever you are, just ask, and Alexa Hands-Free will respond instantly.', 'icon': ''},
        {'title': 'Easy access to Amazon', 'text': 'A pre-installed selection of Amazon apps provide easy access to Amazon Shopping, Amazon Music, Amazon Photos, and Audible.', 'icon': ''},
        {'title': '6.4" OLED display with on-screen fingerprint reader', 'text': 'See incredible details on the impressive 6.4" CinemaVision display. ', 'icon': ''},
        {'title': 'Personalize with moto mods™<sup>§</sup>', 'text': 'Upgrade your phone using moto mods.', 'icon': ''},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '48 MP rear camera sensor** with 4x greater light sensitivity', 'bgcolor': '#0d0f23', 'text': 'Our largest-ever sensor with Quad Pixel technology combines four pixels into one large 1.6µm pixel for vastly improved low light performance. In other words, you get incredibly sharp 12 MP photos—brighter shots with way less noise.', 'img': 'pic-1-0086.jpg'} },
        { 'info-right': {'title': 'capture the night', 'bgcolor': '#0d0f23', 'text': 'Switch to Night Vision mode in low light conditions to bring more details out of the dark. Capture bright, vivid photos with stunning clarity and more accurate colors.', 'img': 'pic-1-0087.jpg'} },
        { 'info-left': {'title': 'shoot photos like a pro with AI', 'bgcolor': '#0d0f23', 'text': 'With new features like Smart Composition, Auto Smile Capture, and AI Portrait Lighting, artificial intelligence (AI) inside the moto z4 looks for ways to give you the most professional-looking photos possible.', 'img': 'pic-1-0088.jpg'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}