// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#0d0f23', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // MOSAIC
    { 'mosaic': {
        'title': '<div style="padding-bottom: 2vh; padding-top: 2vh">get 10x optical zoom, in a snap</div>',
        'items': [
            {'width': '50%', 'height': '350px', 'bg': 'pic-2-0059.jpg', 'title': 'true zoom', 'text': 'With 10x optical zoom, add an advanced imaging experience to your phone, instantly', 'btn': 'Learn more'},
            {'width': '50%', 'height': '350px', 'bg': 'pic-2-0060.jpg', 'title': 'Hasselblad experience', 'text': 'The legendary camera maker brings their legacy and unique shooting experiences to your Moto Z', 'btn': 'Learn more'},
            {'width': '50%', 'height': '350px', 'bg': 'pic-2-0061.jpg', 'title': 'shoot + share', 'text': 'A better experience from start to finish, with Xenon flash, exclusive shooting modes and Google Photos integration', 'btn': 'Learn more'},
            {'width': '50%', 'height': '350px', 'bg': 'pic-2-0062.jpg', 'title': 'moto mods™', 'text': 'Your phone becomes a superzoom camera, a movie projector, a boombox, and more. In a snap.', 'btn': 'Learn more'},
        ],
        'modalbg': '#ff6901',
        'modalcolor': '#fff',
        'modalBtnClose': 'icon_xCLose_50.svg',
        'modalItems': [
            {'model': 'text-image', 'title': 'optical v. digital zoom. the difference is clear.', 'text': 'With 10x optical zoom, capture detailed photos from a distance without affecting resolution. Don’t worry about how far the subject is, just focus on getting the shot.', 'img': 'pic-2-0063.jpg', 'colText': '5', 'bg': '#2d2a26', 'colImage': '7'},
            {'model': 'cards', 'title': 'Hasselblad and moto are transforming mobile photography', 'text': '', 'cards': [
                {'img': 'pic-2-0064.jpg', 'title': 'Hasselblad legacy at your fingertips', 'text': 'The most renowned images in history have been taken with Hasselblad cameras, from the moon to Abbey Road. For the first time, Hasselblad is bringing its unique, iconic design and imaging experience to the mobile space.', 'bg': '#2d2a26', 'col': '4'},
                {'img': 'pic-2-0065.jpg', 'title': 'Hasselblad design and ergonomics', 'text': 'Get a real imaging experience with enhanced control for easier shooting and better results. Physical shutter and zoom controls make capturing great shots a snap.', 'bg': '#2d2a26', 'col': '4'},
                {'img': 'pic-2-0066.jpg', 'title': 'RAW images in Phocus', 'text': 'Hasselblad True Zoom shoots in RAW format so you always have the highest image quality. Plus, enjoy ultimate control with a free subscription to Hasselblad’s Phocus editing software.<br><br><a href="http://www.hasselblad.com/software/phocus" style="color: #00bcd4">Learn more</a>', 'bg': '#2d2a26', 'col': '4'},
            ]},
            {'model': 'cards', 'title': 'seamlessly shoot, share, and store', 'text': '', 'cards': [
                {'img': 'pic-2-0067.jpg', 'title': 'even light, even at night', 'text': 'Never let low light ruin a great shot. The Xenon flash lights up your subject evenly, giving you crisp, blur-free pictures in any environment.', 'bg': '#2d2a26', 'col': '4'},
                {'img': 'pic-2-0068.jpg', 'title': 'set up the perfect shot', 'text': 'Unleash your creativity with special Hasselblad shooting and scene modes to optimize your images depending on the type of shot you are trying to capture.', 'bg': '#2d2a26', 'col': '4'},
                {'img': 'pic-2-0069.jpg', 'title': 'share and store easily', 'text': 'Google Photos is the smarter way to share and store photos and videos. They’re automatically backed up, organized, and searchable, so you can find and share photos faster than ever.*', 'bg': '#2d2a26', 'col': '4'},
            ]},
            {'model': 'text-movie', 'title': 'transformation’s never been easier', 'text': 'With Moto Mods™ like Hasselblad True Zoom, do things you never thought were possible with a phone. The interchangeable backs snap onto your phone with powerful integrated magnets and are instantly ready for use. No pairing. Whatever your passion, there’s a Moto Mod™ for you.', 'img': '', 'bg': '#2d2a26', 'colText': '5', 'colImage': '7', 'movie': {
                'title': '', 
                'yt_id': '2L1zQkJ8UA', 
                'btn_icon': 'icon_circleplay_100@2x.png', 
                'bg': 'pic-2-0070.jpg', 
                'height': '100%',
                'bgcolor': '#2d2a26',
                'titlecolor': '#fff',
            }},
        ],
    }},

    // SPECIFICATION MORE
    { 'specification-btn-more': {
        'title': 'specifications', 
        'img': 'pic-2-0071.jpg', 
        'bgcolor': '#e4e4e4',
        'titlecolor': '#000',
        'col1': '<h5>works with any moto z</h5><p class="p1">Compatible with the <a href="http://www.motorola.com/us/products/moto-z-family" style="color: #0090a6; text-decoration: underline;">Moto Z Family</a> of phones.</p><h5>10x optical zoom</h5><p class="p1">Capture detailed photos from any distance without losing resolution.</p><h5>xenon flash</h5><p class="p1">Get crisp, blur-free pictures with even lighting in any environment.</p>',
        'col2': '<h5>Hasselblad design and ergonomics</h5><p class="p1">Get a real imaging experience with enhanced control for easier shooting and better results.</p><h5>shoots in RAW format</h5><p class="p1">Get the highest level of image quality and brightness, while enjoying more flexibility and control.</p><h5>easy sharing and backup</h5><p class="p1">Share pictures with family and friends while you’re still in the moment, and enjoy automatic backup.</p>',
        'more': {
            'img': 'pic-2-0072.jpg', 
            'bgcolor': '#fff',
            'titlecolor': '#000',
            'col1': '<h5>compatible phones</h5><p>The <a href="http://www.motorola.com/us/products/moto-z-family" style="color: #0090a6; text-decoration: underline;">Moto Z Family</a> of phones</p><h5>dimensions</h5><p>152.3 x 72.9 x 9.0 - 15.1 mm</p><h5>weight</h5><p>145g</p><h5>sensor resolution</h5><p>12MP</p><h5>video resolution</h5>p>1080p Full HD at 30fps</p><h5>mics</h5>p>2</p><h5>sensor type</h5><p>BSI CMOS</p><h5>sensor size</h5>p>1/2.3-inch</p><p>1.55 um</p><h5>aperture</h5><p>f3.5-6.5</p><h5>zoom</h5><p>10x optical/4x digital</p><h5>focal length</h5><p>4.5-45 mm (25-250mm 35mm equivalent)</p><h5>macro</h5><p>5cm @1x - 1.5m @10x</p><h5>flash</h5><p>Xenon flash</p><h5>flash modes</h5><p>Auto, on, off</p>',
            'col2': '<h5><h5>rear camera</h5><p class="p1">8 MP<br>ƒ / 2.2 aperture<br>1.12 um microns<br>71° lens<br>Autofocus<br>Single LED flash<br>Burst mode<br>Panorama<br>HDR<br>Beautification mode</p><h5>front camera</h5><p class="p1">5 MP<br>ƒ / 2.2 aperture<br>1.4 um microns<br>74° lens<br>Fixed focus<br>Single LED flash<br>Burst mode<br>HDR<br>Beautification mode</p><h5>video capture</h5><p class="p1">1080p (30fps)</p><h5>SIM card</h5><p class="p1">Single Nano-SIM</p><h5>connectivity</h5><p class="p1">MicroUSB<br>3.5mm headset port</p><h5>Bluetooth® technology</h5><p class="p1">Bluetooth® version 4.1 LE</p><h5>Wi-Fi</h5><p class="p1">802.11a/b/g/n<br>Wi-Fi hotspot</p><h5>speakers/microphones</h5><p class="p1">Two-in-one speaker at 84dB<br>Dual microphones</p><h5>NFC</h5><p class="p1">No</p><h5>location services</h5><p class="p1">GPS</p><h5>sensors</h5><p class="p1">Vibration<br>Proximity<br>Light<br>Accelerometer<br>Magnetometer (e-compass)<br>Fingerprint reader (in some models)**</p><h5>base color<sup>‡</sup></h5><p class="p1">Licorice Black<br>Fine Gold</p>',
        }
    }},
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '48 MP rear camera sensor** with 4x greater light sensitivity', 'bgcolor': '#0d0f23', 'text': 'Our largest-ever sensor with Quad Pixel technology combines four pixels into one large 1.6µm pixel for vastly improved low light performance. In other words, you get incredibly sharp 12 MP photos—brighter shots with way less noise.', 'img': 'pic-1-0086.jpg'} },
        { 'info-right': {'title': 'capture the night', 'bgcolor': '#0d0f23', 'text': 'Switch to Night Vision mode in low light conditions to bring more details out of the dark. Capture bright, vivid photos with stunning clarity and more accurate colors.', 'img': 'pic-1-0087.jpg'} },
        { 'info-left': {'title': 'shoot photos like a pro with AI', 'bgcolor': '#0d0f23', 'text': 'With new features like Smart Composition, Auto Smile Capture, and AI Portrait Lighting, artificial intelligence (AI) inside the moto z4 looks for ways to give you the most professional-looking photos possible.', 'img': 'pic-1-0088.jpg'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}