// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#fff', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'everything you love—plus Amazon Prime', 
        'text': '<div style="padding-bottom: 10vh">Introducing <strong>moto e⁵ play</strong>, featuring an advanced autofocus camera, an all-day battery, and fast performance—all in a water-repellent design.<sup>†</sup> This <strong>moto e</strong> comes with a pre-installed selection of Amazon apps, including the Alexa widget icon, so you can easily access daily deals, Prime movies and TV shows, Prime Music, Prime Photos storage, and more with a single sign-on experience.</div>', 
        'img': 'pic-1-0119.jpg',
        'paddingBox': '10vh 25px 0vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'Amazon Alexa, right on your phone', 
        'text': '<div style="padding-bottom: 10vh">Whether you are at home, work, a friends house, or out and about, just double press the power button when your phone is unlocked and ask Alexa to check the weather, play music, and more.</div>', 
        'img': 'pic-1-0167.jpg',
        'paddingBox': '10vh 25px 0px',
    }},
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'features', 'specifications': [
        {'title': 'Effortless access to Amazon', 'text': 'Popular pre-installed Amazon apps allow quick access to your content, order history, wish list, shipment updates, and more. Plus, shop a personalized selection of daily offers using the Deals widget.', 'icon': ''},
        {'title': 'Prime benefits at your fingertips', 'text': 'A single sign-on experience provides Prime members with easy access to tens of thousands of movies and TV episodes, over two million songs, unlimited photo storage and backup, free 2-day shipping, and more.', 'icon': ''},
        {'title': 'Amazon Alexa enabled', 'text': 'Take the life-enhancing capabilities of Alexa anywhere you take your phone. Ask questions, plan your day, listen to music, and more. Wherever, whenever', 'icon': ''},
        {'title': 'Clear pics. Super selfies.', 'text': 'You’re always seen in the best light with the 5 MP selfie cam and flash. Plus, the 8 MP rear-facing camera focuses in an instant, so you don’t miss a moment. Your photos look great indoors and on dark cloudy days, thanks to auto enhancement.', 'icon': ''},
        {'title': 'Worry-free enjoyment', 'text': 'A water-repellent design safeguards your phone inside and out. So, don’t worry about accidental spills and splashes.<sup>†</sup>', 'icon': ''},
        {'title': 'For your 24-hour life', 'text': 'Do the things you want to do without stopping to search for an outlet. With a swappable battery, you can go up to 24 hours on a single charge.*', 'icon': ''},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}