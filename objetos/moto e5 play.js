// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#fff', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'clear pics. super selfies.', 
        'text': '<div style="padding-bottom: 13vh">You’re always seen in the best light with the 5 MP selfie cam and flash. Plus, the 8 MP rear-facing camera focuses in an instant, so you don’t miss a moment. Your photos look great indoors and on dark cloudy days, thanks to auto enhancement.</div>', 
        'img': 'pic-1-0118.jpg',
        'paddingBox': '13vh 25px 0vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'for your 24-hour life', 
        'text': '<div style="padding-bottom: 13vh">Do the things you want to do without stopping to search for an outlet. With a 2800 mAh battery, you can go a full day on a single charge.*</div>', 
        'img': 'pic-1-0119.jpg',
        'paddingBox': '13vh 25px 0vh',
    }},
    // BLOCO TEXTO ESQUERDA
    { 'info-left': {
        'title': '<div style="color: #fff">smart and secure</div>', 
        'text': '<div style="color: #fff">Touch the fingerprint reader to wake up and unlock and lock your phone instantly. It’s discreetly located within the phone’s iconic logo, so you hardly know it’s there until you use it. <strong>Availability varies per model.</strong><sup>†</sup></div>', 
        'img': 'pic-1-0120.jpg',
        'paddingBox': '10vh 25px 15vh',
        'bgcolor': '#ff6b02',
        'info-title': '#fff',
        'info-text': '#fff',

    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'worry-free enjoyment', 
        'text': '<div style="padding-bottom: 10vh">A water-repellent design safeguards your phone inside and out. So, don’t worry about accidental spills and splashes.<sup>‡</sup></div>', 
        'img': 'pic-1-0121.jpg',
        'paddingBox': '13vh 25px 10vh',
    }},
        // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'details make the difference', 
        'text': '<div style="padding-bottom: 10vh">We mean it when we say you can make everyday extraordinary with <strong>moto e⁵ play</strong>. All the features. A fraction of the price.</div>',
        'paddingBox': '15vh 25px 0px',
    }},
    // BLOCO DE CARDS
    { 'cards': {'title': '', 'paddingBox': '2vh 25px 0px', 'cards': [
        {'img': 'icon-054.jpg', 'title': 'You can have it all', 'text': 'Add up to 128 GB more photos, songs, and movies thanks to a dedicated microSD card slot.‡ Plus, enjoy free, high-quality, cloud-based storage with Google Photos.††'},
        {'img': 'icon-074.jpg', 'title': 'Performance that packs a punch', 'text': 'We’re not playing games when it comes to performance. With 2 GB of memory and a Qualcomm® Snapdragon™ quad-core processor, you have all the speed you need for games, music, and videos.'},
        {'img': 'icon-053.jpg', 'title': 'Exclusive interactions', 'text': 'Only <strong>motorola </strong>phones react to simple gestures to instantly reveal a function or communication you need at that moment. Customize which <strong>moto experiences</strong> you’d like to use by simply visiting the<strong> moto app</strong> on your new phone.'},
    ]} },
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'specifications', 'paddingBox': '55px 25px 5vh', 'specifications': [
        {'title': '8 MP camera with autofocus', 'text': 'Capture sharp photos even in low light conditions—and even when there’s no time to focus.', 'icon': 'icon-075.svg'},
        {'title': 'All-day battery', 'text': 'Go up to 24 hours on a single charge.*', 'icon': 'icon-047.svg'},
        {'title': 'Fingerprint reader', 'text': 'Never mind remembering a passcode. Your fingerprint unlocks your phone. Availability varies per model.†', 'icon': 'icon-057.svg'},
        {'title': 'Water-repellent design', 'text': 'Keep your phone protected from accidental spills and splashes.‡', 'icon': 'icon-024.png'},
        {'title': 'Expandable storage', 'text': 'Make room for more photos, songs, and videos thanks to an additional microSD card slot.§', 'icon': 'icon-073.svg'},
        {'title': 'Fast performance', 'text': 'Get all the speed you need with 2 GB of memory and a quad-core processor.', 'icon': 'icon-076.svg'},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}