// CORES DA DESCRIÇÃO
var colors = {
    'movie-bg': '#FFFFFF', // COR DE FUNDO DO VIDEO
    'movie-title': '#fff', // COR DO TITULO DO VIDEO
    'info-bg': '#fff', // COR DE FUNDO DOS BLOCOS DE INFORMAÇÕES
    'info-title': '#000', // COR DO TITULO DOS BLOCOS DE INFORMAÇÕES
    'info-text': '#000', // COR DO TEXTO DOS BLOCOS DE INFORMAÇÕES
    'slider-bg': '#fff', // COR DE FUNDO DO SLIDER
    'cards-bg': '#fff', // COR DE FUNDO DOS CARDS
    'specification-bg': '#fff', // COR DE FUNDO DAS ESPECIFICAÇÕES
}

var paddingBox = '';

// OBJETO DOS BLOCOS PRINCIAPIS
var objs = [
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'big battery. ultimate Amazon Prime access. total freedom.', 
        'text': '<div style="padding-bottom: 3vh">Do more of what you love with up to 36 hours of battery life.* Watch videos on an edge-to-edge 5.7" HD+ Max Vision display. Capture great photos with a 13 MP rapid-focus camera. Unlock your phone with your fingerprint. And do it all fast on a 1.4 GHz quad-core processor.<sup>††</sup>',
        'paddingBox': '3vh 25px 0vh',
    }},
    // BLOCO VIDEO
    { 'movie': {
        'title': '<div style="color: #fee500">Watch product video</div>', 
        'yt_id': 'BNIosf0wZ-k', 
        'btn_icon': 'icon_circleplay_100@2x.png', 
        'bg': 'pic-1-0093.jpg', 
        'height': '430px',
        'bgcolor': '#fff',
        'titlecolor': '#fee500',
        'paddingBox': '3vh 25px 0vh',
    }},
    // BLOCO TEXTO NO TOPO
    { 'info-top': {
        'title': 'Amazon Alexa, right on your phone', 
        'text': '<div style="padding-bottom: 3vh">Whether you are at home, work, a friends house, or out and about, just double press the power button when your phone is unlocked and ask Alexa to check the weather, play music, and more.</div>', 
        'img': 'pic-1-0159.jpg',
        'paddingBox': '5vh 25px 0px',
    }},
    // BLOCO DE ESPECIFICAÇÕES
    { 'specification': {'title': 'features', 'paddingBox': '5vh 25px 0px', 'specifications': [
        {'title': 'Effortless access to Amazon', 'text': 'Popular pre-installed Amazon apps allow quick access to your content, order history, wish list, shipment updates, and more. Plus, shop a personalized selection of daily deals right from the home screen using the Amazon Widget.', 'icon': ''},
        {'title': 'Prime benefits at your fingertips', 'text': 'A single sign-on experience provides Prime members with easy access to tens of thousands of movies and TV episodes, over two million songs, unlimited photo storage and backup, free 2-day shipping, and more.', 'icon': ''},
        {'title': 'Amazon Alexa enabled', 'text': 'Take the life-enhancing capabilities of Alexa anywhere you take your phone. Ask questions, plan your day, listen to music, and more. Wherever, whenever', 'icon': ''},
        {'title': 'Unlocked for carrier freedom', 'text': 'The new <strong>moto g</strong> is unlocked so you can change carriers and keep your phone by just swapping SIM cards. Compatible with AT&amp;T, T-Mobile, Sprint, Verizon networks, and Project Fi.', 'icon': ''},
        {'title': '4000 mAh battery + turbopower™ charging', 'text': 'Go up to 36 hours on a single charge, then get hours of power in just minutes.*', 'icon': ''},
        {'title': 'Edge-to-edge display', 'text': 'Enjoy your favorite entertainment on a 5.7” HD+ Max Vision display.', 'icon': ''},
    ]} },
]

// OBJETO DOS BLOCOS "VEJA MAIS" - NOME IDENTIFICADOR DEVE SER IGUAL AO DA CHAMADA NO OBJETO PRINCIPAL
var objs_more = {
    'more-1': [
        { 'info-left': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
        { 'info-right': {'title': '', 'text': '', 'btn_more': '', 'img': 'moto-5g.png'} },
    ],
    'more-2': [
        { 'slider': {'title': '', 'sliders': [
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
            {'img': 'slide1.jpg', 'text': ''},
        ]} },
    ]
}