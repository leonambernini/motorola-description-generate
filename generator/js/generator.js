var version = '1.3';

var types = {
    'movie': '.md-block-movie',
    'movie-slider': '.md-block-movie-slider',
    'info-left': '.md-block-info-left',
    'info-right': '.md-block-info-right',
    'info-top': '.md-block-info-top',
    'info-2-text': '.md-block-info-2-text',
    'slider': '.md-slider',
    'cards': '.md-columns-cards',
    'specification': '.md-specification',
    'specification-btn-more': '.md-specification-btn-more',
    'effect': '.md-effect',
    'cards2': '.md-cards-2',
    'cards3': '.md-cards-3',
    'mosaic': '.md-mosaic',
    'text-bg': '.md-block-text-bg',
}

var yt_count = 0;

function htmlVersion($html){
    return $html.attr('description-version', version);
}

function prepare(type, options, moreInfo){
    var res = '';
    switch (type){
        case 'movie':
            res = prepareMovie(options);
            break;
        case 'movie-slider':
            res = prepareMovieSlider(options);
            break;
        case 'slider':
            res = prepareSlider(options);
            break;
        case 'cards':
            res = prepareCards(type, options);
            break;
        case 'specification':
            res = prepareSpecification(options);
            break;
        case 'specification-btn-more':
            res = prepareSpecificationBtnMore(options);
            break;
        case 'effect':
            res = prepareEffect(options);
            break;
        case 'cards2':
            res = prepareCards(type, options);
            break;
        case 'cards3':
            res = prepareCards(type, options);
            break;
        case 'mosaic':
            res = prepareMosaic(options);
            break;
        case 'text-bg':
            res = prepareTextBg(options);
            break;
        default:
            res = prepareInfo(type, options, moreInfo);
    }

    return res;
}
function prepareTextBg(options){
    var $html = $('#models').find(types['text-bg']).clone();

    htmlVersion($html);
    if( options.paddingBox !== undefined && options.paddingBox !== null && options.paddingBox !== '' ){
        $html.css({'padding': options.paddingBox});
    }else{
        $html.css('padding', paddingBox);
    }

    if( options.bgcolor !== undefined && options.bgcolor !== null && options.bgcolor !== '' ){
        $html.css({'background-color': options.bgcolor});
    }else{
        $html.css('background-color', colors['info-bg']);
    }

    $html.find('.md-h2').html(options.title).css('color', colors['info-title']);
    $html.find('.md-p').html(options.text).css('color', colors['info-text']);

    if( options.titlecolor !== undefined && options.titlecolor !== null && options.titlecolor !== '' ){
        $html.find('.md-h2, .md-p').css({'color': options.titlecolor});
    }

    if( options.col !== undefined && options.col !== null && options.col !== '' ){
        $html.find('.md-text-bg-col').addClass('col-sm-'+options.col);
    }else{
        $html.find('.md-text-bg-col').addClass('col-sm-5');
    }

    if( options.bg != undefined && options.bg != null ){
        $html.find('.md-block-bg').attr('src', __dir + options.bg);
    }

    return $html;
}
function prepareMosaic(options){
    var $html = $('#models').find(types['mosaic']).clone();

    htmlVersion($html);
    if( options.paddingBox !== undefined && options.paddingBox !== null && options.paddingBox !== '' ){
        $html.css({'padding': options.paddingBox});
    }else{
        $html.css('padding', '0');
    }

    if( options.bgcolor !== undefined && options.bgcolor !== null && options.bgcolor !== '' ){
        $html.css({'background': options.bgcolor});
    }

    $html.find('.md-h2').html(options.title).css('color', colors['info-title']);
    $html.find('.md-p').html(options.text).css('color', colors['info-text']);

    if( options.titlecolor !== undefined && options.titlecolor !== null && options.titlecolor !== '' ){
        $html.find('.md-h2, .md-p').css({'color': options.titlecolor});
    }

    $html.find('.md-mosaic-modal-btn-close > img').attr('src', __dir+options.modalBtnClose);

    for( var x = 0; x < options.items.length; x++ ){
        var item = options.items[x];
        $html.find('.md-mosaic-box').append(
            '<div class="md-mosaic-item p-0" style="width: '+item.width+'">'+
            '   <div class="md-mosaic-border" style="height: '+item.height+';">' +
            '       <h3>'+item.title+'</h3>'+
            '       <img src="'+__dir+item.bg+'" alt="" class="md-mosaic-img"/>'+
            '       <a href="#" class="md-mosaic-hover-box">' +
            '           <span class="md-mosaic-text">'+item.text+'</span>'+
            '           <span class="md-mosaic-btn"><span>></span> '+item.btn+'</span>'+
            '       </a>' +
            '   </div>' +
            '</div>'
        )
        $html.find('.md-mosaic-modal-nav-carousel').append(
            '<div class="md-mosaic-btn">'+
            '   <h4>'+item.title+'</h4>'+
            '</div>'
        )
    }
    for( var x = 0; x < options.modalItems.length; x++ ){
        var item = options.modalItems[x];

        var htmlModel = { 
            'text-image': '<div class="md-mosaic-modal-item md-{{class-model}}" style="{{bg}}">{{textContent}}</div>',
            'multi-text': '<div class="md-mosaic-modal-item md-{{class-model}}""><div class="row">{{textsContent}}</div></div>',
            'cards': '<div class="md-mosaic-modal-item md-{{class-model}}"><h4>{{title}}</h4><div class="md-mosaic-cards row">{{cards}}</div></div>',
            'text-movie': '<div class="md-mosaic-modal-item md-{{class-model}}" style="{{bg}}">{{textContent}}</div>',
        }

        var helpHtml = {
            'text-col' : '<div class="row align-items-center"><div class="col-text col-12 col-sm-{{colText}} pb-4 align-self-cente"><h5 class="md-h5">{{title}}</h5><p class="md-p">{{text}}</p></div><div class="col-image col-12 col-sm-{{colImage}} align-self-cente"><img src="{{img}}" alt="" class="img-fluid" /></div></div>',
            'text-movie': '<div class="row align-items-center"><div class="col-text col-12 col-sm-{{colText}} pb-4 align-self-cente"><h5 class="md-h5">{{title}}</h5><p class="md-p">{{text}}</p></div><div class="col-movie col-12 col-sm-{{colImage}} align-self-cente">{{movie-box}}</div></div>',
            'text-card': '<div class="md-mosaic-card-border" {{bg}}><img src="{{img}}" alt="" class="img-fluid d-block m-auto"/><div class="md-mosaic-card-title-border"><h5>{{title}}</h5><p>{{text}}</p></div></div>',
        }
        
        if( item.model != undefined && item.model != null && item.model != '' && htmlModel[item.model] != undefined && htmlModel[item.model] != null ){
            var bg = ( item.bg != undefined && item.bg != null ) ? 'background: '+item.bg+'"' : 'background: '+options.modalbg+'"';
            var title = ( item.title != undefined && item.title != null ) ? item.title : '';
            var text = ( item.text != undefined && item.text != null ) ? item.text : '';
            var img = ( item.img != undefined && item.img != null ) ? __dir+item.img : '#';
            var colText = ( item.colText != undefined && item.colText != null ) ? item.colText : '5';
            var colImage = ( item.colImage != undefined && item.colImage != null ) ? item.colImage : '7';

            var textContent = helpHtml['text-col']
                                            .replace(/{{bg}}/g, bg)
                                            .replace(/{{title}}/g, title)
                                            .replace(/{{text}}/g, text)
                                            .replace(/{{img}}/g, img)
                                            .replace(/{{colText}}/g, colText)
                                            .replace(/{{colImage}}/g, colImage)
            if( item.model == 'text-movie' ){
                var movieBox = $('<div></div>').html(prepareMovie(item.movie));

                textContent = helpHtml['text-movie']
                                        .replace(/{{bg}}/g, bg)
                                        .replace(/{{title}}/g, title)
                                        .replace(/{{text}}/g, text)
                                        .replace(/{{movie-box}}/g, movieBox.html())
                                        .replace(/{{colText}}/g, colText)
                                        .replace(/{{colImage}}/g, colImage)
            }

            var htmlCards = '';
            if( item['cards'] != undefined && item['cards'] != null && item['cards'].length ){
                for( var y = 0; y < item['cards'].length; y++ ){
                    var card = item['cards'][y];
                    var cardCol = ( card.col != undefined && card.col != null ) ? card.col : '4';
                    var cardBg = ( card.bg != undefined && card.bg != null ) ? 'style="background: '+card.bg+'"' : '';
                    var cardImg = ( card.img != undefined && card.img != null ) ? __dir+card.img : '';
                    var cardTitle = ( card.title != undefined && card.title != null ) ? card.title : '';
                    var cardText = ( card.text != undefined && card.text != null ) ? card.text : '';

                    var cartItem = helpHtml['text-card']
                                                .replace(/{{bg}}/g, cardBg)
                                                .replace(/{{img}}/g, cardImg)
                                                .replace(/{{title}}/g, cardTitle)
                                                .replace(/{{text}}/g, cardText)
                    htmlCards += '<div class="md-mosaic-card-item col-12 col-md-'+cardCol+'">'+cartItem+'</div>';
                }
            }
            
            var textsContent = '';
            if( item['texts'] != undefined && item['texts'] != null && item['texts'].length ){
                for( var y = 0; y < item['texts'].length; y++ ){
                    var texts = item['texts'][y];
                    var textCol = ( texts.colText != undefined && texts.colText != null ) ? texts.colText : '';
                    var imageCol = ( texts.colImage != undefined && texts.colImage != null ) ? texts.colImage : '';
                    var textBg = ( texts.bg != undefined && texts.bg != null ) ? 'style="background: '+texts.bg+'"' : 'style="background: '+options.modalbg+'"';
                    var textImg = ( texts.img != undefined && texts.img != null ) ? __dir+texts.img : '';
                    var textTitle = ( texts.title != undefined && texts.title != null ) ? texts.title : '';
                    var textText = ( texts.text != undefined && texts.text != null ) ? texts.text : '';
                    var type = ( texts.type != undefined && texts.type != null ) ? texts.type : 'col';

                    var textItem = helpHtml['text-'+type]
                                                .replace(/{{bg}}/g, textBg)
                                                .replace(/{{colText}}/g, textCol)
                                                .replace(/{{colImage}}/g, imageCol)
                                                .replace(/{{img}}/g, textImg)
                                                .replace(/{{title}}/g, textTitle)
                                                .replace(/{{text}}/g, textText)
                    textsContent += '<div class="col-12 col-md-' + texts['col'] + '"><div class="md-mosaic-multi-text-border" '+textBg+'>' + textItem + '</div></div>';
                }
            }


            var _html = htmlModel[item.model];
            $html.find('.md-mosaic-modal-items-carousel').append(
                _html
                    .replace(/{{class-model}}/g, item.model)
                    .replace(/{{bg}}/g, bg)
                    .replace(/{{title}}/g, title)
                    .replace(/{{text}}/g, text)
                    .replace(/{{img}}/g, img)
                    .replace(/{{colText}}/g, colText)
                    .replace(/{{colImage}}/g, colImage)
                    .replace(/{{cards}}/g, htmlCards)
                    .replace(/{{textContent}}/g, textContent)
                    .replace(/{{textsContent}}/g, textsContent)
            )
        }
    }
    if( options.modalcolor !== undefined && options.modalcolor !== null && options.modalcolor !== '' ){
        $html.find('.md-mosaic-items-box-modal').find('p, h4, h5, h6, h3').css({'color': options.modalcolor});
    }

    return $html;
}
function prepareEffect(options){
    var $html = $('#models').find(types['effect']).clone();

    htmlVersion($html);
    if( options.paddingBox !== undefined && options.paddingBox !== null && options.paddingBox !== '' ){
        $html.css({'padding': options.paddingBox});
    }else{
        $html.css('padding', paddingBox);
    }

    if( options.bgcolor !== undefined && options.bgcolor !== null && options.bgcolor !== '' ){
        $html.css({'background': options.bgcolor});
    }else{
        $html.css('background', colors['info-bg']);
    }
    
    $html.find('.md-h1').html(options.title).css('color', colors['info-title']);
    $html.find('.md-effect-border').html(options.text);

    if( options.titlecolor !== undefined && options.titlecolor !== null && options.titlecolor !== '' ){
        $html.find('.md-h1').css({'color': options.titlecolor});
    }

    if( options.img != undefined && options.img != null ){
        $html.find('.img-fluid').attr('src', __dir + options.img);
    }else{
        $html.find('.img-fluid').remove();
    }

    return $html;
}
function prepareInfo(type, options, moreInfo){
    var $html = $('#models').find(types[type]).clone();

    htmlVersion($html);
    if( options.paddingBox !== undefined && options.paddingBox !== null && options.paddingBox !== '' ){
        $html.css({'padding': options.paddingBox});
    }else{
        $html.css('padding', paddingBox);
    }

    if( options.imgp0 !== undefined && options.imgp0 !== null && options.imgp0 ){
        $html.css({'padding': '0'});
        $html.find('.col-image, .col-img').css({'padding': '0'}).removeClass('pr-5 pl-5 pt-5 pb-5 p-5');
    }

    if( options.bgcolor !== undefined && options.bgcolor !== null && options.bgcolor !== '' ){
        $html.css({'background': options.bgcolor});
    }else{
        $html.css('background', colors['info-bg']);
    }

    if( moreInfo ){
        $html.find('.md-item-more-info').remove();
    }

    $html.find('.md-h2').html(options.title).css('color', colors['info-title']);
    
    if( options.title2 != undefined && options.title2 != null && options.title2 != '' ){
        $html.find('.col-text-2 .md-h2').html(options.title2);
    }

    if( options.titleDegrade ){
        $html.find('.md-h2').addClass('md-title-degrade');
    }

    $html.find('.md-p').html(options.text).css('color', colors['info-text']);
    
    if( options.text2 != undefined && options.text2 != null && options.text2 != '' ){
        $html.find('.col-text-2 .md-p').html(options.text2);
    }

    if( options.titlecolor !== undefined && options.titlecolor !== null && options.titlecolor !== '' ){
        $html.find('.md-h2, .md-p').css({'color': options.titlecolor});
    }

    if( options.img != undefined && options.img != null ){
        $html.find('.md-block-info-img').attr('src', __dir + options.img);
    }else{

        if( options.movie != undefined && options.movie != null ){
            $html.find('.col-image').html('<div class="md-block-movie-player md-movie-no-player" id="player-'+(yt_count).toString()+'" data-width="100%" data-height="'+options.movie.height+'" data-youtube="'+options.movie.yt_id+'"></div>');
            yt_count++;
        }else{
            $html.find('.col-image').remove();
        }
    }

    if( options.btn_more != undefined && options.btn_more != null && options.btn_more != '' && objs_more[options.more_info].length ){
        $html.find('.md-more-info-btn').html(options.btn_more).css({'color': colors['info-title'], 'border-color': colors['info-title']});

        for( var x = 0; x < objs_more[options.more_info].length; x++ ){
            for( obj in objs_more[options.more_info][x] ){
                $html.find('.md-item-more-info').append( prepare(obj, objs_more[options.more_info][x][obj], true) );
            }
        }

        if( $html.find('.md-item-more-info .md-more-info-btn').length < 1 ){
            $html.find('.md-item-more-info').append('<button class="md-more-info-btn"><img src="' + __dir + 'close-icon-light.png" width="50" height="50" /></button>')
        }

    }else{
        $html.find('.md-more-info-btn').remove();
    }

    if( options.paddingBox1 != undefined && options.paddingBox1 != null && options.paddingBox1 != '' ){
        $html.find('.col-text-1').css('padding', options.paddingBox1);
    }
    if( options.paddingBox2 != undefined && options.paddingBox2 != null && options.paddingBox2 != '' ){
        $html.find('.col-text-2').css('padding', options.paddingBox2);
    }

    return $html;
}
function prepareSlider(options){
    var $html = $('#models').find(types['slider']).clone();
    
    htmlVersion($html);
    if( options.paddingBox !== undefined && options.paddingBox !== null && options.paddingBox !== '' ){
        $html.css({'padding': options.paddingBox});
    }else{
        $html.css('padding', paddingBox);
    }

    if( options.bgcolor !== undefined && options.bgcolor !== null && options.bgcolor !== '' ){
        $html.css({'background': options.bgcolor});
    }else{
        $html.css({'background': colors['slider-bg']});
    }

    $html.find('.md-h2').html(options.title);

    if( options.titlecolor !== undefined && options.titlecolor !== null && options.titlecolor !== '' ){
        $html.find('.md-h2').css({'color': options.titlecolor});
        $html.find('.md-slider-box').attr('data-pager-color', options.titlecolor);
    }
    
    var html = '';
    for( var x = 0; x < options.sliders.length; x++  ){
        var slide = options.sliders[x];

        if( slide.yt_id != undefined && slide.yt_id != null && slide.yt_id != '' ){
            html += '<div class="md-slider-item pl-4 pr-4 text-center">' +
                    '   <img src="'+__dir + slide.img+'" alt="" class="img-fluid" />' +
                    '   <div class="md-slider-details text-center">'+slide.text+'</div>' +
                    '   <button class="md-block-movie-play-btn-modal" data-id="#modal-movie-'+yt_count+'" data-yt-id="player-'+yt_count+'"><img src="'+__dir + slide.btn_icon+'" alt="" width="50" height="50"/></button>' +
                    '</div>';
            $('#motorola-description').append('<div id="modal-movie-'+yt_count+'" style="display: none;" class="md-modal">'+
                            '   <div class="md-block-movie-player" id="player-'+yt_count+'" data-youtube="'+slide.yt_id+'"></div>' +
                            '   <button class="md-close-modal" data-yt-id="player-'+yt_count+'"><img src="' + __dir + 'close-icon-light.png" width="50" height="50" /></button>'+
                            '</div>');

            yt_count++;
        }else{
            html += '<div class="md-slider-item pl-4 pr-4 text-center"><img src="'+__dir + slide.img+'" alt="" class="img-fluid" /><div class="md-slider-details text-center">'+slide.text+'</div></div>';
        }
    }
    $html.find('.md-slider-box').html(html);

    return $html;
}
function prepareCards(type, options){
    var $html = $('#models').find(types[type]).clone();

    htmlVersion($html);
    if( options.paddingBox !== undefined && options.paddingBox !== null && options.paddingBox !== '' ){
        $html.css({'padding': options.paddingBox});
    }else{
        $html.css('padding', paddingBox);
    }
    
    if( options.bgcolor !== undefined && options.bgcolor !== null && options.bgcolor !== '' ){
        $html.css({'background': options.bgcolor});
    }else{
        $html.css('background', colors['cards-bg']);
    }

    $html.find('.md-h2').html(options.title);

    if( options.titlecolor !== undefined && options.titlecolor !== null && options.titlecolor !== '' ){
        $html.find('.md-h2, h4, p').css({'color': options.titlecolor});
    }

    if( options.text !== undefined && options.text !== null && options.text !== '' ){
        $html.find('.md-p').html(options.text);
    }
    
    var html = '';
    for( var x = 0; x < options.cards.length; x++  ){
        var card = options.cards[x];
        var imgWidth = ( card.imgWidth != undefined && card.imgWidth != null && card.imgWidth != '' ) ? 'width="'+card.imgWidth+'"' : '';
        var imgHeight = ( card.imgHeight != undefined && card.imgHeight != null && card.imgHeight != '' ) ? 'width="'+card.imgHeight+'"' : '';
        if( type == 'cards2' ){
            html += '<div class="md-card-item col-12 col-sm-4 col-md-3 mb-4 mb-sm-0"><div class="md-card-details"><h4 class=" text-center">'+card.title+'</h4><img src="'+__dir + card.img+'" alt="" class="img-fluid" '+imgWidth+' '+imgHeight+' /><p class=" text-center">'+card.text+'</p></div></div>';
        }else if( type == 'cards3' ){
            var bgcolor = ( card.bgcolor != undefined && card.bgcolor != null && card.bgcolor != '' ) ? 'style="background: '+card.bgcolor+'"' : ''; 
            var titlecolor = ( card.titlecolor != undefined && card.titlecolor != null && card.titlecolor != '' ) ? 'style="color: '+card.titlecolor+'"' : ''; 
            var button = ( card.button != undefined && card.button != null && card.button != '' ) ? '<div class="btn-card">'+card.button+'</div>' : ''; 
            html += '<div class="md-card-item col-12 col-md-4 mb-4 mb-sm-0"><img src="'+__dir + card.img+'" alt="" class="img-fluid" '+imgWidth+' '+imgHeight+' /><div class="md-card-details" '+bgcolor+'><h4 '+titlecolor+'>'+card.title+'</h4><p '+titlecolor+'>'+card.text+'</p>'+button+'</div></div>';
        }else{
            var col = '3';
            if( card.col != undefined && card.col != null && card.col != '' ){
                col = card.col;
            } 
            html += '<div class="md-card-item col-12 col-md-'+col+' mb-4 mb-sm-0"><img src="'+__dir + card.img+'" alt="" class="img-fluid" '+imgWidth+' '+imgHeight+' /><div class="md-card-details"><h4>'+card.title+'</h4><p>'+card.text+'</p></div></div>';
        }
    }
    $html.find('.md-columns-cards-box').html(html);

    return $html;
}
function prepareSpecificationBtnMore(options){
    var $html = $('#models').find(types['specification-btn-more']).clone();

    htmlVersion($html);
    var $box1 = $html.find('.md-specification-2-box');
    
    if( options.paddingBox !== undefined && options.paddingBox !== null && options.paddingBox !== '' ){
        $box1.css({'padding': options.paddingBox});
    }else{
        $box1.css('padding', paddingBox);
    }

    if( options.bgcolor !== undefined && options.bgcolor !== null && options.bgcolor !== '' ){
        $box1.css({'background': options.bgcolor});
    }else{
        $box1.css('background', colors['specification-bg']);
    }

    $box1.find('.md-h2').html(options.title);
    $box1.find('.md-p').html(options.text);
    $box1.find('.md-specification-box-col-1').html(options.col1);
    $box1.find('.md-specification-box-col-2').html(options.col2);

    if( options.img != undefined && options.img != null ){
        $box1.find('.md-specification-img').attr('src', __dir + options.img);
    }else{
        $box1.find('.md-specification-img').remove();
    }

    if( options.titlecolor !== undefined && options.titlecolor !== null && options.titlecolor !== '' ){
        $box1.find('.md-h2, h4, h5, p').css({'color': options.titlecolor});
    }

    if( options.more != undefined && options.more != null ){
        var $box2 = $html.find('.md-specification-2-box-more');
        if( options.more.paddingBox !== undefined && options.more.paddingBox !== null && options.more.paddingBox !== '' ){
            $box2.css({'padding': options.more.paddingBox});
        }else{
            $box2.css('padding', paddingBox);
        }
    
        if( options.more.bgcolor !== undefined && options.more.bgcolor !== null && options.more.bgcolor !== '' ){
            $box2.css({'background': options.more.bgcolor});
        }else{
            $box2.css('background', colors['specification-bg']);
        }
    
        $box2.find('.md-specification-box-col-1').html(options.more.col1);
        $box2.find('.md-specification-box-col-2').html(options.more.col2);
    
        if( options.more.img != undefined && options.more.img != null ){
            $box2.find('.md-specification-img').attr('src', __dir + options.more.img);
        }else{
            $box2.find('.md-specification-img').remove();
        }
    
        if( options.more.titlecolor !== undefined && options.more.titlecolor !== null && options.more.titlecolor !== '' ){
            $box2.find('.md-h2, h4, h5, p').css({'color': options.more.titlecolor});
        }
    }else{
        $box1.find('.btn-box').remove();
    }

    return $html;
}
function prepareSpecification(options){
    var $html = $('#models').find(types['specification']).clone();

    htmlVersion($html);
    if( options.paddingBox !== undefined && options.paddingBox !== null && options.paddingBox !== '' ){
        $html.css({'padding': options.paddingBox});
    }else{
        $html.css('padding', paddingBox);
    }

    if( options.bgcolor !== undefined && options.bgcolor !== null && options.bgcolor !== '' ){
        $html.css({'background': options.bgcolor});
    }else{
        $html.css('background', colors['specification-bg']);
    }

    $html.find('.md-h1').html(options.title);

    if( options.titlecolor !== undefined && options.titlecolor !== null && options.titlecolor !== '' ){
        $html.find('.md-h1, h4, p').css({'color': options.titlecolor});
    }
    
    var html = '';
    for( var x = 0; x < options.specifications.length; x++  ){
        var specifications = options.specifications[x];

        var icon = '';
        if( specifications.icon != undefined && specifications.icon != null ){
            icon = '<img src="'+__dir + specifications.icon+'" alt="" class="mb-1 d-block" width="50" height="50"/>';
        }
        html += '<div class="md-specification-item col-12 col-sm-6 col-md-4"><div class="icon" style="min-height: 50px;">'+icon+'</div><div class="md-specification-border"><h4>'+specifications.title+'</h4><p>'+specifications.text+'</p></div></div>';
    }
    $html.find('.md-specification-box').html(html);

    return $html;
}
function prepareMovie(options){
    var $html = $('#models').find(types['movie']).clone();

    htmlVersion($html);
    if( options.paddingBox !== undefined && options.paddingBox !== null && options.paddingBox !== '' ){
        $html.css({'padding': options.paddingBox});
    }else{
        $html.css('padding', paddingBox);
    }

    if( options.bgcolor !== undefined && options.bgcolor !== null && options.bgcolor !== '' ){
        $html.css({'background': options.bgcolor});
    }else{
        $html.css('background', colors['movie-bg']);
    }

    if( options.height != undefined && options.height != null && options.height != '' ){
        $html.css('height', options.height).addClass('img-absolute');
        $html.find('.md-movie-bg').css({'width': '100%'})
    }
    $html.find('.md-block-movie-play-img').attr({'src': __dir + options.btn_icon})
    $html.find('.md-movie-bg').attr({'src': __dir + options.bg})
    $html.attr('data-id', 'player-'+yt_count);
    $html.find('#player').attr('id', 'player-'+(yt_count).toString()).attr('data-youtube', options.yt_id);
    $html.find('.md-block-movie-play-text').html(options.title).css('color', colors['movie-title']);

    yt_count++;
    return $html;
}
function prepareMovieSlider(options){
    var $html = $('#models').find(types['movie-slider']).clone();

    htmlVersion($html);
    if( options.paddingBox !== undefined && options.paddingBox !== null && options.paddingBox !== '' ){
        $html.css({'padding': options.paddingBox});
    }else{
        $html.css('padding', paddingBox);
    }

    if( options.bgcolor !== undefined && options.bgcolor !== null && options.bgcolor !== '' ){
        $html.css({'background': options.bgcolor});
    }else{
        $html.css('background', colors['movie-bg']);
    }

    if( options.height != undefined && options.height != null && options.height != '' ){
        $html.css('height', options.height).addClass('img-absolute');
        $html.find('.md-movie-bg').css({'width': '100%'})
    }

    for( var x = 0; x < options.movies.length; x++ ){
        var movie = options.movies[x];
        var $htmlItem = $('#models').find(types['movie']).clone();

        $htmlItem.removeClass('md-item');
        if( movie.paddingBox !== undefined && movie.paddingBox !== null && movie.paddingBox !== '' ){
            $htmlItem.css({'padding': movie.paddingBox});
        }else{
            $htmlItem.css('padding', paddingBox);
        }
    
        if( movie.bgcolor !== undefined && movie.bgcolor !== null && movie.bgcolor !== '' ){
            $htmlItem.css({'background': movie.bgcolor});
        }else{
            $htmlItem.css('background', colors['movie-bg']);
        }
    
        if( movie.height != undefined && movie.height != null && movie.height != '' ){
            $htmlItem.css('height', movie.height).addClass('img-absolute');
            $htmlItem.find('.md-movie-bg').css({'width': '100%'})
        }

        $htmlItem.find('.md-block-movie-play-img').attr({'src': __dir + movie.btn_icon})
        $htmlItem.find('.md-movie-bg').attr({'src': __dir + movie.bg})
        $htmlItem.attr('data-id', 'player-'+yt_count);
        $htmlItem.find('#player').attr('id', 'player-'+(yt_count).toString()).attr('data-youtube', movie.yt_id);
        $htmlItem.find('.md-block-movie-play-text').html(movie.title).css('color', colors['movie-title']);
    
        yt_count++;

        $html.append($htmlItem);
    }

    return $html;
}

$(function() {

    for ( var x = 0; x < objs.length; x++ ){
        for( obj in objs[x] ){
            $('#result #motorola-description').append( prepare(obj, objs[x][obj]) ).css({'color': colors['info-title']});
        }
    }

    $('#models').remove();

    
});